package com.example.warehousemobileclient.ui.check_goods;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.appcompat.widget.AppCompatButton;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.example.warehousemobileclient.R;
import com.example.warehousemobileclient.adapter.CheckGoods2Adapter;
import com.example.warehousemobileclient.adapter.CheckGoodsAdapter;
import com.example.warehousemobileclient.dao.CheckGoodsDetailDAO;
import com.example.warehousemobileclient.helper.CSVWriter;
import com.example.warehousemobileclient.helper.ConnectFTPServer;
import com.example.warehousemobileclient.model.CheckGoodsDetail;
import com.example.warehousemobileclient.model.CheckGoodsExport;
import com.example.warehousemobileclient.model.GoodsData;
import com.example.warehousemobileclient.model.WRDataGeneral;
import com.example.warehousemobileclient.ui.setting.SettingFragment;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class ListCheckGoodsActivity extends AppCompatActivity {
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private EditText edtSelectDate = null;
    private EditText edtTotalQuantity = null;
    private AppCompatButton btnSelectDate = null;
    private AppCompatButton btnSendFile = null;
    private RecyclerView recyclerView = null;
    private CheckGoods2Adapter adapter;
    ArrayList<CheckGoodsDetail> checkGoodsDetailArrayList;
    CheckGoodsDetail checkGoodsDetail = null;
    CheckGoodsDetailDAO checkGoodsDetailDAO = null;
    String selectDate = "";
    SharedPreferences storage;
    String ipAdress="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_check_goods);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("List kiểm hàng");
        initComponent();


        checkGoodsDetailArrayList = new ArrayList<>();
        checkGoodsDetail = new CheckGoodsDetail();
        checkGoodsDetailDAO = new CheckGoodsDetailDAO(ListCheckGoodsActivity.this);

        deleteOldRecord();

        edtSelectDate.setText(simpleDateFormat.format(Calendar.getInstance().getTime()));
        checkGoodsDetailArrayList = checkGoodsDetailDAO.getByMonthDay(edtSelectDate.getText().toString());
        setAdapter(checkGoodsDetailArrayList);
        edtTotalQuantity.setText(checkGoodsDetailArrayList.size()+"");


        btnSelectDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseCheckDate();
            }
        });

        btnSendFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                exportFile();
            }
        });

    }

    private void initComponent() {
        edtSelectDate = findViewById(R.id.edtSelectDate);
        edtTotalQuantity=findViewById(R.id.edtTotalQuantity);
        btnSelectDate = findViewById(R.id.btnSelectDate);
        btnSendFile=findViewById(R.id.btnSendFile);
        recyclerView = findViewById(R.id.recycler_view);
        storage = getSharedPreferences(SettingFragment.PREFERENCES, Context.MODE_PRIVATE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void alertError(Context context, String message) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));
        dialog.setTitle("Error")
                .setIcon(R.drawable.ic_baseline_error_24)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                    }
                }).show();
    }

    private void alertSuccess(Context context, String message) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));
        dialog.setTitle("Success")
                .setIcon(R.drawable.ic_baseline_done_24)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                    }
                }).show();
    }

    public void exportFile() {
        if (isStoragePermissionGranted()) {
            String fileName = edtSelectDate.getText().toString()+".csv";

            ArrayList<CheckGoodsExport> checkGoodsExports = new ArrayList<>();
            for (int i = 0; i < checkGoodsDetailArrayList.size(); i++) {
                String goodsID= checkGoodsDetailArrayList.get(i).getGoodsID();
                CheckGoodsExport checkGoodsExport = new CheckGoodsExport(goodsID);
                checkGoodsExports.add(checkGoodsExport);
            }

            //write to txt
            Boolean writeToCSV = CSVWriter
                    .generateCSV(new File(Environment.getExternalStorageDirectory()
                            , fileName), checkGoodsExports.toArray());
            if (writeToCSV == true) {
                Toast.makeText(ListCheckGoodsActivity.this, "Export to TXT Success !!!", Toast.LENGTH_SHORT).show();
            }
            else{
                Toast.makeText(ListCheckGoodsActivity.this, "Export to TXT Fail !!!", Toast.LENGTH_SHORT).show();
            }

            //send to server
            if (storage.getString(SettingFragment.IPADRESS, "") != null) {
                ipAdress = storage.getString(SettingFragment.IPADRESS, "");
            }
            String result = ConnectFTPServer.connect(ipAdress);
            if (result.equalsIgnoreCase("success to connect to server " + ipAdress)) {
                ConnectFTPServer.sendFileToPC(ipAdress, fileName);
                alertSuccess(ListCheckGoodsActivity.this,"Đã gửi danh sách mã hàng đã kiểm ngày "+edtSelectDate.getText().toString());
                checkGoodsDetailDAO.deleteByDate(edtSelectDate.getText().toString());
                checkGoodsDetailArrayList = checkGoodsDetailDAO.getByMonthDay(edtSelectDate.getText().toString());
                setAdapter(checkGoodsDetailArrayList);
            } else {
                try {
                    Thread.sleep(2000);
                    String result2 = ConnectFTPServer.connect(ipAdress);
                    if (result2.equalsIgnoreCase("success to connect to server " + ipAdress)) {
                        ConnectFTPServer.sendFileToPC(ipAdress, fileName);
                        alertSuccess(ListCheckGoodsActivity.this,"Đã gửi danh sách mã hàng đã kiểm ngày "+edtSelectDate.getText().toString());
                        checkGoodsDetailDAO.deleteByDate(edtSelectDate.getText().toString());
                        checkGoodsDetailArrayList = checkGoodsDetailDAO.getByMonthDay(edtSelectDate.getText().toString());
                        setAdapter(checkGoodsDetailArrayList);
                    } else {
                        alertError(ListCheckGoodsActivity.this,"Không gửi được danh sách mã hàng đã kiểm ngày "+edtSelectDate.getText().toString());
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
        }
    }

    private void setAdapter(ArrayList<CheckGoodsDetail> checkGoodsDetails) {
        adapter = new CheckGoods2Adapter(getApplicationContext(), checkGoodsDetailArrayList, new CheckGoods2Adapter.ItemClickListener() {
            @Override
            public void onClick(CheckGoodsDetail checkGoodsDetail1) {
            }
        }, new CheckGoods2Adapter.ItemDeleteListener() {
            @Override
            public void onClick(CheckGoodsDetail checkGoodsDetail) {
                dialogDeleteCheckGoods(checkGoodsDetail);
            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    public void dialogDeleteCheckGoods(final CheckGoodsDetail checkGoodsDetail) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ListCheckGoodsActivity.this);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.dialog_delete, viewGroup, false);
        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        Button btnYes, btnNo;
        btnYes = alertDialog.findViewById(R.id.btnYes);
        btnNo = alertDialog.findViewById(R.id.btnNo);
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String id=checkGoodsDetail.getId();
                checkGoodsDetailDAO.deleteByID(id);
                checkGoodsDetailArrayList = checkGoodsDetailDAO.getByMonthDay(edtSelectDate.getText().toString());
                setAdapter(checkGoodsDetailArrayList);
                edtTotalQuantity.setText(checkGoodsDetailArrayList.size()+"");
                alertDialog.cancel();
            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.cancel();
            }
        });
    }

    private void clearList(){
        checkGoodsDetailArrayList.clear();
        setAdapter(checkGoodsDetailArrayList);
    }

    public void chooseCheckDate() {
        Calendar mcurrentTime = Calendar.getInstance();
        int year = mcurrentTime.get(Calendar.YEAR);
        int month = mcurrentTime.get(Calendar.MONTH);
        int day = mcurrentTime.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog;
        datePickerDialog = new DatePickerDialog(ListCheckGoodsActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                if (dayOfMonth < 10) {
                    edtSelectDate.setText(year + "-" + (month + 1) + "-" + "0" + dayOfMonth);
                } else {
                    edtSelectDate.setText(year + "-" + (month + 1) + "-" + dayOfMonth);
                }
                checkGoodsDetailArrayList = checkGoodsDetailDAO.getByMonthDay(edtSelectDate.getText().toString());
                setAdapter(checkGoodsDetailArrayList);
                edtTotalQuantity.setText(checkGoodsDetailArrayList.size()+"");
            }
        }, year, month, day);
        datePickerDialog.setTitle("Chọn ngày kiểm hàng");
        datePickerDialog.show();
    }

    private void deleteOldRecord(){
        checkGoodsDetailDAO.deleteByDateAuto30();
    }
}