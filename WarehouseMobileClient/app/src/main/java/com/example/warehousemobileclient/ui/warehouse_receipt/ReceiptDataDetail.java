package com.example.warehousemobileclient.ui.warehouse_receipt;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.warehousemobileclient.R;
import com.example.warehousemobileclient.adapter.WRDataDetailAdapter;
import com.example.warehousemobileclient.adapter.WRDataHeaderAdapter2;
import com.example.warehousemobileclient.model.WRDataDetail;
import com.example.warehousemobileclient.model.WRDataHeader;
import com.example.warehousemobileclient.service.IWarehouseApi;
import com.example.warehousemobileclient.service.RetrofitService;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import com.symbol.emdk.EMDKManager;
import com.symbol.emdk.EMDKResults;
import com.symbol.emdk.EMDKManager.EMDKListener;
import com.symbol.emdk.EMDKManager.FEATURE_TYPE;
import com.symbol.emdk.barcode.BarcodeManager;
import com.symbol.emdk.barcode.BarcodeManager.ConnectionState;
import com.symbol.emdk.barcode.BarcodeManager.ScannerConnectionListener;
import com.symbol.emdk.barcode.ScanDataCollection;
import com.symbol.emdk.barcode.Scanner;
import com.symbol.emdk.barcode.ScannerConfig;
import com.symbol.emdk.barcode.ScannerException;
import com.symbol.emdk.barcode.ScannerInfo;
import com.symbol.emdk.barcode.ScannerResults;
import com.symbol.emdk.barcode.ScanDataCollection.ScanData;
import com.symbol.emdk.barcode.Scanner.DataListener;
import com.symbol.emdk.barcode.Scanner.StatusListener;
import com.symbol.emdk.barcode.Scanner.TriggerType;
import com.symbol.emdk.barcode.StatusData.ScannerStates;
import com.symbol.emdk.barcode.StatusData;


public class ReceiptDataDetail extends AppCompatActivity implements EMDKListener, DataListener, StatusListener, ScannerConnectionListener {
    private EMDKManager emdkManager = null;
    private BarcodeManager barcodeManager = null;
    private Scanner scanner = null;
    private Spinner spinnerScannerDevices = null;
    private List<ScannerInfo> deviceList = null;
    private final Object lock = new Object();
    private boolean bSoftTriggerSelected = false;
    private boolean bDecoderSettingsChanged = false;
    private boolean bExtScannerDisconnected = false;
    private boolean bContinuousMode = false;
    private String numberRegex = "\\d+(?:\\.\\d+)?";

    static final String WRDNUMBER = "wrdnumber";
    private RecyclerView recyclerView = null;
    private EditText edtWRDNumber = null;
    private EditText edtReferenceNumber = null;
    private EditText edtGoodsID = null;
    private EditText edtIDCode = null;
    private EditText edtQuantity = null;
    private EditText edtTotalQuantityOrg = null;
    private EditText edtTotalQuantity = null;
    private EditText edtTotalGoodsOrg = null;
    private EditText edtTotalGoods = null;
    private Button btnClearWRDNumber = null;
    private Button btnClearGoodsID = null;
    private Button btnClearIDCode = null;
    private Button btnClearQuantity = null;
    private WRDataDetailAdapter adapter;
    ArrayList<WRDataDetail> arrayList;
    ArrayList<WRDataDetail> arrayListByIDCode;
    ArrayList<WRDataHeader> wrDataHeaders;
    WRDataHeader wrDataHeader;
    IWarehouseApi iWarehouseApi = RetrofitService.getService();
    private static final String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%";
    Boolean isGoodsIDExist = false;
    Boolean isGoodsIDAndIDCodeExist = false;
    Double totalGoods = 0.0;
    int countGoodsDuplicate=0;
    Double totalQuantity = 0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receipt_data_detail);


//        EMDKResults results = EMDKManager.getEMDKManager(getApplicationContext(), this);
//        if (results.statusCode != EMDKResults.STATUS_CODE.SUCCESS) {
//            updateStatus("EMDKManager object request failed!");
//            return;
//        }


        initComponent();

        edtWRDNumber.setText("21000-00001");
//        edtGoodsID.setText("GN1P7R081BF");
//        edtIDCode.setText("1/1");

        arrayList = new ArrayList<>();
        arrayListByIDCode = new ArrayList<>();
        wrDataHeaders = new ArrayList<>();


        edtWRDNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (TextUtils.isEmpty(editable)) {
                    alertError(getApplicationContext(), "Nhập số phiếu");
                    edtWRDNumber.requestFocus();
                    return;
                }
                if (editable.length() == 11) {
                    fetchWRDataHeaderData();
                    fetchWRDataDetailData();
                    if (arrayList.size() > 0) {
                        alertSuccess(getApplicationContext(), "Lấy dữ liệu nhập thành công");
                        edtGoodsID.requestFocus();
                    } else {
                        alertError(getApplicationContext(), "Không tìm thấy chi tiết dữ liệu nhập");
                    }
                }
            }
        });

        edtGoodsID.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (TextUtils.isEmpty(edtWRDNumber.getText().toString())) {
                    alertError(getApplicationContext(), "Nhập số phiếu");
                    edtWRDNumber.requestFocus();
                    return;
                }
                if (TextUtils.isEmpty(edtGoodsID.getText().toString())) {
                    alertError(getApplicationContext(), "Scan mã hàng");
                    edtGoodsID.requestFocus();
                    return;
                }
                checkExistGoodsID(arrayList);
                if (isGoodsIDExist == false) {
                    alertError(getApplicationContext(), "Mã hàng không tồn tại");
                }
            }
        });

        edtIDCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String wrdNumber = edtWRDNumber.getText().toString();
                String goodsID = edtGoodsID.getText().toString();
                String idCode = edtIDCode.getText().toString();
                if (TextUtils.isEmpty(wrdNumber)) {
                    alertError(getApplicationContext(), "Nhập số phiếu");
                    edtWRDNumber.requestFocus();
                    return;
                }
                if (TextUtils.isEmpty(goodsID)) {
                    alertError(getApplicationContext(), "Scan mã hàng");
                    edtGoodsID.requestFocus();
                    return;
                }
                if (TextUtils.isEmpty(edtIDCode.getText().toString())) {
                    alertError(getApplicationContext(), "Scan mã định danh");
                    edtIDCode.requestFocus();
                    return;
                }
                if (editable.length() >= 3) {
                    getWRDataDetailByIdCode(wrdNumber, goodsID, idCode);
                    if (arrayListByIDCode.size() > 0) {
                        edtQuantity.requestFocus();
                    }
                }
            }
        });

        edtQuantity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (TextUtils.isEmpty(edtWRDNumber.getText().toString())) {
                    alertError(getApplicationContext(), "Nhập số phiếu");
                    edtWRDNumber.requestFocus();
                    return;
                }
                if (TextUtils.isEmpty(edtGoodsID.getText().toString())) {
                    alertError(getApplicationContext(), "Scan mã hàng");
                    edtGoodsID.requestFocus();
                    return;
                }
                if (TextUtils.isEmpty(edtIDCode.getText().toString())) {
                    alertError(getApplicationContext(), "Scan mã định danh");
                    edtIDCode.requestFocus();
                    return;
                }
                if (TextUtils.isEmpty(edtQuantity.getText().toString())) {
                    alertError(getApplicationContext(), "Scan số lượng");
                    edtQuantity.requestFocus();
                    return;
                }
                if (!edtQuantity.getText().toString().matches(numberRegex)) {
                    alertError(getApplicationContext(), "Không phải là số");
                    return;
                }
                if (arrayListByIDCode.size() > 0) {
                    WRDataDetail wrDataDetail = arrayListByIDCode.get(0);
                    checkExistGoodsIDAndIDCode(arrayListByIDCode);
                    if (isGoodsIDAndIDCodeExist) {
                        alertError(getApplicationContext(), "Mã hàng và mã định danh này đã scan");
                        isGoodsIDAndIDCodeExist=false;
                        return;
                    }
                    totalQuantity=0.0;
                    totalGoods=0.0;
                    getTotalGoods(arrayList);
                    getTotalQuantity(arrayList);
                    WRDataDetail wrDataDetailUpdate = new WRDataDetail();
                    wrDataDetailUpdate=wrDataDetail;
                    wrDataDetailUpdate.setQuantity(Double.parseDouble(edtQuantity.getText().toString()));
                    wrDataDetailUpdate.setWrdNumber(edtWRDNumber.getText().toString());
                    wrDataDetailUpdate.setGoodsID(edtGoodsID.getText().toString());
                    wrDataDetailUpdate.setIdCode(edtIDCode.getText().toString());
                    wrDataDetailUpdate.setTotalGoods(totalGoods+1);
                    wrDataDetailUpdate.setTotalQuantity(totalQuantity + Double.parseDouble(edtQuantity.getText().toString()));
                    if (editable.length() >= 1) {
                        //update wrdata detail
                        putWRDataDetail(wrDataDetailUpdate.getWrdNumber(), wrDataDetailUpdate.getGoodsID(), wrDataDetailUpdate.getOrdinal(), wrDataDetailUpdate);
                        //update wrdata header
                        WRDataHeader wrDataHeaderUpdate = wrDataHeader;
                        wrDataHeader.setTotalQuantity(totalQuantity + Double.parseDouble(edtQuantity.getText().toString()));
                        putWRDataHeader(edtWRDNumber.getText().toString(), wrDataHeaderUpdate);
                    }
                }
            }
        });

        btnClearWRDNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtWRDNumber.setText("");
                edtReferenceNumber.setText("");
            }
        });
        btnClearGoodsID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtGoodsID.setText("");
            }
        });
        btnClearIDCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtIDCode.setText("");
            }
        });
        btnClearQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtQuantity.setText("");
            }
        });
    }

    private void alertError(Context context, String message) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));
        dialog.setTitle("Error")
                .setIcon(R.drawable.ic_baseline_error_24)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                    }
                }).show();
    }

    private void alertSuccess(Context context, String message) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));
        dialog.setTitle("Success")
                .setIcon(R.drawable.ic_baseline_done_24)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                    }
                }).show();
    }

    private void initComponent() {
        recyclerView = findViewById(R.id.recycler_view);
        edtWRDNumber = findViewById(R.id.edtWRDNumber);
        edtReferenceNumber = findViewById(R.id.edtReferenceNumber);
        edtGoodsID = findViewById(R.id.edtGoodsID);
        edtIDCode = findViewById(R.id.edtIDCode);
        edtQuantity = findViewById(R.id.edtQuantity);
        btnClearWRDNumber = findViewById(R.id.btn_clear_wrdnumber);
        btnClearGoodsID = findViewById(R.id.btn_clear_goodsid);
        btnClearIDCode = findViewById(R.id.btn_clear_idcode);
        btnClearQuantity = findViewById(R.id.btn_clear_quantity);
        edtTotalQuantityOrg = findViewById(R.id.edtTotalQuantityOrg);
        edtTotalQuantity = findViewById(R.id.edtTotalQuantity);
        edtTotalGoodsOrg = findViewById(R.id.edtTotalGoodsOrg);
        edtTotalGoods = findViewById(R.id.edtTotalGoods);
    }

    private void fetchWRDataHeaderData() {
        Call<List<WRDataHeader>> callback = iWarehouseApi.getWRDataHeader(edtWRDNumber.getText().toString());
        callback.enqueue(new Callback<List<WRDataHeader>>() {
            @Override
            public void onResponse(Call<List<WRDataHeader>> call, Response<List<WRDataHeader>> response) {
                if (response.isSuccessful()) {
                    wrDataHeaders = (ArrayList<WRDataHeader>) response.body();
                    if (wrDataHeaders.size() > 0) {
                        wrDataHeader = wrDataHeaders.get(0);
                        hideSoftKeyBoard();
                        edtReferenceNumber.setText(wrDataHeader.getReferenceNumber());
                        edtTotalQuantityOrg.setText(wrDataHeader.getTotalQuantityOrg() + "");
                        edtTotalQuantity.setText(wrDataHeader.getTotalQuantity() + "");
                        edtGoodsID.requestFocus();
                    }
                } else {
                    alertError(getApplicationContext(), "Không tìm thấy số phiếu");
                }
            }

            @Override
            public void onFailure(Call<List<WRDataHeader>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetch wrdataheader fail", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void fetchWRDataDetailData() {
        Call<List<WRDataDetail>> callback = iWarehouseApi.getWRDataDetailById(edtWRDNumber.getText().toString());
        callback.enqueue(new Callback<List<WRDataDetail>>() {
            @Override
            public void onResponse(Call<List<WRDataDetail>> call, Response<List<WRDataDetail>> response) {
                if (response.isSuccessful()) {
                    arrayList = (ArrayList<WRDataDetail>) response.body();
                    if (arrayList.size() > 0) {
                        edtTotalGoodsOrg.setText(arrayList.get(0).getTotalGoodsOrg() + "");
                        getTotalGoods(arrayList);
                        edtTotalGoods.setText(totalGoods + "");
                    }
                    setAdapter(arrayList);
                }
            }

            @Override
            public void onFailure(Call<List<WRDataDetail>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetch  wrdatadetail fail", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private ArrayList<WRDataDetail> getWRDataDetailByIdCode(String wrdNumber, String goodsId, String idCode) {
        Call<List<WRDataDetail>> callback = iWarehouseApi.getWRDataDetailByIdCode(wrdNumber, goodsId, idCode);
        callback.enqueue(new Callback<List<WRDataDetail>>() {
            @Override
            public void onResponse(Call<List<WRDataDetail>> call, Response<List<WRDataDetail>> response) {
                if (response.isSuccessful()) {
                    arrayListByIDCode = new ArrayList<>();
                    arrayListByIDCode = (ArrayList<WRDataDetail>) response.body();
                    if (arrayListByIDCode.size() > 0) {
                        Toast.makeText(getApplicationContext(), "succes!!! " + arrayListByIDCode.get(0).getGoodsID() + "-" + arrayListByIDCode.get(0).getIdCode(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    alertError(getApplicationContext(), "Không tìm thấy mã định danh theo mã hàng");
                }
            }

            @Override
            public void onFailure(Call<List<WRDataDetail>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "get wrdatadetail by idcode" + t.getMessage() + "", Toast.LENGTH_SHORT).show();
            }
        });
        return arrayList;
    }

    private void putWRDataDetail(String wRDNumber, String goodsID, int ordinal, WRDataDetail wrDataDetail) {
        Call<WRDataDetail> callback = iWarehouseApi.updateWRDataDetail(wRDNumber, goodsID, ordinal, wrDataDetail);
        callback.enqueue(new Callback<WRDataDetail>() {
            @Override
            public void onResponse(Call<WRDataDetail> call, Response<WRDataDetail> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "Success!!!", Toast.LENGTH_SHORT).show();
                    fetchWRDataDetailData();
                    getTotalQuantity(arrayList);
                    getTotalGoods(arrayList);
                    edtTotalQuantity.setText(totalQuantity + "");
                    edtTotalGoods.setText(totalGoods + "");
                } else {
                    alertError(getApplicationContext(), "Cập nhật dữ liệu nhập thất bại");
                }

            }

            @Override
            public void onFailure(Call<WRDataDetail> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "put wrdatadetail fail" + t.getMessage() + "", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private ArrayList<WRDataDetail> getNotYetList(ArrayList<WRDataDetail> wrDataDetails){
        ArrayList<WRDataDetail> notYetList=new ArrayList<>();
        for(int i=0;i<wrDataDetails.size();i++){
            if(wrDataDetails.get(i).getQuantity()==0){
                notYetList.add(wrDataDetails.get(i));
            }
        }
        return notYetList;
    }

    private ArrayList<WRDataDetail> getEnoughtYetList(ArrayList<WRDataDetail> wrDataDetails){
        ArrayList<WRDataDetail> enoughYettList=new ArrayList<>();
        for(int i=0;i<wrDataDetails.size();i++){
            if(wrDataDetails.get(i).getQuantity()>0&&wrDataDetails.get(i).getQuantity()<wrDataDetails.get(i).getQuantityOrg()){
                enoughYettList.add(wrDataDetails.get(i));
            }
        }
        return enoughYettList;
    }

    private ArrayList<WRDataDetail> getEnoughtList(ArrayList<WRDataDetail> wrDataDetails){
        ArrayList<WRDataDetail> enoughtList=new ArrayList<>();
        for(int i=0;i<wrDataDetails.size();i++){
            if(wrDataDetails.get(i).getQuantity()>0&&wrDataDetails.get(i).getQuantity().doubleValue()==wrDataDetails.get(i).getQuantityOrg().doubleValue()){
                enoughtList.add(wrDataDetails.get(i));
            }
        }
        return enoughtList;
    }

    private ArrayList<WRDataDetail> getOvertList(ArrayList<WRDataDetail> wrDataDetails){
        ArrayList<WRDataDetail> overList=new ArrayList<>();
        for(int i=0;i<wrDataDetails.size();i++){
            if(wrDataDetails.get(i).getQuantity()>wrDataDetails.get(i).getQuantityOrg()){
                overList.add(wrDataDetails.get(i));
            }
        }
        return overList;
    }

    private void checkGoodsIDDuplicate(ArrayList<WRDataDetail> wrDataDetails) {
        List<String> goodsIDs = new ArrayList<>();
        for (int i = 0; i < wrDataDetails.size(); i++) {
            goodsIDs.add(wrDataDetails.get(i).getGoodsID());
        }
        HashSet<String> hashSet = new HashSet<String>();
        hashSet.addAll(goodsIDs);
        goodsIDs.clear();
        goodsIDs.addAll(hashSet);
        countGoodsDuplicate=goodsIDs.size();
    }

    private void checkExistGoodsID(ArrayList<WRDataDetail> wrDataDetails) {
        for (int i = 0; i < wrDataDetails.size(); i++) {
            if (edtGoodsID.length() >= 11 && edtGoodsID.getText().toString().equalsIgnoreCase(wrDataDetails.get(i).getGoodsID())) {
                isGoodsIDExist = true;
            }
        }
    }

    private void checkExistGoodsIDAndIDCode(ArrayList<WRDataDetail> wrDataDetails) {
        WRDataDetail wrDataDetail = wrDataDetails.get(0);
        if (wrDataDetail.getQuantity() > 0) {
            isGoodsIDAndIDCodeExist = true;
        }
    }

    private void getTotalQuantity(ArrayList<WRDataDetail> wrDataDetails) {
        for (int i = 0; i < wrDataDetails.size(); i++) {
            if (wrDataDetails.size() > 0 && wrDataDetails.get(i).getQuantity() > 0) {
                totalQuantity+=wrDataDetails.get(i).getQuantity();
            }
        }
    }

    private void getTotalGoods(ArrayList<WRDataDetail> wrDataDetails) {
        ArrayList<WRDataDetail> wrDataDetails1 = new ArrayList<>();
        for (int i = 0; i < wrDataDetails.size(); i++) {
            if (wrDataDetails.get(i).getQuantity() > 0) {
                wrDataDetails1.add(wrDataDetails.get(i));
            }
        }
        checkGoodsIDDuplicate(wrDataDetails1);
        totalGoods = Double.valueOf(countGoodsDuplicate);
    }

    private void putWRDataHeader(String wRDNumber, WRDataHeader wrDataHeader) {
        Call<WRDataHeader> callback = iWarehouseApi.updateWRDataHeader(wRDNumber, wrDataHeader);
        callback.enqueue(new Callback<WRDataHeader>() {
            @Override
            public void onResponse(Call<WRDataHeader> call, Response<WRDataHeader> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "Success!!!", Toast.LENGTH_SHORT).show();
                    fetchWRDataHeaderData();
                } else {
                    Toast.makeText(getApplicationContext(), "put wrdataheader" + response.code() + "", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<WRDataHeader> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "put wrdataheader fail" + t.getMessage() + "", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setAdapter(ArrayList<WRDataDetail> wrDataDetails) {
        adapter = new WRDataDetailAdapter(getApplicationContext(), wrDataDetails, new WRDataDetailAdapter.ItemClickListener() {
            @Override
            public void onClick(WRDataDetail wrDataDetail) {
            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

//    public void getIntentData() {
//        Intent intent = getIntent();
//        wRDNumber = intent.getStringExtra(ReceiptDataDetail.WRDNUMBER);
//    }

    private void hideSoftKeyBoard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);

        if (imm.isAcceptingText()) { // verify if the soft keyboard is open
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.action_bar_wrdatadetail_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        if (id == R.id.action_all) {
            ArrayList<WRDataDetail> allList=arrayList;
            setAdapter(allList);
            return true;
        }
        if (id == R.id.action_not_yet) {
            ArrayList<WRDataDetail> notYetList=getNotYetList(arrayList);
            setAdapter(notYetList);
            return true;
        }
        if (id == R.id.action_enought_yet) {
            ArrayList<WRDataDetail> enoughtYetLish=getEnoughtYetList(arrayList);
            setAdapter(enoughtYetLish);
            return true;
        }
        if (id == R.id.action_enought) {
            ArrayList<WRDataDetail> enoughtList=getEnoughtList(arrayList);
            setAdapter(enoughtList);
            return true;
        }
        if (id == R.id.action_over) {
            ArrayList<WRDataDetail> overList=getOvertList(arrayList);
            setAdapter(overList);
            return true;
        }
        if (id == R.id.action_save) {
            WRDataHeader wrDataHeaderUpdate = wrDataHeader;
            wrDataHeaderUpdate.setHandlingStatusID("2");
            wrDataHeaderUpdate.setHandlingStatusName("Duyệt mức 2");
            putWRDataHeader(edtWRDNumber.getText().toString(), wrDataHeaderUpdate);
            alertSuccess(getApplicationContext(),"Duyệt mức 2 nhập kho thành công");
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onOpened(EMDKManager emdkManager) {
        this.emdkManager = emdkManager;
        // Acquire the barcode manager resources
        barcodeManager = (BarcodeManager) emdkManager.getInstance(FEATURE_TYPE.BARCODE);
        // Add connection listener
        if (barcodeManager != null) {
            barcodeManager.addConnectionListener(this);
        }
        // Enumerate scanner devices
        enumerateScannerDevices();

        //
        initScanner();

        stopScan();

    }

    @Override
    protected void onResume() {
        super.onResume();
        // The application is in foreground
        if (emdkManager != null) {
            // Acquire the barcode manager resources
            initBarcodeManager();
            // Enumerate scanner devices
            enumerateScannerDevices();
            // Initialize scanner
            initScanner();
            stopScan();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        // The application is in background
        // Release the barcode manager resources
        deInitScanner();
        deInitBarcodeManager();
    }

    @Override
    public void onClosed() {
        // Release all the resources
        if (emdkManager != null) {
            emdkManager.release();
            emdkManager = null;
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Release all the resources
        if (emdkManager != null) {
            emdkManager.release();
            emdkManager = null;
        }
    }

    @Override
    public void onConnectionChange(ScannerInfo scannerInfo, ConnectionState connectionState) {
        String status;
        String scannerName = "";
        String statusExtScanner = connectionState.toString();
        String scannerNameExtScanner = scannerInfo.getFriendlyName();
        if (deviceList.size() != 0) {
            scannerName = deviceList.get(1).getFriendlyName();
        }
        if (scannerName.equalsIgnoreCase(scannerNameExtScanner)) {
            switch (connectionState) {
                case CONNECTED:
                    synchronized (lock) {
                        initScanner();
                        setDecoders();
                        bExtScannerDisconnected = false;
                    }
                    break;
                case DISCONNECTED:
                    bExtScannerDisconnected = true;
                    synchronized (lock) {
                        deInitScanner();
                    }
                    break;
            }
        } else {
            bExtScannerDisconnected = false;
        }
    }

    @Override
    public void onData(ScanDataCollection scanDataCollection) {
        if ((scanDataCollection != null) && (scanDataCollection.getResult() == ScannerResults.SUCCESS)) {
            ArrayList<ScanData> scanData = scanDataCollection.getScanData();
            for (ScanData data : scanData) {
                updateData(data.getData());
            }
        }
    }

    @Override
    public void onStatus(StatusData statusData) {
        ScannerStates state = statusData.getState();
        switch (state) {
            case IDLE:
//                statusString = statusData.getFriendlyName() + " is enabled and idle...";
//                updateStatus(statusString);
                if (bContinuousMode) {
                    try {
                        scanner.triggerType = TriggerType.SOFT_ALWAYS;
                        // An attempt to use the scanner continuously and rapidly (with a delay < 100 ms between scans)
                        // may cause the scanner to pause momentarily before resuming the scanning.
                        // Hence add some delay (>= 100ms) before submitting the next read.
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        scanner.read();
                    } catch (ScannerException e) {
                        updateStatus(e.getMessage());
                    }
                }
                // set trigger type
                if (bSoftTriggerSelected) {
                    scanner.triggerType = TriggerType.SOFT_ONCE;
                    bSoftTriggerSelected = false;
                } else {
                    scanner.triggerType = TriggerType.HARD;
                }
                // set decoders
                if (bDecoderSettingsChanged) {
                    setDecoders();
                    bDecoderSettingsChanged = false;
                } else {
                    setDecoders();
                }
                // submit read
                if (!scanner.isReadPending() && !bExtScannerDisconnected) {
                    try {
                        scanner.read();
                    } catch (ScannerException e) {
                    }
                }
                break;
            case WAITING:
                break;
            case SCANNING:
                break;
            case DISABLED:
                break;
            case ERROR:
                break;
            default:
                break;
        }
    }

    private void initScanner() {
        if (scanner == null) {
            if ((deviceList != null) && (deviceList.size() != 0)) {
                if (barcodeManager != null)
                    scanner = barcodeManager.getDevice(deviceList.get(1));
            } else {
//                updateStatus("Failed to get the specified scanner device! Please close and restart the application.");
                return;
            }
            if (scanner != null) {
                scanner.addDataListener(this);
                scanner.addStatusListener(this);
                try {
                    scanner.enable();
                } catch (ScannerException e) {
                    updateStatus(e.getMessage());
                    deInitScanner();
                }
            } else {
//                updateStatus("Failed to initialize the scanner device.");
            }
        }
    }

    private void deInitScanner() {
        if (scanner != null) {
            try {
                scanner.disable();
            } catch (Exception e) {
//                updateStatus(e.getMessage());
            }

            try {
                scanner.removeDataListener(this);
                scanner.removeStatusListener(this);
            } catch (Exception e) {
            }
            scanner = null;
        }
    }

    private void initBarcodeManager() {
        barcodeManager = (BarcodeManager) emdkManager.getInstance(FEATURE_TYPE.BARCODE);
        // Add connection listener
        if (barcodeManager != null) {
            barcodeManager.addConnectionListener(this);
        }
    }

    private void deInitBarcodeManager() {
        if (emdkManager != null) {
            emdkManager.release(FEATURE_TYPE.BARCODE);
        }
    }


    private void enumerateScannerDevices() {
        if (barcodeManager != null) {
            deviceList = barcodeManager.getSupportedDevicesInfo();
            if ((deviceList != null) && (deviceList.size() != 0)) {

            } else {
            }
        }
    }

    private void setDecoders() {
        if (scanner != null) {
            try {
                ScannerConfig config = scanner.getConfig();
                // Set EAN8
                config.decoderParams.ean8.enabled = true;
                // Set EAN13
                config.decoderParams.ean13.enabled = true;
                // Set Code39
                config.decoderParams.code39.enabled = true;
                //Set Code128
                config.decoderParams.code128.enabled = true;
                //set inverser1Mode
                config.readerParams.readerSpecific.laserSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                config.readerParams.readerSpecific.cameraSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                config.readerParams.readerSpecific.imagerSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                scanner.setConfig(config);
            } catch (ScannerException e) {
                updateStatus(e.getMessage());
            }
        }
    }

    private void setReaderParams() {
        if (scanner != null) {
            try {
                ScannerConfig config = scanner.getConfig();
                config.readerParams.readerSpecific.laserSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                scanner.setConfig(config);
            } catch (ScannerException e) {
                updateStatus(e.getMessage());
            }
        }
    }

    private void stopScan() {

        if (scanner != null) {
            // Cancel the pending read.
            try {
                scanner.cancelRead();
            } catch (ScannerException e) {
                e.printStackTrace();
            }
        }
    }

    private void cancelRead() {
        if (scanner != null) {
            if (scanner.isReadPending()) {
                try {
                    scanner.cancelRead();
                } catch (ScannerException e) {
                }
            }
        }
    }

    private void updateStatus(final String status) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
            }
        });
    }

    private void updateData(final String result) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (result != null) {
                    if (edtGoodsID.hasFocus()) {
                        edtGoodsID.setText(result);
                        if (isGoodsIDExist) {
                            edtIDCode.requestFocus();
                        }
                    } else if (edtIDCode.hasFocus()) {
                        edtIDCode.setText(result);
                        if (isGoodsIDAndIDCodeExist == false) {
                            edtQuantity.requestFocus();
                        }
                    } else if (edtQuantity.hasFocus()) {
                        edtQuantity.setText(result);
                    }
                }
            }
        });
    }
}