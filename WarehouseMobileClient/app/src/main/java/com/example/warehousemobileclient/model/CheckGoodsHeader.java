package com.example.warehousemobileclient.model;


import java.util.Date;

public class CheckGoodsHeader {
    private String id;
    private Date checkDate;
    private int totalQuantity;

    public CheckGoodsHeader() {
    }

    public CheckGoodsHeader(String id, Date checkDate, int totalQuantity) {
        this.id = id;
        this.checkDate = checkDate;
        this.totalQuantity = totalQuantity;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getCheckDate() {
        return checkDate;
    }

    public void setCheckDate(Date checkDate) {
        this.checkDate = checkDate;
    }

    public int getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(int totalQuantity) {
        this.totalQuantity = totalQuantity;
    }
}
