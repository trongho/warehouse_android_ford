package com.example.warehousemobileclient.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.warehousemobileclient.R;
import com.example.warehousemobileclient.model.WRDataHeader;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class WRDataHeaderAdapter extends RecyclerView.Adapter<WRDataHeaderAdapter.WRDataHeaderViewHolder>{
    @NonNull
    @NotNull
    private final Context context;
    private List<WRDataHeader> wrDataHeaders = new ArrayList<>();
    private final OnItemClickListener<WRDataHeader> onCategoryClickListener;

    public WRDataHeaderAdapter(@NonNull Context context, OnItemClickListener<WRDataHeader> onCategoryClickListener) {
        this.context = context;
        this.onCategoryClickListener = onCategoryClickListener;
    }

    @Override
    public WRDataHeaderAdapter.WRDataHeaderViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        return new WRDataHeaderViewHolder(LayoutInflater.from(context).inflate(R.layout.wrdataheader_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull WRDataHeaderAdapter.WRDataHeaderViewHolder holder, int position) {
        holder.setWRDataHeaderItem(wrDataHeaders.get(position));
    }

    @Override
    public int getItemCount() {
        return wrDataHeaders == null ? 0 : wrDataHeaders.size();
    }

    public void setCategories(List<WRDataHeader> wRDataHeaders) {
        this.wrDataHeaders = wRDataHeaders;
        this.notifyDataSetChanged();
    }

    public class WRDataHeaderViewHolder extends RecyclerView.ViewHolder {
        private final TextView wRDNumber;
        private final View wRDataHeaderItem;
        public WRDataHeaderViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            wRDNumber = itemView.findViewById(R.id.textWRDNumber);
            this.wRDataHeaderItem=itemView;
        }
        private void setWRDataHeaderItem(WRDataHeader wrDataHeader){
            wRDNumber.setText(wrDataHeader.getWrdNumber());
            wRDataHeaderItem.setOnClickListener(view -> onCategoryClickListener.onItemClicked(view, wrDataHeader));
        }
    }

    public interface OnItemClickListener<T> {
        void onItemClicked(View view, T data);
    }
}
