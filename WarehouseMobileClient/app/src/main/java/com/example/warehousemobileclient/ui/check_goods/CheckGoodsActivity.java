package com.example.warehousemobileclient.ui.check_goods;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.warehousemobileclient.R;
import com.example.warehousemobileclient.adapter.CGDataGenerallAdapter;
import com.example.warehousemobileclient.adapter.CheckGoodsAdapter;
import com.example.warehousemobileclient.adapter.WRDataDetailAdapter2;
import com.example.warehousemobileclient.dao.CheckGoodsDetailDAO;
import com.example.warehousemobileclient.model.CGDataGeneral;
import com.example.warehousemobileclient.model.CGDataHeader;
import com.example.warehousemobileclient.model.CheckGoodsDetail;
import com.example.warehousemobileclient.model.GoodsData;
import com.example.warehousemobileclient.model.WRDataDetail;
import com.example.warehousemobileclient.model.WRDataGeneral;
import com.example.warehousemobileclient.model.WRDataHeader;
import com.example.warehousemobileclient.service.IWarehouseApi;
import com.example.warehousemobileclient.service.RetrofitService;
import com.gtranslate.Audio;
import com.gtranslate.Language;
import com.gtranslate.Translator;
import com.symbol.emdk.EMDKManager;
import com.symbol.emdk.EMDKResults;
import com.symbol.emdk.barcode.BarcodeManager;
import com.symbol.emdk.barcode.ScanDataCollection;
import com.symbol.emdk.barcode.Scanner;
import com.symbol.emdk.barcode.ScannerConfig;
import com.symbol.emdk.barcode.ScannerException;
import com.symbol.emdk.barcode.ScannerInfo;
import com.symbol.emdk.barcode.ScannerResults;
import com.symbol.emdk.barcode.StatusData;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javazoom.jl.decoder.JavaLayerException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckGoodsActivity extends AppCompatActivity implements EMDKManager.EMDKListener, Scanner.DataListener, Scanner.StatusListener, BarcodeManager.ScannerConnectionListener {
    private EMDKManager emdkManager = null;
    private BarcodeManager barcodeManager = null;
    private Scanner scanner = null;
    private Spinner spinnerScannerDevices = null;
    private List<ScannerInfo> deviceList = null;
    private final Object lock = new Object();
    private boolean bSoftTriggerSelected = false;
    private boolean bDecoderSettingsChanged = false;
    private boolean bExtScannerDisconnected = false;
    private boolean bContinuousMode = false;

    private String numberRegex = "\\d+(?:\\.\\d+)?";

    private RecyclerView recyclerView = null;
    private EditText edtGoodsID = null;
    private Button btnClearGoodsID = null;
    private Button btnCheckNew = null;
    private CheckGoodsAdapter adapter;
    ArrayList<GoodsData> goodsDataArrayList;
    GoodsData goodsData = null;
    private static final String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%";

    Boolean isGoodsIDExist = false;
    Boolean isCheckOk = false;

    private CheckGoodsDetailDAO checkGoodsDetailDAO = null;
    private CheckGoodsDetail checkGoodsDetail = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_goods);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Kiểm tra hàng");

        EMDKResults results = EMDKManager.getEMDKManager(getApplicationContext(), this);
        if (results.statusCode != EMDKResults.STATUS_CODE.SUCCESS) {
            updateStatus("EMDKManager object request failed!");
            return;
        }
        initComponent();
        goodsDataArrayList = new ArrayList<>();
        goodsData = new GoodsData();
        checkGoodsDetailDAO = new CheckGoodsDetailDAO(CheckGoodsActivity.this);
        checkGoodsDetail = new CheckGoodsDetail();

        edtGoodsID.requestFocus();

        btnClearGoodsID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtGoodsID.setText("");
            }
        });

        btnCheckNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clear();
            }
        });
    }

    private void initComponent() {
        recyclerView = findViewById(R.id.recycler_view);
        edtGoodsID = findViewById(R.id.edtGoodsID);
        btnClearGoodsID = findViewById(R.id.btn_clear_goodsid);
        btnCheckNew = findViewById(R.id.btnCheckNew);
    }


    private Boolean checkExistGoodsID(String goodsID, ArrayList<GoodsData> goodsDataArrayList) {
        for (int i = 0; i < goodsDataArrayList.size(); i++) {
            if (goodsDataArrayList.size() > 0 && goodsID.equalsIgnoreCase(goodsDataArrayList.get(i).getGoodsID())) {
                isGoodsIDExist = true;
                break;
            }
        }
        return isGoodsIDExist;
    }

    private Boolean getIsCheckOk(ArrayList<GoodsData> goodsDataArrayList) {
        for (int i = 0; i < goodsDataArrayList.size(); i++) {
            if (goodsDataArrayList.size() > 0 && goodsDataArrayList.get(i).getStatus().equalsIgnoreCase("checkOK")) {
                isCheckOk = true;
                break;
            }
        }
        return isCheckOk;
    }

    private void alertError(Context context, String message) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));
        dialog.setTitle("Error")
                .setIcon(R.drawable.ic_baseline_error_24)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                    }
                }).show();
    }

    private void alertSuccess(Context context, String message) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));
        dialog.setTitle("Success")
                .setIcon(R.drawable.ic_baseline_done_24)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                    }
                }).show();
    }

    private void setAdapter(ArrayList<GoodsData> goodsData) {
        adapter = new CheckGoodsAdapter(getApplicationContext(), goodsData, new CheckGoodsAdapter.ItemClickListener() {
            @Override
            public void onClick(GoodsData goodsData1) {
            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void hideSoftKeyBoard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);

        if (imm.isAcceptingText()) { // verify if the soft keyboard is open
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.action_bar_listcheckgoods_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        if (id == R.id.action_list) {
            startActivity(new Intent(CheckGoodsActivity.this, ListCheckGoodsActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onOpened(EMDKManager emdkManager) {
        this.emdkManager = emdkManager;
        // Acquire the barcode manager resources
        barcodeManager = (BarcodeManager) emdkManager.getInstance(EMDKManager.FEATURE_TYPE.BARCODE);
        // Add connection listener
        if (barcodeManager != null) {
            barcodeManager.addConnectionListener(this);
        }
        // Enumerate scanner devices
        enumerateScannerDevices();

        //
        initScanner();

        stopScan();

    }

    @Override
    protected void onResume() {
        super.onResume();
        // The application is in foreground
        if (emdkManager != null) {
            // Acquire the barcode manager resources
            initBarcodeManager();
            // Enumerate scanner devices
            enumerateScannerDevices();
            // Initialize scanner
            initScanner();
            stopScan();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        // The application is in background
        // Release the barcode manager resources
        deInitScanner();
        deInitBarcodeManager();
    }

    @Override
    public void onClosed() {
        // Release all the resources
        if (emdkManager != null) {
            emdkManager.release();
            emdkManager = null;
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Release all the resources
        if (emdkManager != null) {
            emdkManager.release();
            emdkManager = null;
        }
    }

    @Override
    public void onConnectionChange(ScannerInfo scannerInfo, BarcodeManager.ConnectionState connectionState) {
        String status;
        String scannerName = "";
        String statusExtScanner = connectionState.toString();
        String scannerNameExtScanner = scannerInfo.getFriendlyName();
        if (deviceList.size() != 0) {
            scannerName = deviceList.get(1).getFriendlyName();
        }
        if (scannerName.equalsIgnoreCase(scannerNameExtScanner)) {
            switch (connectionState) {
                case CONNECTED:
                    synchronized (lock) {
                        initScanner();
                        setDecoders();
                        bExtScannerDisconnected = false;
                    }
                    break;
                case DISCONNECTED:
                    bExtScannerDisconnected = true;
                    synchronized (lock) {
                        deInitScanner();
                    }
                    break;
            }
        } else {
            bExtScannerDisconnected = false;
        }
    }

    @Override
    public void onData(ScanDataCollection scanDataCollection) {
        if ((scanDataCollection != null) && (scanDataCollection.getResult() == ScannerResults.SUCCESS)) {
            ArrayList<ScanDataCollection.ScanData> scanData = scanDataCollection.getScanData();
            for (ScanDataCollection.ScanData data : scanData) {
                updateData(data.getData());
            }
        }
    }

    @Override
    public void onStatus(StatusData statusData) {
        StatusData.ScannerStates state = statusData.getState();
        switch (state) {
            case IDLE:
//                statusString = statusData.getFriendlyName() + " is enabled and idle...";
//                updateStatus(statusString);
                if (bContinuousMode) {
                    try {
                        scanner.triggerType = Scanner.TriggerType.SOFT_ALWAYS;
                        // An attempt to use the scanner continuously and rapidly (with a delay < 100 ms between scans)
                        // may cause the scanner to pause momentarily before resuming the scanning.
                        // Hence add some delay (>= 100ms) before submitting the next read.
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        scanner.read();
                    } catch (ScannerException e) {
                        updateStatus(e.getMessage());
                    }
                }
                // set trigger type
                if (bSoftTriggerSelected) {
                    scanner.triggerType = Scanner.TriggerType.SOFT_ONCE;
                    bSoftTriggerSelected = false;
                } else {
                    scanner.triggerType = Scanner.TriggerType.HARD;
                }
                // set decoders
                if (bDecoderSettingsChanged) {
                    setDecoders();
                    bDecoderSettingsChanged = false;
                } else {
                    setDecoders();
                }
                // submit read
                if (!scanner.isReadPending() && !bExtScannerDisconnected) {
                    try {
                        scanner.read();
                    } catch (ScannerException e) {
                    }
                }
                break;
            case WAITING:
                break;
            case SCANNING:
                break;
            case DISABLED:
                break;
            case ERROR:
                break;
            default:
                break;
        }
    }

    private void initScanner() {
        if (scanner == null) {
            if ((deviceList != null) && (deviceList.size() != 0)) {
                if (barcodeManager != null)
                    scanner = barcodeManager.getDevice(deviceList.get(1));
            } else {
//                updateStatus("Failed to get the specified scanner device! Please close and restart the application.");
                return;
            }
            if (scanner != null) {
                scanner.addDataListener(this);
                scanner.addStatusListener(this);
                try {
                    scanner.enable();
                } catch (ScannerException e) {
                    updateStatus(e.getMessage());
                    deInitScanner();
                }
            } else {
//                updateStatus("Failed to initialize the scanner device.");
            }
        }
    }

    private void deInitScanner() {
        if (scanner != null) {
            try {
                scanner.disable();
            } catch (Exception e) {
//                updateStatus(e.getMessage());
            }

            try {
                scanner.removeDataListener(this);
                scanner.removeStatusListener(this);
            } catch (Exception e) {
//                updateStatus(e.getMessage());
            }

            try {
                scanner.release();
            } catch (Exception e) {
//                updateStatus(e.getMessage());
            }
            scanner = null;
        }
    }

    private void initBarcodeManager() {
        barcodeManager = (BarcodeManager) emdkManager.getInstance(EMDKManager.FEATURE_TYPE.BARCODE);
        // Add connection listener
        if (barcodeManager != null) {
            barcodeManager.addConnectionListener(this);
        }
    }

    private void deInitBarcodeManager() {
        if (emdkManager != null) {
            emdkManager.release(EMDKManager.FEATURE_TYPE.BARCODE);
        }
    }


    private void enumerateScannerDevices() {
        if (barcodeManager != null) {
            deviceList = barcodeManager.getSupportedDevicesInfo();
            if ((deviceList != null) && (deviceList.size() != 0)) {

            } else {
            }
        }
    }

    private void setDecoders() {
        if (scanner != null) {
            try {
                ScannerConfig config = scanner.getConfig();
                // Set EAN8
                config.decoderParams.ean8.enabled = true;
                // Set EAN13
                config.decoderParams.ean13.enabled = true;
                // Set Code39
                config.decoderParams.code39.enabled = true;
                //Set Code128
                config.decoderParams.code128.enabled = true;
                //set inverser1Mode
                config.readerParams.readerSpecific.laserSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                config.readerParams.readerSpecific.cameraSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                config.readerParams.readerSpecific.imagerSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                scanner.setConfig(config);
            } catch (ScannerException e) {
                updateStatus(e.getMessage());
            }
        }
    }

    private void setReaderParams() {
        if (scanner != null) {
            try {
                ScannerConfig config = scanner.getConfig();
                config.readerParams.readerSpecific.laserSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                scanner.setConfig(config);
            } catch (ScannerException e) {
                updateStatus(e.getMessage());
            }
        }
    }

    private void stopScan() {

        if (scanner != null) {
            // Cancel the pending read.
            try {
                scanner.cancelRead();
            } catch (ScannerException e) {
                e.printStackTrace();
            }
        }
    }

    private void cancelRead() {
        if (scanner != null) {
            if (scanner.isReadPending()) {
                try {
                    scanner.cancelRead();
                } catch (ScannerException e) {
                }
            }
        }
    }

    private void updateStatus(final String status) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
            }
        });
    }

    private void updateData(final String result) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (result != null) {
                    String goodsID = "";
                    int index1 = result.indexOf("-");
                    if (index1 > 0) {
                        goodsID = result.substring(0, index1);
                    } else {
                        goodsID = result;
                    }
                    edtGoodsID.setText(goodsID);

                    isCheckOk = false;
                    getIsCheckOk(goodsDataArrayList);
                    if (isCheckOk == false) {
                        process();
                    } else {
                        goodsDataArrayList.clear();
                        setAdapter(goodsDataArrayList);
                        process();
                    }
                }
            }
        });
    }

    private void process() {
        if (TextUtils.isEmpty(edtGoodsID.getText().toString())) {
            alertError(getApplicationContext(), "Scan mã hàng");
            edtGoodsID.requestFocus();
            return;
        }

        isGoodsIDExist = false;
        checkExistGoodsID(edtGoodsID.getText().toString(), goodsDataArrayList);
        if (goodsDataArrayList.size() > 0) {
            if (isGoodsIDExist == false) {
                alertError(getApplicationContext(), "Mã hàng không trùng");
                return;
            } else {
                isGoodsIDExist = false;
            }
            goodsDataArrayList.clear();
            goodsData.setGoodsID(edtGoodsID.getText().toString());
            goodsData.setStatus("checkOK");
            goodsDataArrayList.add(goodsData);

            checkGoodsDetail.setGoodsID(edtGoodsID.getText().toString());
            addCheckGoodsDetail(checkGoodsDetail);

            setAdapter(goodsDataArrayList);

            try {
                Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                r.play();
            } catch (Exception e) {
                e.printStackTrace();
            }
//            try {
//                AssetFileDescriptor afd = null;
//                afd = getAssets().openFd("Ok.mp3");
//                MediaPlayer player = new MediaPlayer();
//                player.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
//                player.prepare();
//                player.start();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }

        } else {
            goodsData.setGoodsID(edtGoodsID.getText().toString());
            goodsData.setStatus("");
            goodsDataArrayList.add(goodsData);
            setAdapter(goodsDataArrayList);
        }
    }

    private void clear() {
        goodsDataArrayList.clear();
        setAdapter(goodsDataArrayList);
        edtGoodsID.setText("");
        edtGoodsID.requestFocus();
    }

    private void addCheckGoodsDetail(CheckGoodsDetail checkGoodsDetail) {
        checkGoodsDetailDAO.insert(checkGoodsDetail);
    }
}