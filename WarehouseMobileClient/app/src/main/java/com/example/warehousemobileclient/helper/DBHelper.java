package com.example.warehousemobileclient.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {
    public final static String DBNAME = "WMSANDROID";
    public final static int DBVERSION = 1;

    public DBHelper(Context context) {
        super(context, DBNAME, null, DBVERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String sql;
        //creat wrdatageneral table
        sql = "CREATE TABLE WRDATAGENERAL(" +
                "ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                "WRDNUMBER TEXT NOT NULL," +
                "GOODSID TEXT NOT NULL," +
                "DATETIME TEXT NOT NULL," +
                "STATUS TEXT)";
        sqLiteDatabase.execSQL(sql);

        //creat wrdatageneral table
        sql = "CREATE TABLE WIDATAGENERAL(" +
                "ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                "WIDNUMBER TEXT NOT NULL," +
                "GOODSID TEXT NOT NULL," +
                "GOODSGROUPID TEXT NOT NULL," +
                "DATETIME TEXT NOT NULL," +
                "STATUS TEXT)";
        sqLiteDatabase.execSQL(sql);

//        //
//        sql = "CREATE TABLE CHECKGOODSHEADER(" +
//                "ID INTEGER PRIMARY KEY AUTOINCREMENT," +
//                "CHECKDATE DATE NOT NULL," +
//                "TOTALQUANTITY INTEGER NOT NULL)";
//        sqLiteDatabase.execSQL(sql);

        //
        sql = "CREATE TABLE CHECKGOODSDETAIL(" +
                "ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                "CHECKDATE DATE NOT NULL," +
                "GOODSID TEXT NOT NULL)";
        sqLiteDatabase.execSQL(sql);

        sql = "CREATE TABLE COUNTGOODS(" +
                "ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                "LOCATIONID TEXT NOT NULL," +
                "GOODSID TEXT NOT NULL," +
                "GOODSNAME TEXT," +
                "QUANTITY REAL NOT NULL," +
                "STATUS TEXT NOT NULL," +
                "COUNTDATE DATE NOT NULL)";
        sqLiteDatabase.execSQL(sql);

        sql = "CREATE TABLE LOCATION(" +
                "ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                "LOCATIONID TEXT NOT NULL)";
        sqLiteDatabase.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        String sql;
        sql = "DROP TABLE IF EXISTS WRDATAGENERAL";
        sqLiteDatabase.execSQL(sql);
        sql = "DROP TABLE IF EXISTS WIDATAGENERAL";
        sqLiteDatabase.execSQL(sql);
//        sql = "DROP TABLE IF EXISTS CHECKGOODSHEADER";
//        sqLiteDatabase.execSQL(sql);
        sql = "DROP TABLE IF EXISTS CHECKGOODSDETAIL";
        sqLiteDatabase.execSQL(sql);
        sql = "DROP TABLE IF EXISTS COUNTGOODS";
        sqLiteDatabase.execSQL(sql);
        sql = "DROP TABLE IF EXISTS LOCATION";
        sqLiteDatabase.execSQL(sql);
        onCreate(sqLiteDatabase);
    }
}
