package com.example.warehousemobileclient.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.warehousemobileclient.R;
import com.example.warehousemobileclient.model.TallyData;
import com.example.warehousemobileclient.model.WIDataGeneral;
import com.example.warehousemobileclient.service.IWarehouseApi;
import com.example.warehousemobileclient.service.RetrofitService;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TallyDataAdapter extends RecyclerView.Adapter<TallyDataAdapter.ViewHolder> {
    Context context;
    ArrayList<TallyData> list;
    TallyData entry;
    TallyDataAdapter.ItemClickListener listener;
    TallyDataAdapter.ItemActionListener actionListener;
    SimpleDateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    public TallyDataAdapter(Context context, ArrayList<TallyData> list, TallyDataAdapter.ItemClickListener listener,ItemActionListener actionListener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
        this.actionListener=actionListener;
    }

    @NonNull
    @Override
    public TallyDataAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context)
                .inflate(R.layout.tally_data_item, viewGroup, false);
        // gan cac thuoc tinh nhu size, margins, paddings.....
        return new TallyDataAdapter.ViewHolder(v);
    }

    @SuppressLint("ResourceType")
    @Override
    public void onBindViewHolder(@NonNull TallyDataAdapter.ViewHolder viewHolder, @SuppressLint("RecyclerView") final int i) {
        entry = list.get(i);
        viewHolder.tvGoodsID.setText(entry.getGoodsID());
        viewHolder.tvQuantity.setText(entry.getQuantity() + "");
        viewHolder.tvNo.setText(entry.getNo()+".");
        viewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onClick(list.get(i));
                }
            }
        });
        viewHolder.ivAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (actionListener != null) {
                    actionListener.onClick(list.get(i),i);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvGoodsID, tvQuantity,tvNo;
        public ImageView ivAction;
        public CardView cardView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvGoodsID = itemView.findViewById(R.id.tvGoodsID);
            tvQuantity = itemView.findViewById(R.id.tvQuantity);
            tvNo=itemView.findViewById(R.id.tvNo);
            ivAction=itemView.findViewById(R.id.action);
            cardView = itemView.findViewById(R.id.carView);
        }
    }

    public void updateList(List<TallyData> lists) {
        this.list.clear();
        this.list.addAll(lists);
    }

    public interface ItemClickListener {
        void onClick(TallyData tallyData);
    }
    public interface ItemActionListener {
        void onClick(TallyData tallyData,int position);
    }


}
