package com.example.warehousemobileclient.service;

import com.example.warehousemobileclient.model.CGDataGeneral;
import com.example.warehousemobileclient.model.CGDataHeader;
import com.example.warehousemobileclient.model.CountGoods;
import com.example.warehousemobileclient.model.Location;
import com.example.warehousemobileclient.model.TSDetail;
import com.example.warehousemobileclient.model.TSHeader;
import com.example.warehousemobileclient.model.TallyData;
import com.example.warehousemobileclient.model.User;
import com.example.warehousemobileclient.model.WIDataDetail;
import com.example.warehousemobileclient.model.WIDataGeneral;
import com.example.warehousemobileclient.model.WIDataHeader;
import com.example.warehousemobileclient.model.WRDataDetail;
import com.example.warehousemobileclient.model.WRDataGeneral;
import com.example.warehousemobileclient.model.WRDataHeader;

import java.util.List;

import okhttp3.Callback;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface IWarehouseApi {
    @GET("wrdataheader")
    Call<List<WRDataHeader>> getWRDataHeaders();
    @GET("wrdataheader/GetByID/{id}")
    Call<List<WRDataHeader>> getWRDataHeader(@Path("id") String id);
    @PUT("wrdataheader/Put/{wRDNumber}")
    Call<WRDataHeader> updateWRDataHeader(@Path("wRDNumber") String wRDNumber,@Body WRDataHeader body);

    @GET("wrdatadetail/GetByID/{id}")
    Call<List<WRDataDetail>> getWRDataDetailById(@Path("id") String id);
    @GET("wrdatadetail/GetByIDCode/{wRDNumber}/{goodsID}/{idCode}")
    Call<List<WRDataDetail>> getWRDataDetailByIdCode(@Path("wRDNumber") String wRDNumber,@Path("goodsID") String goodsID,@Path("idCode") String idCode);
    @PUT("wrdatadetail/Put/{wRDNumber}/{goodsID}/{ordinal}")
    Call<WRDataDetail> updateWRDataDetail(@Path("wRDNumber") String wRDNumber, @Path("goodsID") String goodsID, @Path("ordinal") int ordinal, @Body WRDataDetail body);

    @GET("wrdatageneral/getByID/{id}")
    Call<List<WRDataGeneral>> getWRDataGeneralById(@Path("id") String id);
    @PUT("wrdatageneral/Put/{wRDNumber}/{goodsID}/{ordinal}")
    Call<WRDataGeneral> updateWRDataGeneral(@Path("wRDNumber") String wRDNumber, @Path("goodsID") String goodsID, @Path("ordinal") int ordinal, @Body WRDataGeneral body);
    @GET("wrdatageneral/GetByWRDNumberAndGoodsID/{wRDNumber}/{goodsID}")
    Call<List<WRDataGeneral>> getWRDataGeneralByGoods(@Path("wRDNumber") String wRDNumber,@Path("goodsID") String goodsID);
    @GET("wrdatageneral/GetByIDCode/{wRDNumber}/{goodsID}/{idCode}")
    Call<List<WRDataGeneral>> getWRDataGeneralByIDCode(@Path("wRDNumber") String wRDNumber,@Path("goodsID") String goodsID,@Path("idCode") String idCode);
    @GET("wrdatageneral/CheckIDCodeIsScaned/{wRDNumber}/{goodsID}/{idCode}")
    Call<Boolean> CheckIDCodeIsScaned(@Path("wRDNumber") String wRDNumber,@Path("goodsID") String goodsID,@Path("idCode") String idCode);
    @GET("wrdatageneral/CheckExist/{wRDNumber}/{goodsID}")
    Call<Boolean> CheckExist(@Path("wRDNumber") String wRDNumber,@Path("goodsID") String goodsID);
    @GET("wrdatageneral/CheckGoodsIDDuplicate/{wRDNumber}/{goodsID}")
    Call<Boolean> CheckGoodsIDDuplicate(@Path("wRDNumber") String wRDNumber,@Path("goodsID") String goodsID);
    @GET("wrdatageneral/GetTotalQuantity/{wRDNumber}")
    Call<Double> GetTotalQuantity(@Path("wRDNumber") String wRDNumber);
    @GET("wrdatageneral/GetTotalGoods/{wRDNumber}")
    Call<Double> GetTotalGoods(@Path("wRDNumber") String wRDNumber);


    @GET("widataheader")
    Call<List<WIDataHeader>> getWIDataHeaders();
    @GET("widataheader/GetByID/{id}")
    Call<List<WIDataHeader>> getWIDataHeader(@Path("id") String id);
    @PUT("widataheader/Put/{wIDNumber}")
    Call<WIDataHeader> updateWIDataHeader(@Path("wIDNumber") String wIDNumber,@Body WIDataHeader body);

    @GET("widatadetail/GetByID/{id}")
    Call<List<WIDataDetail>> getWIDataDetailById(@Path("id") String id);
    @GET("widatadetail/GetByIDCode/{wIDNumber}/{goodsID}/{idCode}")
    Call<List<WIDataDetail>> getWIDataDetailByIdCode(@Path("wIDNumber") String wIDNumber,@Path("goodsID") String goodsID,@Path("idCode") String idCode);
    @PUT("widatadetail/Put/{wIDNumber}/{goodsID}/{ordinal}")
    Call<WIDataDetail> updateWIDataDetail(@Path("wIDNumber") String wIDNumber, @Path("goodsID") String goodsID, @Path("ordinal") int ordinal, @Body WIDataDetail body);

    @GET("widatageneral/getByID/{id}")
    Call<List<WIDataGeneral>> getWIDataGeneralById(@Path("id") String id);
    @PUT("widatageneral/Put/{wIDNumber}/{goodsID}/{ordinal}/{goodsGroupID}")
    Call<WIDataGeneral> updateWIDataGeneral(@Path("wIDNumber") String wIDNumber, @Path("goodsID") String goodsID, @Path("ordinal") int ordinal, @Path("goodsGroupID") String goodsGroupID, @Body WIDataGeneral body);
    @GET("widatageneral/GetByWIDNumberAndGoodsID/{wIDNumber}/{goodsID}")
    Call<List<WIDataGeneral>> getWIDataGeneralByGoods(@Path("wIDNumber") String wIDNumber,@Path("goodsID") String goodsID);
    @GET("widatageneral/getGoodsGroupIDs/{id}")
    Call<List<String>> getGoodsGroupIDs(@Path("id") String id);
    @GET("widatageneral/getPickerNames/{id}")
    Call<List<String>> getPickerNames(@Path("id") String id);
    @GET("widatageneral/FilterByGoodsGroupIDAndpickerName/{WIDNumber}/{GoodsGroupID}/{PickerName}")
    Call<List<WIDataGeneral>> FilterByGoodsGroupIDAndpickerName(@Path("WIDNumber") String WIDNumber,@Path("GoodsGroupID") String GoodsGroupID,@Path("PickerName") String PickerName);
    @GET("widatageneral/GetByPickerName/{WIDNumber}/{PickerName}")
    Call<List<WIDataGeneral>> GetByPickerName(@Path("WIDNumber") String WIDNumber,@Path("PickerName") String PickerName);
    @GET("widatageneral/GetByGoodsGroupID/{WIDNumber}/{GoodsGroupID}")
    Call<List<WIDataGeneral>> GetByGoodsGroupID(@Path("WIDNumber") String WIDNumber,@Path("GoodsGroupID") String GoodsGroupID);
    @GET("widatageneral/GetByScanOption/{WIDNumber}/{ScanOption}")
    Call<List<WIDataGeneral>> GetByScanOption(@Path("WIDNumber") String WIDNumber,@Path("ScanOption") String ScanOption);
    @GET("widatageneral/FilterByGoodsGroupIDAndScanOption/{WIDNumber}/{GoodsGroupID}/{ScanOption}")
    Call<List<WIDataGeneral>> FilterByGoodsGroupIDAndScanOption(@Path("WIDNumber") String WIDNumber,@Path("GoodsGroupID") String GoodsGroupID,@Path("ScanOption") String ScanOption);
    @GET("widatageneral/FilterByPickerNameAndScanOption/{WIDNumber}/{PickerName}/{ScanOption}")
    Call<List<WIDataGeneral>> FilterByPickerNameAndScanOption(@Path("WIDNumber") String WIDNumber,@Path("PickerName") String PickerName,@Path("ScanOption") String ScanOption);
    @GET("widatageneral/Filter/{WIDNumber}/{GoodsGroupID}/{PickerName}/{ScanOption}")
    Call<List<WIDataGeneral>> Filter(@Path("WIDNumber") String WIDNumber,@Path("GoodsGroupID") String GoodsGroupID,@Path("PickerName") String PickerName,@Path("ScanOption") String ScanOption);
    @GET("widatageneral/GetByGoodsGroupIDAndGoodsID/{wIDNumber}/{goodsID}/{ordinal}/{goodsGroupID}")
    Call<List<WIDataGeneral>> GetByGoodsGroupIDAndGoodsID(@Path("wIDNumber") String wIDNumber,@Path("goodsID") String goodsID,@Path("ordinal") String ordinal,@Path("goodsGroupID") String goodsGroupID);
    @GET("widatageneral/GetByWIDNumberAndGoodsIDAndGoodsGroupID/{wIDNumber}/{goodsID}/{goodsGroupID}")
    Call<List<WIDataGeneral>> GetByWIDNumberAndGoodsIDAndGoodsGroupID(@Path("wIDNumber") String wIDNumber,@Path("goodsID") String goodsID,@Path("goodsGroupID") String goodsGroupID);


    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST("user/login")
    Call<User> login(@Body User body);

    @GET("tallydata/GetByID/{id}")
    Call<List<TallyData>> getTallyDataById(@Path("id") String id);
    @GET("tallydata/lastID")
    Call<String> getLastTSNumber();
    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST("tallydata/Post")
    Call<TallyData> creatTallyData(@Body TallyData body);
    @PUT("tallydata/Put/{tsNumber}/{GoodsID}/{No}")
    Call<TallyData> updateTallyData(@Path("tsNumber") String tsNumber,@Path("GoodsID") String goodsID,@Path("No") int no,@Body TallyData body);
    @DELETE("tallydata/Delete/{tsNumber}")
    Call<Boolean> deleteTallyData(@Path("tsNumber") String tsNumber);
    @DELETE("tallydata/DeleteDetail/{tsNumber}/{goodsID}/{No}")
    Call<Boolean> deleteTallyDataDetail(@Path("tsNumber") String tsNumber,@Path("goodsID") String goodsID,@Path("No") int No);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST("tsheader/Post")
    Call<TSHeader> creatTSHeader(@Body TSHeader body);
    @PUT("tsheader/Put/{tsNumber}")
    Call<TSHeader> updateTSHeader(@Path("tsNumber") String tsNumber,@Body TSHeader body);
    @GET("tsheader/GetByID/{id}")
    Call<List<TSHeader>> getTSHeaderById(@Path("id") String id);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST("tsdetail/Post")
    Call<TSDetail> creatTSDetail(@Body TSDetail body);
    @PUT("tsdetail/Put/{tsNumber}/{GoodsID}/{Ordinal}")
    Call<TSDetail> updateTSDetail(@Path("tsNumber") String tsNumber,@Path("GoodsID") String goodsID,@Path("Ordinal") int ordinal,@Body TSDetail body);
    @GET("tsdetail/GetByMultiID/{TSNumber}/{GoodsID}/{Ordinal}")
    Call<List<TSDetail>> getTSDetailByMultiId(@Path("TSNumber") String tsNumber,@Path("GoodsID") String goodsID,@Path("Ordinal") int ordinal);
    @GET("tsdetail/GetByID/{id}")
    Call<List<TSDetail>> getTSDetailById(@Path("id") String id);

    @GET("cgdataheader")
    Call<List<CGDataHeader>> getCGDataHeaders();
    @GET("cgdataheader/GetByID/{id}")
    Call<List<CGDataHeader>> getCGDataHeader(@Path("id") String id);
    @PUT("cgdataheader/Put/{cgDNumber}")
    Call<CGDataHeader> updateCGDataHeader(@Path("cgDNumber") String cgDNumber,@Body CGDataHeader body);

    @GET("cgdatageneral/getByID/{id}")
    Call<List<CGDataGeneral>> getCGDataGeneralById(@Path("id") String id);
    @PUT("cgdatageneral/Put/{cgDNumber}/{goodsID}/{ordinal}")
    Call<CGDataGeneral> updateCGDataGeneral(@Path("cgDNumber") String cgDNumber, @Path("goodsID") String goodsID, @Path("ordinal") int ordinal, @Body CGDataGeneral body);
    @GET("cgdatageneral/GetByCGDNumberAndGoodsID/{cgDNumber}/{goodsID}")
    Call<List<CGDataGeneral>> getCGDataGeneralByGoods(@Path("cgDNumber") String cgDNumber,@Path("goodsID") String goodsID);

    @GET("location")
    Call<List<Location>> getLocations();

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST("countgoods/Post")
    Call<CountGoods> creatCountGoods(@Body CountGoods body);
    @PUT("countgoods/Put/{locationID}/{goodsID}")
    Call<CountGoods> updateCountGoods(@Path("locationID") String locationID, @Path("goodsID") String goodsID, @Body CountGoods body);
    @GET("countgoods/GetByID/{locationID}")
    Call<List<CountGoods>> GetCountGoodsByID(@Path("locationID") String locationID);
    @GET("countgoods/GetByMultiID/{locationID}/{goodsID}")
    Call<List<CountGoods>> GetCountGoodsByMultiID(@Path("locationID") String locationID, @Path("goodsID") String goodsID);
    @DELETE("countgoods/DeleteByID/{locationID}")
    Call<Boolean> DeleteByLocationID(@Path("locationID") String locationID);
    @DELETE("countgoods/DeleteByMultiID/{locationID}/{goodsID}")
    Call<Boolean> DeleteByMultiID(@Path("locationID") String locationID,@Path("goodsID") String goodsID);
}

