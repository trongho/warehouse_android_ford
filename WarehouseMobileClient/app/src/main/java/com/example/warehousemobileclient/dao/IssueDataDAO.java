package com.example.warehousemobileclient.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ParseException;

import com.example.warehousemobileclient.helper.DBHelper;
import com.example.warehousemobileclient.model.GoodsDataIssue;

import java.util.ArrayList;

public class IssueDataDAO {
    DBHelper dbHelper;

    public IssueDataDAO(Context context) {
        dbHelper = new DBHelper(context);
    }

    public ArrayList<GoodsDataIssue> getAll() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        ArrayList<GoodsDataIssue> list = new ArrayList<>();
        String sql = "SELECT * FROM WIDATAGENERAL";
        Cursor c = db.rawQuery(sql, null);
        c.moveToFirst();
        while (!c.isAfterLast()) {
            try {
                String WIDNumber = c.getString(1);
                String goodsID = c.getString(2);
                String goodsGroupID=c.getString(3);
                String date = c.getString(4);
                String status = c.getString(5);
                list.add(new GoodsDataIssue(WIDNumber,goodsID,goodsGroupID,date,status));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            c.moveToNext();
        }
        return list;
    }

    public ArrayList<GoodsDataIssue> getByWIDNumber(String WIDNumber) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        ArrayList<GoodsDataIssue> list = new ArrayList<>();
        String sql = "SELECT * FROM WIDATAGENERAL WHERE WIDNUMBER=? ";
        Cursor c = db.rawQuery(sql, new String[]{WIDNumber});
        c.moveToFirst();
        while (!c.isAfterLast()) {
            try {
                String widNumber = c.getString(1);
                String goodsID = c.getString(2);
                String goodsGroupID=c.getString(3);
                String date = c.getString(4);
                String status = c.getString(5);
                list.add(new GoodsDataIssue(widNumber,goodsID,goodsGroupID,date,status));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            c.moveToNext();
        }
        return list;
    }

    public long insert(GoodsDataIssue GoodsDataIssue) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("WIDNumber",GoodsDataIssue.getWidNumber());
        values.put("GOODSID",GoodsDataIssue.getGoodsID());
        values.put("GOODSGROUPID",GoodsDataIssue.getGoodsGroupID());
        values.put("DATETIME",GoodsDataIssue.getDate());
        values.put("STATUS",GoodsDataIssue.getStatus());
        return db.insert("WIDATAGENERAL", null, values);
    }

    public int update(GoodsDataIssue GoodsDataIssue) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("WIDNumber",GoodsDataIssue.getWidNumber());
        values.put("GOODSID",GoodsDataIssue.getGoodsID());
        values.put("GOODSGROUPID",GoodsDataIssue.getGoodsGroupID());
        values.put("DATETIME",GoodsDataIssue.getDate());
        values.put("STATUS",GoodsDataIssue.getStatus());
        return db.update("WIDATAGENERAL", values, "WIDNUMBER=? AND GOODSID=? AND GOODSGROUPID=? AND DATETIME=?", new String[]{GoodsDataIssue.getWidNumber(),GoodsDataIssue.getGoodsID(),GoodsDataIssue.getGoodsGroupID(),GoodsDataIssue.getDate()});
    }

    public int delete(GoodsDataIssue GoodsDataIssue) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        return db.delete("WIDATAGENERAL","WIDNUMBER=? AND GOODSID=? AND GOODSGROUPID=? AND DATETIME=?", new String[]{GoodsDataIssue.getWidNumber(),GoodsDataIssue.getGoodsID(),GoodsDataIssue.getGoodsGroupID(),GoodsDataIssue.getDate()});
    }

    public int deleteByWIDNumber(String WIDNumber) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        return db.delete("WIDATAGENERAL","WIDNUMBER=?", new String[]{WIDNumber});
    }

    public int deleteAll() {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        return db.delete("WIDATAGENERAL", null, null);
    }
}
