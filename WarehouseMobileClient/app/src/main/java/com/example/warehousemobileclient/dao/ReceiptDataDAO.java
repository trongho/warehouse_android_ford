package com.example.warehousemobileclient.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ParseException;

import com.example.warehousemobileclient.helper.DBHelper;
import com.example.warehousemobileclient.model.GoodsData;

import java.util.ArrayList;
import java.util.Date;

public class ReceiptDataDAO {
    DBHelper dbHelper;

    public ReceiptDataDAO(Context context) {
        dbHelper = new DBHelper(context);
    }

    public ArrayList<GoodsData> getAll() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        ArrayList<GoodsData> list = new ArrayList<>();
        String sql = "SELECT * FROM WRDATAGENERAL";
        Cursor c = db.rawQuery(sql, null);
        c.moveToFirst();
        while (!c.isAfterLast()) {
            try {
                String wrdNumber = c.getString(1);
                String goodsID = c.getString(2);
                String date = c.getString(3);
                String status = c.getString(4);
                list.add(new GoodsData(wrdNumber,goodsID,date,status));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            c.moveToNext();
        }
        return list;
    }

    public ArrayList<GoodsData> getByWRDNumber(String WRDNumber) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        ArrayList<GoodsData> list = new ArrayList<>();
        String sql = "SELECT * FROM WRDATAGENERAL WHERE WRDNUMBER=? ";
        Cursor c = db.rawQuery(sql, new String[]{WRDNumber});
        c.moveToFirst();
        while (!c.isAfterLast()) {
            try {
                String wrdNumber = c.getString(1);
                String goodsID = c.getString(2);
                String date = c.getString(3);
                String status = c.getString(4);
                list.add(new GoodsData(wrdNumber,goodsID,date,status));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            c.moveToNext();
        }
        return list;
    }

    public long insert(GoodsData goodsData) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("WRDNUMBER",goodsData.getWrdNumber());
        values.put("GOODSID",goodsData.getGoodsID());
        values.put("DATETIME",goodsData.getDate());
        values.put("STATUS",goodsData.getStatus());
        return db.insert("WRDATAGENERAL", null, values);
    }

    public int update(GoodsData goodsData) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("WRDNUMBER",goodsData.getWrdNumber());
        values.put("GOODSID",goodsData.getGoodsID());
        values.put("DATETIME",goodsData.getDate());
        values.put("STATUS",goodsData.getStatus());
        return db.update("WRDATAGENERAL", values, "WRDNUMBER=? AND GOODSID=? AND DATETIME=?", new String[]{goodsData.getWrdNumber(),goodsData.getGoodsID(),goodsData.getDate()});
    }

    public int delete(GoodsData goodsData) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        return db.delete("WRDATAGENERAL","WRDNUMBER=? AND GOODSID=? AND DATETIME=?", new String[]{goodsData.getWrdNumber(),goodsData.getGoodsID(),goodsData.getDate()});
    }

    public int deleteByWRDNumber(String wrdNumber) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        return db.delete("WRDATAGENERAL","WRDNUMBER=?", new String[]{wrdNumber});
    }

    public int deleteAll() {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        return db.delete("WRDATAGENERAL", null, null);
    }
}
