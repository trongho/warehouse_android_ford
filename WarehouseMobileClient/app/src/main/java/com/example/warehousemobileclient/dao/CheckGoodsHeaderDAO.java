package com.example.warehousemobileclient.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ParseException;

import com.example.warehousemobileclient.helper.DBHelper;
import com.example.warehousemobileclient.model.CheckGoodsHeader;
import com.example.warehousemobileclient.model.GoodsData;
import com.example.warehousemobileclient.model.GoodsDataIssue;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class CheckGoodsHeaderDAO {
    DBHelper dbHelper;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public CheckGoodsHeaderDAO(Context context) {
        dbHelper = new DBHelper(context);
    }

    public ArrayList<CheckGoodsHeader> getAll() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        ArrayList<CheckGoodsHeader> list = new ArrayList<>();
        String sql = "SELECT * FROM CHECKGOODSHEADER";
        Cursor c = db.rawQuery(sql, null);
        c.moveToFirst();
        while (!c.isAfterLast()) {
            try {
                String id = c.getString(0);
                Date checkDate = simpleDateFormat.parse(c.getString(1));
                int totalQuantity = Integer.parseInt(c.getString(2));
                list.add(new CheckGoodsHeader(id,checkDate,totalQuantity));
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }
            c.moveToNext();
        }
        return list;
    }

    public ArrayList<CheckGoodsHeader> getByID(String ID) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        ArrayList<CheckGoodsHeader> list = new ArrayList<>();
        String sql = "SELECT * FROM CHECKGOODSHEADER WHERE ID=? ";
        Cursor c = db.rawQuery(sql, new String[]{ID});
        c.moveToFirst();
        while (!c.isAfterLast()) {
            try {
                String id = c.getString(0);
                Date checkDate = simpleDateFormat.parse(c.getString(1));
                int totalQuantity = Integer.parseInt(c.getString(2));
                list.add(new CheckGoodsHeader(id,checkDate,totalQuantity));
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }
            c.moveToNext();
        }
        return list;
    }

    public ArrayList<CheckGoodsHeader> getByMonth(String month){
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        ArrayList<CheckGoodsHeader> list = new ArrayList<>();
        String sql = "SELECT * FROM CHECKGOODSHEADER WHERE STRFTIME('%m',CHECKGOODSHEADER.CHECKDATE)=?";
        Cursor c = db.rawQuery(sql, new String[]{month});
        c.moveToFirst();
        while (!c.isAfterLast()) {
            try {
                String id = c.getString(0);
                Date checkDate = simpleDateFormat.parse(c.getString(1));
                int totalQuantity = Integer.parseInt(c.getString(2));
                list.add(new CheckGoodsHeader(id,checkDate,totalQuantity));
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }
            c.moveToNext();
        }
        return list;
    }

    public ArrayList<CheckGoodsHeader> getByDay(String day){
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        ArrayList<CheckGoodsHeader> list = new ArrayList<>();
        String sql = "SELECT * FROM CHECKGOODSHEADER WHERE STRFTIME('%d',CHECKGOODSHEADER.CHECKDATE)=?";
        Cursor c = db.rawQuery(sql, new String[]{day});
        c.moveToFirst();
        while (!c.isAfterLast()) {
            try {
                String id = c.getString(0);
                Date checkDate = simpleDateFormat.parse(c.getString(1));
                int totalQuantity = Integer.parseInt(c.getString(2));
                list.add(new CheckGoodsHeader(id,checkDate,totalQuantity));
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }
            c.moveToNext();
        }
        return list;
    }

    public ArrayList<CheckGoodsHeader> getByMonthDay(String month,String day){
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        ArrayList<CheckGoodsHeader> list = new ArrayList<>();
        String sql = "SELECT * FROM SESSION WHERE STRFTIME('%m-%d',CHECKGOODSHEADER.CHECKDATE)=?";
        Cursor c = db.rawQuery(sql, new String[]{month+"-"+day});
        c.moveToFirst();
        while (!c.isAfterLast()) {
            try {
                String id = c.getString(0);
                Date checkDate = simpleDateFormat.parse(c.getString(1));
                int totalQuantity = Integer.parseInt(c.getString(2));
                list.add(new CheckGoodsHeader(id,checkDate,totalQuantity));
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }
            c.moveToNext();
        }
        return list;
    }

    public long insert(CheckGoodsHeader checkGoodsHeader) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("CHECKDATE", String.valueOf(checkGoodsHeader.getCheckDate()));
        values.put("TOTALQUANTITY",checkGoodsHeader.getTotalQuantity());
        return db.insert("CHECKGOODSHEADER", null, values);
    }

    public int update(CheckGoodsHeader checkGoodsHeader) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("CHECKDATE", String.valueOf(checkGoodsHeader.getCheckDate()));
        values.put("TOTALQUANTITY",checkGoodsHeader.getTotalQuantity());
        return db.update("CHECKGOODSHEADER", values, "ID=?", new String[]{checkGoodsHeader.getId()});
    }

    public int deleteByID(String id) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        return db.delete("CHECKGOODSHEADER","ID=?", new String[]{id});
    }
}
