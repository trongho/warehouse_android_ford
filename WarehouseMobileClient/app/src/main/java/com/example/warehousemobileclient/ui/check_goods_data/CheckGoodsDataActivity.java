package com.example.warehousemobileclient.ui.check_goods_data;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.warehousemobileclient.R;
import com.example.warehousemobileclient.adapter.CGDataGenerallAdapter;
import com.example.warehousemobileclient.adapter.WRDataDetailAdapter2;
import com.example.warehousemobileclient.model.CGDataGeneral;
import com.example.warehousemobileclient.model.CGDataHeader;
import com.example.warehousemobileclient.model.WRDataDetail;
import com.example.warehousemobileclient.model.WRDataGeneral;
import com.example.warehousemobileclient.model.WRDataHeader;
import com.example.warehousemobileclient.service.IWarehouseApi;
import com.example.warehousemobileclient.service.RetrofitService;
import com.symbol.emdk.EMDKManager;
import com.symbol.emdk.EMDKResults;
import com.symbol.emdk.barcode.BarcodeManager;
import com.symbol.emdk.barcode.ScanDataCollection;
import com.symbol.emdk.barcode.Scanner;
import com.symbol.emdk.barcode.ScannerConfig;
import com.symbol.emdk.barcode.ScannerException;
import com.symbol.emdk.barcode.ScannerInfo;
import com.symbol.emdk.barcode.ScannerResults;
import com.symbol.emdk.barcode.StatusData;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckGoodsDataActivity extends AppCompatActivity implements EMDKManager.EMDKListener, Scanner.DataListener, Scanner.StatusListener, BarcodeManager.ScannerConnectionListener {
    private EMDKManager emdkManager = null;
    private BarcodeManager barcodeManager = null;
    private Scanner scanner = null;
    private Spinner spinnerScannerDevices = null;
    private List<ScannerInfo> deviceList = null;
    private final Object lock = new Object();
    private boolean bSoftTriggerSelected = false;
    private boolean bDecoderSettingsChanged = false;
    private boolean bExtScannerDisconnected = false;
    private boolean bContinuousMode = false;

    private String numberRegex = "\\d+(?:\\.\\d+)?";

    private RecyclerView recyclerView = null;
    private EditText edtCGDNumber = null;
    private EditText edtGoodsID = null;
    private EditText edtTotalGoodsOrg = null;
    private EditText edtTotalGoods = null;
    private Button btnClearWRDNumber = null;
    private Button btnClearGoodsID = null;
    private CGDataGenerallAdapter adapter;
    ArrayList<CGDataGeneral> cgDataGenerals;
    ArrayList<CGDataGeneral> arrayListByGoodsID;
    ArrayList<CGDataHeader> cgDataHeaders;
    CGDataHeader cgDataHeader;
    IWarehouseApi iWarehouseApi = RetrofitService.getService();
    private static final String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%";

    Boolean isGoodsIDExist = false;
    Boolean isGoodsIdDuplicate = false;
    Double totalGoods = 0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_goods_data);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("ĐIền hàng");

        EMDKResults results = EMDKManager.getEMDKManager(getApplicationContext(), this);
        if (results.statusCode != EMDKResults.STATUS_CODE.SUCCESS) {
            updateStatus("EMDKManager object request failed!");
            return;
        }
        initComponent();
        cgDataHeaders = new ArrayList<>();
        cgDataGenerals = new ArrayList<>();
        arrayListByGoodsID = new ArrayList<>();

        edtCGDNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (TextUtils.isEmpty(editable)) {
                    alertError(getApplicationContext(), "Nhập số phiếu");
                    edtCGDNumber.requestFocus();
                    return;
                }
                if (editable.length() >= 1) {
                    Call<List<CGDataHeader>> callback = iWarehouseApi.getCGDataHeader(edtCGDNumber.getText().toString());
                    callback.enqueue(new Callback<List<CGDataHeader>>() {
                        @Override
                        public void onResponse(Call<List<CGDataHeader>> call, Response<List<CGDataHeader>> response) {
                            if (response.isSuccessful()) {
                                cgDataHeaders = (ArrayList<CGDataHeader>) response.body();
                                if (cgDataHeaders.size() > 0) {
                                    alertSuccess(getApplicationContext(), "Lấy dữ liệu thành công");
                                    cgDataHeader = cgDataHeaders.get(0);
                                    fetchCGDataGeneralData(edtCGDNumber.getText().toString());
                                    hideSoftKeyBoard();
                                    edtGoodsID.requestFocus();
                                }

                            }
                        }

                        @Override
                        public void onFailure(Call<List<CGDataHeader>> call, Throwable t) {
                            Toast.makeText(getApplicationContext(), "Fetch wrdataheader fail", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });

        edtGoodsID.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (TextUtils.isEmpty(edtCGDNumber.getText().toString())) {
                    alertError(getApplicationContext(), "Nhập số phiếu");
                    edtCGDNumber.requestFocus();
                    return;
                }
            }
        });

        btnClearWRDNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtCGDNumber.setText("");
            }
        });
        btnClearGoodsID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtGoodsID.setText("");
            }
        });
    }

    private void initComponent() {
        recyclerView = findViewById(R.id.recycler_view);
        edtCGDNumber = findViewById(R.id.edtCGDNumber);
        edtGoodsID = findViewById(R.id.edtGoodsID);
        btnClearWRDNumber = findViewById(R.id.btn_clear_wrdnumber);
        btnClearGoodsID = findViewById(R.id.btn_clear_goodsid);
        edtTotalGoodsOrg = findViewById(R.id.edtTotalGoodsOrg);
        edtTotalGoods = findViewById(R.id.edtTotalGoods);
    }

    private void clear() {
        edtGoodsID.setText("");
    }

    private CGDataHeader fetchCGDataHeaderData(String cgdNumber) {
        Call<List<CGDataHeader>> callback = iWarehouseApi.getCGDataHeader(cgdNumber);
        callback.enqueue(new Callback<List<CGDataHeader>>() {
            @Override
            public void onResponse(Call<List<CGDataHeader>> call, Response<List<CGDataHeader>> response) {
                if (response.isSuccessful()) {
                    cgDataHeaders = (ArrayList<CGDataHeader>) response.body();
                    if (cgDataHeaders.size() > 0) {
                        cgDataHeader = cgDataHeaders.get(0);
                        hideSoftKeyBoard();
                        edtGoodsID.requestFocus();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<CGDataHeader>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetch fail", Toast.LENGTH_SHORT).show();
            }
        });
        return cgDataHeader;
    }

    private ArrayList<CGDataGeneral> fetchCGDataGeneralData(String cgdNumber) {
        Call<List<CGDataGeneral>> callback = iWarehouseApi.getCGDataGeneralById(cgdNumber);
        callback.enqueue(new Callback<List<CGDataGeneral>>() {
            @Override
            public void onResponse(Call<List<CGDataGeneral>> call, Response<List<CGDataGeneral>> response) {
                if (response.isSuccessful()) {
                    cgDataGenerals = (ArrayList<CGDataGeneral>) response.body();
                    if (cgDataGenerals.size() > 0) {
                        edtTotalGoodsOrg.setText(cgDataGenerals.get(0).getTotalGoodsOrg() + "");
                        totalGoods = 0.0;
                        getTotalGoods(cgDataGenerals);
                        edtTotalGoods.setText(totalGoods + "");
                    }
                    setAdapter(cgDataGenerals);
                }
            }

            @Override
            public void onFailure(Call<List<CGDataGeneral>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetch  wrdatadetail fail", Toast.LENGTH_SHORT).show();
            }
        });
        return cgDataGenerals;
    }

    private void putCGDataHeader(String cgDNumber, CGDataHeader cgDataHeader) {
        Call<CGDataHeader> callback = iWarehouseApi.updateCGDataHeader(cgDNumber, cgDataHeader);
        callback.enqueue(new Callback<CGDataHeader>() {
            @Override
            public void onResponse(Call<CGDataHeader> call, Response<CGDataHeader> response) {
                if (response.isSuccessful()) {
                    fetchCGDataHeaderData(cgDNumber);
                }
            }

            @Override
            public void onFailure(Call<CGDataHeader> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "put fail" + t.getMessage() + "", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void putCGDataGeneral(String cgDNumber, String goodsID, int ordinal, CGDataGeneral cgDataGeneral) {
        Call<CGDataGeneral> callback = iWarehouseApi.updateCGDataGeneral(cgDNumber, goodsID, ordinal, cgDataGeneral);
        callback.enqueue(new Callback<CGDataGeneral>() {
            @Override
            public void onResponse(Call<CGDataGeneral> call, Response<CGDataGeneral> response) {
                if (response.isSuccessful()) {
                    fetchCGDataGeneralData(cgDNumber);
                    getTotalGoods(cgDataGenerals);
                    edtTotalGoods.setText(totalGoods + "");
                } else {
                    alertError(getApplicationContext(), "Cập nhật dữ liệu nhập thất bại");
                }

            }

            @Override
            public void onFailure(Call<CGDataGeneral> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "put wrdatadetail fail" + t.getMessage() + "", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getTotalGoods(ArrayList<CGDataGeneral> cgDataGenerals) {
        for (int i = 0; i < cgDataGenerals.size(); i++) {
            if (cgDataGenerals.get(i).getStatus().equalsIgnoreCase("checkOK")) {
                totalGoods += 1;
            }
        }
    }

    private Boolean checkGoodsIDDuplicate(ArrayList<CGDataGeneral> cgDataGenerals, String goodsID) {
        for (int i = 0; i < cgDataGenerals.size(); i++) {
            if (cgDataGenerals.get(i).getStatus().equalsIgnoreCase("checkOK") && cgDataGenerals.get(i).getGoodsID().equalsIgnoreCase(goodsID)) {
                isGoodsIdDuplicate = true;
                break;
            }
        }
        return isGoodsIdDuplicate;
    }

    private Boolean checkExistGoodsID(String goodsID, ArrayList<CGDataGeneral> cgDataGenerals) {
        for (int i = 0; i < cgDataGenerals.size(); i++) {
            if (goodsID.equalsIgnoreCase(cgDataGenerals.get(i).getGoodsID())) {
                isGoodsIDExist = true;
                break;
            }
        }
        return isGoodsIDExist;
    }

    private ArrayList<CGDataGeneral> getNotYetList(ArrayList<CGDataGeneral> cgDataGenerals) {
        ArrayList<CGDataGeneral> notYetList = new ArrayList<>();
        for (int i = 0; i < cgDataGenerals.size(); i++) {
            if (cgDataGenerals.get(i).getStatus().equalsIgnoreCase("")) {
                notYetList.add(cgDataGenerals.get(i));
            }
        }
        return notYetList;
    }


    private ArrayList<CGDataGeneral> getEnoughtList(ArrayList<CGDataGeneral> cgDataGenerals) {
        ArrayList<CGDataGeneral> enoughtList = new ArrayList<>();
        for (int i = 0; i < cgDataGenerals.size(); i++) {
            if (cgDataGenerals.get(i).getStatus().equalsIgnoreCase("checkOK")) {
                enoughtList.add(cgDataGenerals.get(i));
            }
        }
        return enoughtList;
    }

    private void alertError(Context context, String message) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));
        dialog.setTitle("Error")
                .setIcon(R.drawable.ic_baseline_error_24)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                    }
                }).show();
    }

    private void alertSuccess(Context context, String message) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));
        dialog.setTitle("Success")
                .setIcon(R.drawable.ic_baseline_done_24)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                    }
                }).show();
    }

    private void setAdapter(ArrayList<CGDataGeneral> cgDataGenerals) {
        adapter = new CGDataGenerallAdapter(getApplicationContext(), cgDataGenerals, new CGDataGenerallAdapter.ItemClickListener() {
            @Override
            public void onClick(CGDataGeneral cgDataGeneral) {
            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void hideSoftKeyBoard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);

        if (imm.isAcceptingText()) { // verify if the soft keyboard is open
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.action_bar_cgdatageneral_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        if (id == R.id.action_all) {
            ArrayList<CGDataGeneral> allList = cgDataGenerals;
            setAdapter(allList);
            return true;
        }
        if (id == R.id.action_not_check) {
            ArrayList<CGDataGeneral> notYetList = getNotYetList(cgDataGenerals);
            setAdapter(notYetList);
            return true;
        }
        if (id == R.id.action_checked) {
            ArrayList<CGDataGeneral> enoughtList = getEnoughtList(cgDataGenerals);
            setAdapter(enoughtList);
            return true;
        }
        if (id == R.id.action_save) {
            process();
            return true;
        }
        if (id == R.id.action_approve) {
            if (cgDataHeader != null) {
                CGDataHeader cgDataHeaderUpdate = cgDataHeader;
                cgDataHeaderUpdate.setHandlingStatusID("2");
                cgDataHeaderUpdate.setHandlingStatusName("Duyệt mức 2");
                putCGDataHeader(edtCGDNumber.getText().toString(), cgDataHeaderUpdate);
                alertSuccess(getApplicationContext(), "Duyệt mức 2 điền hàng thành công");
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onOpened(EMDKManager emdkManager) {
        this.emdkManager = emdkManager;
        // Acquire the barcode manager resources
        barcodeManager = (BarcodeManager) emdkManager.getInstance(EMDKManager.FEATURE_TYPE.BARCODE);
        // Add connection listener
        if (barcodeManager != null) {
            barcodeManager.addConnectionListener(this);
        }
        // Enumerate scanner devices
        enumerateScannerDevices();

        //
        initScanner();

        stopScan();

    }

    @Override
    protected void onResume() {
        super.onResume();
        // The application is in foreground
        if (emdkManager != null) {
            // Acquire the barcode manager resources
            initBarcodeManager();
            // Enumerate scanner devices
            enumerateScannerDevices();
            // Initialize scanner
            initScanner();
            stopScan();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        // The application is in background
        // Release the barcode manager resources
        deInitScanner();
        deInitBarcodeManager();
    }

    @Override
    public void onClosed() {
        // Release all the resources
        if (emdkManager != null) {
            emdkManager.release();
            emdkManager = null;
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Release all the resources
        if (emdkManager != null) {
            emdkManager.release();
            emdkManager = null;
        }
    }

    @Override
    public void onConnectionChange(ScannerInfo scannerInfo, BarcodeManager.ConnectionState connectionState) {
        String status;
        String scannerName = "";
        String statusExtScanner = connectionState.toString();
        String scannerNameExtScanner = scannerInfo.getFriendlyName();
        if (deviceList.size() != 0) {
            scannerName = deviceList.get(1).getFriendlyName();
        }
        if (scannerName.equalsIgnoreCase(scannerNameExtScanner)) {
            switch (connectionState) {
                case CONNECTED:
                    synchronized (lock) {
                        initScanner();
                        setDecoders();
                        bExtScannerDisconnected = false;
                    }
                    break;
                case DISCONNECTED:
                    bExtScannerDisconnected = true;
                    synchronized (lock) {
                        deInitScanner();
                    }
                    break;
            }
        } else {
            bExtScannerDisconnected = false;
        }
    }

    @Override
    public void onData(ScanDataCollection scanDataCollection) {
        if ((scanDataCollection != null) && (scanDataCollection.getResult() == ScannerResults.SUCCESS)) {
            ArrayList<ScanDataCollection.ScanData> scanData = scanDataCollection.getScanData();
            for (ScanDataCollection.ScanData data : scanData) {
                updateData(data.getData());
            }
        }
    }

    @Override
    public void onStatus(StatusData statusData) {
        StatusData.ScannerStates state = statusData.getState();
        switch (state) {
            case IDLE:
//                statusString = statusData.getFriendlyName() + " is enabled and idle...";
//                updateStatus(statusString);
                if (bContinuousMode) {
                    try {
                        scanner.triggerType = Scanner.TriggerType.SOFT_ALWAYS;
                        // An attempt to use the scanner continuously and rapidly (with a delay < 100 ms between scans)
                        // may cause the scanner to pause momentarily before resuming the scanning.
                        // Hence add some delay (>= 100ms) before submitting the next read.
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        scanner.read();
                    } catch (ScannerException e) {
                        updateStatus(e.getMessage());
                    }
                }
                // set trigger type
                if (bSoftTriggerSelected) {
                    scanner.triggerType = Scanner.TriggerType.SOFT_ONCE;
                    bSoftTriggerSelected = false;
                } else {
                    scanner.triggerType = Scanner.TriggerType.HARD;
                }
                // set decoders
                if (bDecoderSettingsChanged) {
                    setDecoders();
                    bDecoderSettingsChanged = false;
                } else {
                    setDecoders();
                }
                // submit read
                if (!scanner.isReadPending() && !bExtScannerDisconnected) {
                    try {
                        scanner.read();
                    } catch (ScannerException e) {
                    }
                }
                break;
            case WAITING:
                break;
            case SCANNING:
                break;
            case DISABLED:
                break;
            case ERROR:
                break;
            default:
                break;
        }
    }

    private void initScanner() {
        if (scanner == null) {
            if ((deviceList != null) && (deviceList.size() != 0)) {
                if (barcodeManager != null)
                    scanner = barcodeManager.getDevice(deviceList.get(1));
            } else {
//                updateStatus("Failed to get the specified scanner device! Please close and restart the application.");
                return;
            }
            if (scanner != null) {
                scanner.addDataListener(this);
                scanner.addStatusListener(this);
                try {
                    scanner.enable();
                } catch (ScannerException e) {
                    updateStatus(e.getMessage());
                    deInitScanner();
                }
            } else {
//                updateStatus("Failed to initialize the scanner device.");
            }
        }
    }

    private void deInitScanner() {
        if (scanner != null) {
            try {
                scanner.disable();
            } catch (Exception e) {
//                updateStatus(e.getMessage());
            }

            try {
                scanner.removeDataListener(this);
                scanner.removeStatusListener(this);
            } catch (Exception e) {
//                updateStatus(e.getMessage());
            }

            try {
                scanner.release();
            } catch (Exception e) {
//                updateStatus(e.getMessage());
            }
            scanner = null;
        }
    }

    private void initBarcodeManager() {
        barcodeManager = (BarcodeManager) emdkManager.getInstance(EMDKManager.FEATURE_TYPE.BARCODE);
        // Add connection listener
        if (barcodeManager != null) {
            barcodeManager.addConnectionListener(this);
        }
    }

    private void deInitBarcodeManager() {
        if (emdkManager != null) {
            emdkManager.release(EMDKManager.FEATURE_TYPE.BARCODE);
        }
    }


    private void enumerateScannerDevices() {
        if (barcodeManager != null) {
            deviceList = barcodeManager.getSupportedDevicesInfo();
            if ((deviceList != null) && (deviceList.size() != 0)) {

            } else {
            }
        }
    }

    private void setDecoders() {
        if (scanner != null) {
            try {
                ScannerConfig config = scanner.getConfig();
                // Set EAN8
                config.decoderParams.ean8.enabled = true;
                // Set EAN13
                config.decoderParams.ean13.enabled = true;
                // Set Code39
                config.decoderParams.code39.enabled = true;
                //Set Code128
                config.decoderParams.code128.enabled = true;
                //set inverser1Mode
                config.readerParams.readerSpecific.laserSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                config.readerParams.readerSpecific.cameraSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                config.readerParams.readerSpecific.imagerSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                scanner.setConfig(config);
            } catch (ScannerException e) {
                updateStatus(e.getMessage());
            }
        }
    }

    private void setReaderParams() {
        if (scanner != null) {
            try {
                ScannerConfig config = scanner.getConfig();
                config.readerParams.readerSpecific.laserSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                scanner.setConfig(config);
            } catch (ScannerException e) {
                updateStatus(e.getMessage());
            }
        }
    }

    private void stopScan() {

        if (scanner != null) {
            // Cancel the pending read.
            try {
                scanner.cancelRead();
            } catch (ScannerException e) {
                e.printStackTrace();
            }
        }
    }

    private void cancelRead() {
        if (scanner != null) {
            if (scanner.isReadPending()) {
                try {
                    scanner.cancelRead();
                } catch (ScannerException e) {
                }
            }
        }
    }

    private void updateStatus(final String status) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
            }
        });
    }

    private void updateData(final String result) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (result != null) {
                    if(edtCGDNumber.hasFocus()){
                        edtCGDNumber.setText(result);
                    }
                    else {
                        String goodsID = "";
                        int index1 = result.indexOf("-");
//                        int index2 = result.indexOf("-", index1 + 1);
                        if (index1 > 0) {
                            goodsID = result.substring(0, index1);
                        }
                        edtGoodsID.setText(goodsID);
                        process();
                    }
                }
            }
        });
    }

    private void process() {
        if (TextUtils.isEmpty(edtGoodsID.getText().toString())) {
            alertError(getApplicationContext(), "Nhập mã hàng");
            edtGoodsID.requestFocus();
            return;
        }

        isGoodsIDExist = false;
        checkExistGoodsID(edtGoodsID.getText().toString(), cgDataGenerals);
        if (isGoodsIDExist == true) {
            isGoodsIDExist = false;
        } else {
            alertError(getApplicationContext(), "Mã hàng không tồn tại");
            return;
        }

        totalGoods = 0.0;
        getTotalGoods(cgDataGenerals);

        Call<List<CGDataGeneral>> callback = iWarehouseApi.getCGDataGeneralByGoods(edtCGDNumber.getText().toString(), edtGoodsID.getText().toString());
        callback.enqueue(new Callback<List<CGDataGeneral>>() {
            @Override
            public void onResponse(Call<List<CGDataGeneral>> call, Response<List<CGDataGeneral>> response) {
                if (response.isSuccessful()) {
                    arrayListByGoodsID = (ArrayList<CGDataGeneral>) response.body();
                    if (arrayListByGoodsID.size() > 0) {
                        CGDataGeneral cgDataGeneralUpdate = arrayListByGoodsID.get(0);
                        cgDataGeneralUpdate.setcgdNumber(edtCGDNumber.getText().toString());
                        cgDataGeneralUpdate.setGoodsID(edtGoodsID.getText().toString());
                        cgDataGeneralUpdate.setOrdinal(arrayListByGoodsID.get(0).getOrdinal());
                        cgDataGeneralUpdate.setTotalGoods(totalGoods + 1);
                        cgDataGeneralUpdate.setStatus("checkOK");
                        putCGDataGeneral(cgDataGeneralUpdate.getcgdNumber(), cgDataGeneralUpdate.getGoodsID(), cgDataGeneralUpdate.getOrdinal(), cgDataGeneralUpdate);

                        CGDataHeader cgDataHeaderUpdate = cgDataHeader;
                        putCGDataHeader(edtCGDNumber.getText().toString(), cgDataHeaderUpdate);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<CGDataGeneral>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "get wrdatadetail by idcode" + t.getMessage() + "", Toast.LENGTH_SHORT).show();
            }
        });
    }
}