package com.example.warehousemobileclient.ui.warehouse_receipt;

import androidx.activity.OnBackPressedCallback;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavOptions;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interceptors.HttpLoggingInterceptor;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.example.warehousemobileclient.R;
import com.example.warehousemobileclient.adapter.WRDataHeaderAdapter;
import com.example.warehousemobileclient.adapter.WRDataHeaderAdapter2;
import com.example.warehousemobileclient.model.WRDataHeader;
import com.example.warehousemobileclient.service.IWarehouseApi;
import com.example.warehousemobileclient.service.RetrofitService;
import com.example.warehousemobileclient.ui.issue_order.IssueOrderDetailFragment;
import com.google.gson.Gson;
import com.jacksonandroidnetworking.JacksonParserFactory;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WarehouseReceiptFragment extends Fragment {
    RecyclerView recyclerView;
    private WRDataHeaderAdapter2 wrDataHeaderAdapter2;
    ArrayList<WRDataHeader> arrayList=new ArrayList<>();

    public static WarehouseReceiptFragment newInstance() {
        return new WarehouseReceiptFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.warehouse_receipt_fragment, container, false);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);

        ContentLoadingProgressBar progress = view.findViewById(R.id.progress);
        recyclerView = view.findViewById(R.id.categories_recycler_view);

        progress.show();

        IWarehouseApi iWarehouseApi = RetrofitService.getService();
        Call<List<WRDataHeader>> callback =iWarehouseApi.getWRDataHeaders();
        callback.enqueue(new Callback<List<WRDataHeader>>() {
            @Override
            public void onResponse(Call<List<WRDataHeader>> call, Response<List<WRDataHeader>> response) {
                arrayList=(ArrayList<WRDataHeader>)response.body();
                setAdapter();
                progress.hide();
            }

            @Override
            public void onFailure(Call<List<WRDataHeader>> call, Throwable t) {
                Toast.makeText(getContext(),"Fetch data fail",Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setAdapter(){
        wrDataHeaderAdapter2=new WRDataHeaderAdapter2(getContext(), arrayList, new WRDataHeaderAdapter2.ItemClickListener() {
            @Override
            public void onClick(WRDataHeader wrDataHeader) {
                Bundle bundle = new Bundle();
                bundle.putString(WarehouseReceiptDetailFragment.WRDNumber,wrDataHeader.getWrdNumber());
                Navigation.findNavController(getActivity(),R.id.nav_host_fragment_activity_main).navigate(R.id.warehouseReceiptDetailDest, bundle,getNavOptions());
            }
        });
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
        recyclerView.setAdapter(wrDataHeaderAdapter2);
        wrDataHeaderAdapter2.notifyDataSetChanged();
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Navigation.findNavController(getActivity(), R.id.nav_host_fragment_activity_main).popBackStack();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    protected NavOptions getNavOptions() {
        NavOptions navOptions = new NavOptions.Builder()
                .setEnterAnim(R.anim.slide_in_right)
                .setExitAnim(R.anim.slide_out_left)
                .setPopEnterAnim(R.anim.slide_in_left)
                .setPopExitAnim(R.anim.slide_out_right)
                .build();
        return navOptions;
    }
}