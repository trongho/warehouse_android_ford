package com.example.warehousemobileclient.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.warehousemobileclient.R;
import com.example.warehousemobileclient.model.CountGoods;

import java.util.ArrayList;

public class CountGoodsAdapter extends RecyclerView.Adapter<CountGoodsAdapter.ViewHolder>{
    Context context;
    ArrayList<CountGoods> list;
    CountGoods countGoods;
    ItemClickListener listener;

    public CountGoodsAdapter(Context context, ArrayList<CountGoods> list, ItemClickListener listener) {
        this.context = context;
        this.list = list;
        this.listener=listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context)
                .inflate(R.layout.count_goods_item, viewGroup, false);
        // gan cac thuoc tinh nhu size, margins, paddings.....
        return new ViewHolder(v);
    }

    @SuppressLint("ResourceType")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, @SuppressLint("RecyclerView") final int i) {
        countGoods=list.get(i);
        viewHolder.itemGoodsID.setText(countGoods.getGoodsID());
        viewHolder.itemQuantity.setText(countGoods.getQuantity()+"");
        for(int j=0;j<=i;j++){
            viewHolder.itemLineNo.setText(j+1+"");
        }

        viewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onClick(list.get(i));
                }
            }
        });

        if(i%2==0){
            viewHolder.cardView.setBackgroundColor(Color.parseColor("#CDE0F1"));
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView itemGoodsID;
        TextView itemQuantity;
        TextView itemLineNo;
        CardView cardView;
        public ViewHolder(final View itemView) {
            super(itemView);
            itemGoodsID = (TextView)itemView.findViewById(R.id.tvGoodsID);
            itemQuantity = (TextView) itemView.findViewById(R.id.tvQuantity);
            itemLineNo=(TextView)itemView.findViewById(R.id.tvLineNo);
            cardView=itemView.findViewById(R.id.cardView);
        }
    }

    public interface ItemClickListener {
        void onClick(CountGoods countGoods);
    }
}
