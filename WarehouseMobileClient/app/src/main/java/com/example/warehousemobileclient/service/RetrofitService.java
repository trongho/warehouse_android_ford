package com.example.warehousemobileclient.service;

import com.example.warehousemobileclient.helper.Common;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitService {
    private static final String BASE_URL = "http://192.168.1.195:6377/api/";
//private static final String BASE_URL = "http://wmservice.hp.com:6379/api/";
    public static IWarehouseApi getService(){
        return APIRetrofitClient.getClient(Common.IP_SERVER).create(IWarehouseApi.class);
    }

//    public static DataService Create(){
//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl("http://192.168.1.55:6379/api/")
//                .addConverterFactory(GsonConverterFactory.create())
//                .build();
//        return retrofit.create(DataService.class);
//    }
}
