package com.example.warehousemobileclient.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.warehousemobileclient.R;
import com.example.warehousemobileclient.model.CheckGoodsDetail;
import com.example.warehousemobileclient.model.GoodsData;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class CheckGoods2Adapter extends RecyclerView.Adapter<CheckGoods2Adapter.ViewHolder> {
    Context context;
    ArrayList<CheckGoodsDetail> list;
    CheckGoodsDetail checkGoodsDetail;
    ItemClickListener listener;
    ItemDeleteListener deleteListener;
    SimpleDateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    public CheckGoods2Adapter(Context context, ArrayList<CheckGoodsDetail> list, ItemClickListener listener,ItemDeleteListener deleteListener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
        this.deleteListener=deleteListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context)
                .inflate(R.layout.cgdata_general_item2, viewGroup, false);
        // gan cac thuoc tinh nhu size, margins, paddings.....
        return new ViewHolder(v);
    }

    @SuppressLint("ResourceType")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, @SuppressLint("RecyclerView") final int i) {
        checkGoodsDetail = list.get(i);
        if(i%2==0){
            viewHolder.cardView.setBackgroundColor(Color.parseColor("#CDE0F1"));
        }

        viewHolder.tvGoodsID.setText(checkGoodsDetail.getGoodsID());
        for(int j=0;j<=i;j++){
            viewHolder.tvLineNo.setText(j+1+".");
        }
        viewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onClick(list.get(i));
                }
            }
        });

        viewHolder.btnDetele.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (deleteListener != null) {
                    deleteListener.onClick(list.get(i));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvGoodsID,tvLineNo;
        public CardView cardView;
        public AppCompatButton btnDetele;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvGoodsID = itemView.findViewById(R.id.tvGoodsID);
            tvLineNo=itemView.findViewById(R.id.tvLineNo);
            cardView = itemView.findViewById(R.id.carView);
            btnDetele=itemView.findViewById(R.id.btnDelete);
        }
    }

    public void updateList(List<CheckGoodsDetail> lists) {
        this.list.clear();
        this.list.addAll(lists);
    }

    public interface ItemClickListener {
        void onClick(CheckGoodsDetail checkGoodsDetail);
    }

    public interface ItemDeleteListener {
        void onClick(CheckGoodsDetail checkGoodsDetail);
    }

}
