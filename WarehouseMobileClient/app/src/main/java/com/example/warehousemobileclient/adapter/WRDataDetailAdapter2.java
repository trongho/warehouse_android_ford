package com.example.warehousemobileclient.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.warehousemobileclient.R;
import com.example.warehousemobileclient.model.WRDataGeneral;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class WRDataDetailAdapter2 extends RecyclerView.Adapter<WRDataDetailAdapter2.ViewHolder> {
    Context context;
    ArrayList<WRDataGeneral> list;
    WRDataGeneral wrDataGeneral;
    WRDataDetailAdapter2.ItemClickListener listener;
    ItemEditNoteClickListener itemEditNoteClickListener;

    SimpleDateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    public WRDataDetailAdapter2(Context context, ArrayList<WRDataGeneral> list, WRDataDetailAdapter2.ItemClickListener listener,ItemEditNoteClickListener itemEditNoteClickListener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
        this.itemEditNoteClickListener=itemEditNoteClickListener;
    }

    @NonNull
    @Override
    public WRDataDetailAdapter2.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context)
                .inflate(R.layout.wrdata_general_item, viewGroup, false);
        // gan cac thuoc tinh nhu size, margins, paddings.....
        return new WRDataDetailAdapter2.ViewHolder(v);
    }

    @SuppressLint("ResourceType")
    @Override
    public void onBindViewHolder(@NonNull WRDataDetailAdapter2.ViewHolder viewHolder, @SuppressLint("RecyclerView") final int i) {
        wrDataGeneral = list.get(i);
        viewHolder.tvGoodsID.setText(wrDataGeneral.getGoodsID());
        viewHolder.tvQuantity.setText(String.format("%.0f",wrDataGeneral.getQuantity()));
        viewHolder.tvQuantityOrg.setText(String.format("%.0f",wrDataGeneral.getQuantityOrg()));
        viewHolder.tvSLPart.setText(wrDataGeneral.getSlPart());
        viewHolder.tvWhse.setText(wrDataGeneral.getReceiptStatus());
        if(!TextUtils.isEmpty(wrDataGeneral.getNote())){
            viewHolder.llNote.setVisibility(View.VISIBLE);
            viewHolder.tvNote.setText(wrDataGeneral.getNote());
        }else {
            viewHolder.llNote.setVisibility(View.GONE);
            viewHolder.tvNote.setText(wrDataGeneral.getNote());
        }
        for(int j=0;j<=i;j++){
            viewHolder.tvLineNo.setText(j+1+"");
        }
        viewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onClick(list.get(i));
                }
            }
        });

        viewHolder.cardView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if(itemEditNoteClickListener!=null){
                    itemEditNoteClickListener.onClick(list.get(i));
                }
                return true;
            }
        });

        if(wrDataGeneral.getReceiptStatus().equalsIgnoreCase("NEW")){
            viewHolder.tvLineNo.setBackgroundColor(Color.parseColor("#FF99FF"));
        }
        else if(wrDataGeneral.getReceiptStatus().equalsIgnoreCase("QC")){
            viewHolder.tvLineNo.setBackgroundColor(Color.parseColor("#FF99FF"));
        }
        else if(wrDataGeneral.getReceiptStatus().equalsIgnoreCase("QDC")){
            viewHolder.tvLineNo.setBackgroundColor(Color.parseColor("#FF99FF"));
        }
        if(wrDataGeneral.getSlPart()!=null&&wrDataGeneral.getSlPart().equalsIgnoreCase("Y")){
//            viewHolder.tvLineNo.setBackground(ContextCompat.getDrawable(context,R.drawable.backnew));
            viewHolder.tvLineNo.setBackgroundColor(Color.parseColor("#FF99FF"));
        }


        if(wrDataGeneral.getQuantity().doubleValue()==0){
            viewHolder.cardView.setBackgroundColor(Color.parseColor("#FFFFFF"));
        }
        else if(wrDataGeneral.getQuantity().doubleValue()>0&&wrDataGeneral.getQuantity().doubleValue()<wrDataGeneral.getQuantityOrg().doubleValue()){
            viewHolder.cardView.setBackgroundColor(Color.parseColor("#f75c78"));
        }
        else if(wrDataGeneral.getQuantity().doubleValue()==wrDataGeneral.getQuantityOrg().doubleValue()){
            viewHolder.cardView.setBackgroundColor(Color.parseColor("#1dde3d"));
        } else if(wrDataGeneral.getQuantity().doubleValue()>wrDataGeneral.getQuantityOrg().doubleValue()){
            viewHolder.cardView.setBackgroundColor(Color.parseColor("#f7fc60"));
        }


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvGoodsID, tvQuantity,tvQuantityOrg,tvLineNo,tvSLPart,tvWhse,tvNote;
        public CardView cardView;
        public LinearLayout llNote;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvGoodsID = itemView.findViewById(R.id.tvGoodsID);
            tvQuantity = itemView.findViewById(R.id.tvQuantity);
            tvQuantityOrg = itemView.findViewById(R.id.tvQuantityOrg);
            tvLineNo=itemView.findViewById(R.id.tvLineNo);
            tvSLPart=itemView.findViewById(R.id.tvSLPart);
            tvWhse=itemView.findViewById(R.id.tvWhse);
            cardView = itemView.findViewById(R.id.carView);
            tvNote=itemView.findViewById(R.id.tvNote);
            llNote=itemView.findViewById(R.id.llNote);
        }
    }

    public void updateList(List<WRDataGeneral> lists) {
        this.list.clear();
        this.list.addAll(lists);
    }

    public interface ItemClickListener {
        void onClick(WRDataGeneral wrDataGeneral);
    }

    public interface ItemEditNoteClickListener{
        void onClick(WRDataGeneral wrDataGeneral);
    }

}
