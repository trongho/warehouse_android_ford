package com.example.warehousemobileclient.model;

public class GoodsData {
    private String wrdNumber;
    private String goodsID;
    private String date;
    private String status;

    public GoodsData() {
    }

    public GoodsData(String wrdNumber, String goodsID, String date, String status) {
        this.wrdNumber = wrdNumber;
        this.goodsID = goodsID;
        this.date = date;
        this.status = status;
    }

    public String getWrdNumber() {
        return wrdNumber;
    }

    public void setWrdNumber(String wrdNumber) {
        this.wrdNumber = wrdNumber;
    }

    public String getGoodsID() {
        return goodsID;
    }

    public void setGoodsID(String goodsID) {
        this.goodsID = goodsID;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
