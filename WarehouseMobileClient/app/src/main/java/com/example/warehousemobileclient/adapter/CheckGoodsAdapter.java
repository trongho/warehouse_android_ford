package com.example.warehousemobileclient.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.warehousemobileclient.R;
import com.example.warehousemobileclient.model.CGDataGeneral;
import com.example.warehousemobileclient.model.GoodsData;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class CheckGoodsAdapter extends RecyclerView.Adapter<CheckGoodsAdapter.ViewHolder> {
    Context context;
    ArrayList<GoodsData> list;
    GoodsData goodsData;
    ItemClickListener listener;
    SimpleDateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    public CheckGoodsAdapter(Context context, ArrayList<GoodsData> list, ItemClickListener listener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context)
                .inflate(R.layout.cgdata_general_item, viewGroup, false);
        // gan cac thuoc tinh nhu size, margins, paddings.....
        return new ViewHolder(v);
    }

    @SuppressLint("ResourceType")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, @SuppressLint("RecyclerView") final int i) {
        goodsData = list.get(i);
        viewHolder.tvGoodsID.setText(goodsData.getGoodsID());
        for(int j=0;j<=i;j++){
            viewHolder.tvLineNo.setText(j+1+".");
        }
        viewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onClick(list.get(i));
                }
            }
        });


        if(goodsData.getStatus().equalsIgnoreCase("")){
            viewHolder.cardView.setBackgroundColor(Color.parseColor("#f7fc60"));
        }
        
        else if(goodsData.getStatus().equalsIgnoreCase("checkOK")){
            viewHolder.cardView.setBackgroundColor(Color.parseColor("#1dde3d"));
        }


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvGoodsID,tvLineNo;
        public CardView cardView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvGoodsID = itemView.findViewById(R.id.tvGoodsID);
            tvLineNo=itemView.findViewById(R.id.tvLineNo);
            cardView = itemView.findViewById(R.id.carView);
        }
    }

    public void updateList(List<GoodsData> lists) {
        this.list.clear();
        this.list.addAll(lists);
    }

    public interface ItemClickListener {
        void onClick(GoodsData goodsData);
    }

}
