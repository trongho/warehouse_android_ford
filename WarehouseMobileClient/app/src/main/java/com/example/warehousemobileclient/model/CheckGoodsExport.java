package com.example.warehousemobileclient.model;

public class CheckGoodsExport {
    String goodsID;

    public CheckGoodsExport() {
    }

    public CheckGoodsExport(String goodsID) {
        this.goodsID = goodsID;
    }

    public String getGoodsID() {
        return goodsID;
    }

    public void setGoodsID(String goodsID) {
        this.goodsID = goodsID;
    }
}
