package com.example.warehousemobileclient.ui.login;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.warehousemobileclient.R;
import com.example.warehousemobileclient.databinding.ActivityLoginBinding;
import com.example.warehousemobileclient.databinding.ActivityMainBinding;
import com.example.warehousemobileclient.model.User;
import com.example.warehousemobileclient.model.WRDataDetail;
import com.example.warehousemobileclient.service.IWarehouseApi;
import com.example.warehousemobileclient.service.RetrofitService;
import com.example.warehousemobileclient.ui.user.UserViewModel;
import com.google.android.material.textfield.TextInputEditText;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    ActivityLoginBinding binding;
    private LoginViewModel loginViewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityLoginBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        loginViewModel= ViewModelProviders.of(this).get(LoginViewModel.class);

        final TextInputEditText edtUsername = binding.edtUsername;
        final TextInputEditText edtPassword = binding.edtPassword;
        final Button btnLogin = binding.btnLogin;

        loginViewModel.getmUsername().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                edtUsername.setText(s);
            }
        });
        loginViewModel.getmPassword().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                edtPassword.setText(s);
            }
        });
//        loginViewModel.login().observe(this,user -> {
//            if(user==null){
//                Toast.makeText(this,"Login fail",Toast.LENGTH_SHORT).show();
//            }
//            else {
//                Toast.makeText(this,"Login sucess",Toast.LENGTH_SHORT).show();
//            }
//        });


    }
}