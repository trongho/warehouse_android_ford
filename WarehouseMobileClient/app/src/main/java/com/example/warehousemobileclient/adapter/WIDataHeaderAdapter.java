package com.example.warehousemobileclient.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.warehousemobileclient.R;
import com.example.warehousemobileclient.model.WIDataHeader;
import com.example.warehousemobileclient.model.WRDataHeader;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class WIDataHeaderAdapter extends RecyclerView.Adapter<WIDataHeaderAdapter.ViewHolder> {
    Context context;
    ArrayList<WIDataHeader> list;
    WIDataHeader entry;
    WIDataHeaderAdapter.ItemClickListener listener;
    SimpleDateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    public WIDataHeaderAdapter(Context context, ArrayList<WIDataHeader> list, WIDataHeaderAdapter.ItemClickListener listener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public WIDataHeaderAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context)
                .inflate(R.layout.wrdataheader_item, viewGroup, false);
        // gan cac thuoc tinh nhu size, margins, paddings.....
        return new WIDataHeaderAdapter.ViewHolder(v);
    }

    @SuppressLint("ResourceType")
    @Override
    public void onBindViewHolder(@NonNull WIDataHeaderAdapter.ViewHolder viewHolder, @SuppressLint("RecyclerView") final int i) {
        entry = list.get(i);
        try {
            viewHolder.tvWRDNumber.setText(entry.getWidNumber());
            Date date = originalFormat.parse(entry.getWidDate());
            viewHolder.tvWRDate.setText(simpleDateFormat.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        viewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onClick(list.get(i));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvWRDNumber, tvWRDate;
        public CardView cardView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvWRDNumber = itemView.findViewById(R.id.textWRDNumber);
            tvWRDate = itemView.findViewById(R.id.textWRDDate);
            cardView = itemView.findViewById(R.id.carView);
        }
    }

    public void updateList(List<WIDataHeader> lists) {
        this.list.clear();
        this.list.addAll(lists);
    }

    public interface ItemClickListener {
        void onClick(WIDataHeader wiDataHeader);
    }

}

