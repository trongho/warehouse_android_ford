package com.example.warehousemobileclient.helper;

import android.os.Environment;
import android.util.Log;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class ConnectFTPServer {
    static String result="";
    static String receiverResult="";
    public static String connect(final String ip) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                FTPClient client = new FTPClient();
                try {
                    client.connect(ip);
                    // When login success the login method returns true.
                    boolean login = client.login("HPGROUP\\trong.hk", "123456789");
                    if (login) {
                        result="success to connect to server "+ip;
                        // When logout success the logout method returns true.
//                        boolean logout = client.logout();
//                        if (logout) {
//                            System.out.println("Logout from FTP server...");
//                        }
                    } else {
                        result="fail to connect to server "+ip;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        // Closes the connection to the FTP server
                        client.disconnect();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        thread.start();
        return result;
    }

    public static void sendFileToPC(final String ip, final String fileName) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    FTPClient con = null;
                    con = new FTPClient();
                    con.connect(ip);

                    if (con.login("HPGROUP\\trong.hk", "trong.hk")) {
                        con.enterLocalPassiveMode(); // important!
                        con.setFileType(FTP.BINARY_FILE_TYPE);

                        FileInputStream in = new FileInputStream(new File(Environment.getExternalStorageDirectory(), fileName));
                        System.out.println(fileName + "");
                        boolean result = con.storeFile(fileName, in);
                        in.close();
                        if (result) {
                            Log.d("upload result", "succeeded");
                            System.out.println("send to pc success");
                        }
                        con.logout();
                        con.disconnect();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }

    public static String receiverFileFromPC(final String ip, final String fileName) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    FTPClient con = null;
                    con = new FTPClient();
                    con.connect(ip);

                    if (con.login("HPGROUP\\trong.hk", "123456789")) {
                        con.enterLocalPassiveMode(); // important!
                        con.setFileType(FTP.BINARY_FILE_TYPE);

                        FileOutputStream ou = new FileOutputStream(new File(Environment.getExternalStorageDirectory(), fileName));
                        System.out.println(fileName + "");
                        boolean result = con.retrieveFile(fileName,ou);
                        ou.close();
                        if (result) {
                            receiverResult="success";
                        }
                        else {
                            receiverResult="fail";
                        }
                        con.logout();
                        con.disconnect();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    receiverResult=e.getMessage().toString();
                }
            }
        });
        thread.start();
        return receiverResult;
    }
}
