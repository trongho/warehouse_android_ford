package com.example.warehousemobileclient.helper;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class MyInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        String stringurl = request.url().toString();
        stringurl = stringurl.replace("%2F", "/");

        Request newRequest = new Request.Builder()
                .url(stringurl)
                .build();

        return chain.proceed(newRequest);
    }
}
