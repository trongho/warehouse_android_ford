package com.example.warehousemobileclient.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class TSHeader {
    @SerializedName("tsNumber")
    @Expose
    private String tsNumber;
    @SerializedName("tsDate")
    @Expose
    private Date tsDate;
    @SerializedName("referenceNumber")
    @Expose
    private String referenceNumber;
    @SerializedName("warehouseID")
    @Expose
    private String warehouseID;
    @SerializedName("warehouseName")
    @Expose
    private String warehouseName;
    @SerializedName("priceTypeID")
    @Expose
    private String priceTypeID;
    @SerializedName("priceTypeName")
    @Expose
    private String priceTypeName;
    @SerializedName("handlingStatusID")
    @Expose
    private String handlingStatusID;
    @SerializedName("handlingStatusName")
    @Expose
    private String handlingStatusName;
    @SerializedName("note")
    @Expose
    private String note;
    @SerializedName("branchID")
    @Expose
    private String branchID;
    @SerializedName("branchName")
    @Expose
    private String branchName;
    @SerializedName("totalDiscountAmount")
    @Expose
    private Double totalDiscountAmount;
    @SerializedName("totalVATAmount")
    @Expose
    private Double totalVATAmount;
    @SerializedName("totalAmountExcludedVAT")
    @Expose
    private Double totalAmountExcludedVAT;
    @SerializedName("totalAmountIncludedVAT")
    @Expose
    private Double totalAmountIncludedVAT;
    @SerializedName("totalAmount")
    @Expose
    private Double totalAmount;
    @SerializedName("totalQuantity")
    @Expose
    private Double totalQuantity;
    @SerializedName("checkerID1")
    @Expose
    private String checkerID1;
    @SerializedName("checkerName1")
    @Expose
    private String checkerName1;
    @SerializedName("checkerID2")
    @Expose
    private String checkerID2;
    @SerializedName("checkerName2")
    @Expose
    private String checkerName2;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("createdUserID")
    @Expose
    private String createdUserID;
    @SerializedName("createdDate")
    @Expose
    private Date createdDate;
    @SerializedName("updatedUserID")
    @Expose
    private String updatedUserID;
    @SerializedName("updatedDate")
    @Expose
    private Date updatedDate;

    public TSHeader() {
    }

    public TSHeader(String tsNumber, Date tsDate, String referenceNumber, String warehouseID, String warehouseName, String priceTypeID, String priceTypeName, String handlingStatusID, String handlingStatusName, String note, String branchID, String branchName, Double totalDiscountAmount, Double totalVATAmount, Double totalAmountExcludedVAT, Double totalAmountIncludedVAT, Double totalAmount, Double totalQuantity, String checkerID1, String checkerName1, String checkerID2, String checkerName2, String status, String createdUserID, Date createdDate, String updatedUserID, Date updatedDate) {
        this.tsNumber = tsNumber;
        this.tsDate = tsDate;
        this.referenceNumber = referenceNumber;
        this.warehouseID = warehouseID;
        this.warehouseName = warehouseName;
        this.priceTypeID = priceTypeID;
        this.priceTypeName = priceTypeName;
        this.handlingStatusID = handlingStatusID;
        this.handlingStatusName = handlingStatusName;
        this.note = note;
        this.branchID = branchID;
        this.branchName = branchName;
        this.totalDiscountAmount = totalDiscountAmount;
        this.totalVATAmount = totalVATAmount;
        this.totalAmountExcludedVAT = totalAmountExcludedVAT;
        this.totalAmountIncludedVAT = totalAmountIncludedVAT;
        this.totalAmount = totalAmount;
        this.totalQuantity = totalQuantity;
        this.checkerID1 = checkerID1;
        this.checkerName1 = checkerName1;
        this.checkerID2 = checkerID2;
        this.checkerName2 = checkerName2;
        this.status = status;
        this.createdUserID = createdUserID;
        this.createdDate = createdDate;
        this.updatedUserID = updatedUserID;
        this.updatedDate = updatedDate;
    }

    public String getTsNumber() {
        return tsNumber;
    }

    public void setTsNumber(String tsNumber) {
        this.tsNumber = tsNumber;
    }

    public Date getTsDate() {
        return tsDate;
    }

    public void setTsDate(Date tsDate) {
        this.tsDate = tsDate;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String getWarehouseID() {
        return warehouseID;
    }

    public void setWarehouseID(String warehouseID) {
        this.warehouseID = warehouseID;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public String getPriceTypeID() {
        return priceTypeID;
    }

    public void setPriceTypeID(String priceTypeID) {
        this.priceTypeID = priceTypeID;
    }

    public String getPriceTypeName() {
        return priceTypeName;
    }

    public void setPriceTypeName(String priceTypeName) {
        this.priceTypeName = priceTypeName;
    }

    public String getHandlingStatusID() {
        return handlingStatusID;
    }

    public void setHandlingStatusID(String handlingStatusID) {
        this.handlingStatusID = handlingStatusID;
    }

    public String getHandlingStatusName() {
        return handlingStatusName;
    }

    public void setHandlingStatusName(String handlingStatusName) {
        this.handlingStatusName = handlingStatusName;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getBranchID() {
        return branchID;
    }

    public void setBranchID(String branchID) {
        this.branchID = branchID;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public Double getTotalDiscountAmount() {
        return totalDiscountAmount;
    }

    public void setTotalDiscountAmount(Double totalDiscountAmount) {
        this.totalDiscountAmount = totalDiscountAmount;
    }

    public Double getTotalVATAmount() {
        return totalVATAmount;
    }

    public void setTotalVATAmount(Double totalVATAmount) {
        this.totalVATAmount = totalVATAmount;
    }

    public Double getTotalAmountExcludedVAT() {
        return totalAmountExcludedVAT;
    }

    public void setTotalAmountExcludedVAT(Double totalAmountExcludedVAT) {
        this.totalAmountExcludedVAT = totalAmountExcludedVAT;
    }

    public Double getTotalAmountIncludedVAT() {
        return totalAmountIncludedVAT;
    }

    public void setTotalAmountIncludedVAT(Double totalAmountIncludedVAT) {
        this.totalAmountIncludedVAT = totalAmountIncludedVAT;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Double getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(Double totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public String getCheckerID1() {
        return checkerID1;
    }

    public void setCheckerID1(String checkerID1) {
        this.checkerID1 = checkerID1;
    }

    public String getCheckerName1() {
        return checkerName1;
    }

    public void setCheckerName1(String checkerName1) {
        this.checkerName1 = checkerName1;
    }

    public String getCheckerID2() {
        return checkerID2;
    }

    public void setCheckerID2(String checkerID2) {
        this.checkerID2 = checkerID2;
    }

    public String getCheckerName2() {
        return checkerName2;
    }

    public void setCheckerName2(String checkerName2) {
        this.checkerName2 = checkerName2;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedUserID() {
        return createdUserID;
    }

    public void setCreatedUserID(String createdUserID) {
        this.createdUserID = createdUserID;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedUserID() {
        return updatedUserID;
    }

    public void setUpdatedUserID(String updatedUserID) {
        this.updatedUserID = updatedUserID;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }
}
