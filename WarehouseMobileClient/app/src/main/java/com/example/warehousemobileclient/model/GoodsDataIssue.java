package com.example.warehousemobileclient.model;

public class GoodsDataIssue {
    private String widNumber;
    private String goodsID;
    private String goodsGroupID;
    private String date;
    private String status;

    public GoodsDataIssue() {
    }

    public GoodsDataIssue(String widNumber, String goodsID, String goodsGroupID, String date, String status) {
        this.widNumber = widNumber;
        this.goodsID = goodsID;
        this.goodsGroupID = goodsGroupID;
        this.date = date;
        this.status = status;
    }

    public String getWidNumber() {
        return widNumber;
    }

    public void setWidNumber(String widNumber) {
        this.widNumber = widNumber;
    }

    public String getGoodsID() {
        return goodsID;
    }

    public void setGoodsID(String goodsID) {
        this.goodsID = goodsID;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getGoodsGroupID() {
        return goodsGroupID;
    }

    public void setGoodsGroupID(String goodsGroupID) {
        this.goodsGroupID = goodsGroupID;
    }
}
