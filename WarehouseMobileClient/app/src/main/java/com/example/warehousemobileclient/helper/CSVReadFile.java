package com.example.warehousemobileclient.helper;

import android.content.Context;
import android.os.Environment;
import android.widget.Toast;

import com.example.warehousemobileclient.model.Location;
import com.opencsv.CSVReader;

import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class CSVReadFile {
    static ArrayList<String> locationList=new ArrayList<String>();
    public static ArrayList<String> readCSVFile(){
        try {
            File csvfile = new File(Environment.getExternalStorageDirectory() + "/location_list.csv");
            CSVReader reader = new CSVReader(new FileReader(csvfile.getAbsolutePath()));
            String[] nextLine;
            locationList.clear();
            while ((nextLine = reader.readNext()) != null) {
                for(String s:nextLine) {
                    locationList.add(s);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return locationList;
    }
}
