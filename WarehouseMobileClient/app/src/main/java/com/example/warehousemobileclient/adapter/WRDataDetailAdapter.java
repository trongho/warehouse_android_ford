package com.example.warehousemobileclient.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.warehousemobileclient.R;
import com.example.warehousemobileclient.model.WRDataDetail;
import com.example.warehousemobileclient.model.WRDataHeader;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class WRDataDetailAdapter extends RecyclerView.Adapter<WRDataDetailAdapter.ViewHolder> {
    Context context;
    ArrayList<WRDataDetail> list;
    WRDataDetail wrDataDetail;
    WRDataDetailAdapter.ItemClickListener listener;
    SimpleDateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    public WRDataDetailAdapter(Context context, ArrayList<WRDataDetail> list, WRDataDetailAdapter.ItemClickListener listener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public WRDataDetailAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context)
                .inflate(R.layout.wrdatadetail_item, viewGroup, false);
        // gan cac thuoc tinh nhu size, margins, paddings.....
        return new WRDataDetailAdapter.ViewHolder(v);
    }

    @SuppressLint("ResourceType")
    @Override
    public void onBindViewHolder(@NonNull WRDataDetailAdapter.ViewHolder viewHolder, @SuppressLint("RecyclerView") final int i) {
        wrDataDetail = list.get(i);
        viewHolder.tvGoodsID.setText(wrDataDetail.getGoodsID());
        viewHolder.tvIdCode.setText(wrDataDetail.getIdCode());
        viewHolder.tvQuantity.setText(wrDataDetail.getQuantity() + "");
        viewHolder.tvQuantityOrg.setText(wrDataDetail.getQuantityOrg() + "");
        viewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onClick(list.get(i));
                }
            }
        });


        if(wrDataDetail.getQuantity().doubleValue()==0){
            viewHolder.cardView.setBackgroundColor(Color.parseColor("#FFFFFF"));
        }
        else if(wrDataDetail.getQuantity().doubleValue()>0&&wrDataDetail.getQuantity().doubleValue()<wrDataDetail.getQuantityOrg().doubleValue()){
            viewHolder.cardView.setBackgroundColor(Color.parseColor("#f75c78"));
        }
        else if(wrDataDetail.getQuantity().doubleValue()==wrDataDetail.getQuantityOrg().doubleValue()){
            viewHolder.cardView.setBackgroundColor(Color.parseColor("#1dde3d"));
        } else if(wrDataDetail.getQuantity().doubleValue()>wrDataDetail.getQuantityOrg().doubleValue()){
            viewHolder.cardView.setBackgroundColor(Color.parseColor("#f7fc60"));
        }


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvGoodsID, tvIdCode, tvQuantity,tvQuantityOrg;
        public CardView cardView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvGoodsID = itemView.findViewById(R.id.tvGoodsID);
            tvIdCode = itemView.findViewById(R.id.tvIDCode);
            tvQuantity = itemView.findViewById(R.id.tvQuantity);
            tvQuantityOrg = itemView.findViewById(R.id.tvQuantityOrg);
            cardView = itemView.findViewById(R.id.carView);
        }
    }

    public void updateList(List<WRDataDetail> lists) {
        this.list.clear();
        this.list.addAll(lists);
    }

    public interface ItemClickListener {
        void onClick(WRDataDetail wrDataDetail);
    }

}
