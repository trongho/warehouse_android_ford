package com.example.warehousemobileclient.helper;

import android.content.Context;
import android.net.ConnectivityManager;

import java.net.InetAddress;


public class NetworkHelper {
    Context mContext;

    public NetworkHelper(Context mContext) {
        this.mContext = mContext;
    }

    public boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager)mContext.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }
    public boolean isInternetAvailable() {
        try {
            InetAddress ipAddr = InetAddress.getByName("google.com");
            //You can replace it with your name
            return !ipAddr.equals("");

        } catch (Exception e) {
            return false;
        }
    }
}
