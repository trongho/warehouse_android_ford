package com.example.warehousemobileclient.ui.receipt_data;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.appcompat.widget.AppCompatButton;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.warehousemobileclient.R;
import com.example.warehousemobileclient.adapter.WRDataDetailAdapter;
import com.example.warehousemobileclient.adapter.WRDataDetailAdapter2;
import com.example.warehousemobileclient.dao.ReceiptDataDAO;
import com.example.warehousemobileclient.helper.Logger;
import com.example.warehousemobileclient.helper.TinyDB;
import com.example.warehousemobileclient.model.GoodsData;
import com.example.warehousemobileclient.model.WIDataGeneral;
import com.example.warehousemobileclient.model.WRDataDetail;
import com.example.warehousemobileclient.model.WRDataGeneral;
import com.example.warehousemobileclient.model.WRDataHeader;
import com.example.warehousemobileclient.service.IWarehouseApi;
import com.example.warehousemobileclient.service.RetrofitService;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.symbol.emdk.EMDKManager;
import com.symbol.emdk.EMDKResults;
import com.symbol.emdk.EMDKManager.EMDKListener;
import com.symbol.emdk.EMDKManager.FEATURE_TYPE;
import com.symbol.emdk.barcode.BarcodeManager;
import com.symbol.emdk.barcode.BarcodeManager.ConnectionState;
import com.symbol.emdk.barcode.BarcodeManager.ScannerConnectionListener;
import com.symbol.emdk.barcode.ScanDataCollection;
import com.symbol.emdk.barcode.Scanner;
import com.symbol.emdk.barcode.ScannerConfig;
import com.symbol.emdk.barcode.ScannerException;
import com.symbol.emdk.barcode.ScannerInfo;
import com.symbol.emdk.barcode.ScannerResults;
import com.symbol.emdk.barcode.ScanDataCollection.ScanData;
import com.symbol.emdk.barcode.Scanner.DataListener;
import com.symbol.emdk.barcode.Scanner.StatusListener;
import com.symbol.emdk.barcode.Scanner.TriggerType;
import com.symbol.emdk.barcode.StatusData.ScannerStates;
import com.symbol.emdk.barcode.StatusData;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReceiptDataActivity extends AppCompatActivity implements EMDKManager.EMDKListener, Scanner.DataListener, Scanner.StatusListener, BarcodeManager.ScannerConnectionListener {
    private EMDKManager emdkManager = null;
    private BarcodeManager barcodeManager = null;
    private Scanner scanner = null;
    private Spinner spinnerScannerDevices = null;
    private List<ScannerInfo> deviceList = null;
    private final Object lock = new Object();
    private boolean bSoftTriggerSelected = false;
    private boolean bDecoderSettingsChanged = false;
    private boolean bExtScannerDisconnected = false;
    private boolean bContinuousMode = false;

    private String numberRegex = "\\d+(?:\\.\\d+)?";

    private RecyclerView recyclerView = null;
    private EditText edtWRDNumber = null;
    //    private EditText edtReferenceNumber = null;
    private EditText edtGoodsID = null;
    private EditText edtDateTime = null;
    private EditText edtQuantity = null;
    private EditText edtTotalQuantityOrg = null;
    private EditText edtTotalQuantity = null;
    private EditText edtTotalGoodsOrg = null;
    private EditText edtTotalGoods = null;
    private Button btnClearWRDNumber = null;
    private Button btnClearGoodsID = null;
    private Button btnClearDateTime = null;
    private Button btnClearQuantity = null;
    private Button btnClearList = null;
    private Button btnExit = null;
    private Button btnSave = null;
    private WRDataDetailAdapter2 adapter;
    //    ArrayList<WRDataDetail> wrDataDetails;
    ArrayList<WRDataGeneral> wrDataGenerals;
    ArrayList<WRDataGeneral> arrayListByIDCode;
    ArrayList<WRDataGeneral> arrayListByGoodsID;
    ArrayList<WRDataHeader> wrDataHeaders;
    WRDataHeader wrDataHeader;
    IWarehouseApi iWarehouseApi = RetrofitService.getService();
    private static final String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%";

    Boolean isGoodsIDExist = false;
    //    Boolean isGoodsIDAndIDCodeExist = false;
    Boolean isGoodsIDAndDateScanned = false;
    Boolean isGoodsIdDuplicate = false;
    Boolean isGoodsIDAndIDCodeScanned = false;
    Double totalGoods = 0.0;
    Double totalQuantity = 0.0;

    //    ArrayList<GoodsData> goodsDataArrayList = null;
//    GoodsData goodsData = null;
//    String date = "";
//    ReceiptDataDAO receiptDataDAO;
//    Logger logger;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    FloatingActionButton floatingActionButton = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receipt_data);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Nhập hàng");

        EMDKResults results = EMDKManager.getEMDKManager(getApplicationContext(), this);
        if (results.statusCode != EMDKResults.STATUS_CODE.SUCCESS) {
            updateStatus("EMDKManager object request failed!");
            return;
        }
        initComponent();


        edtWRDNumber.requestFocus();

        wrDataHeaders = new ArrayList<>();
        wrDataGenerals = new ArrayList<>();
        arrayListByGoodsID = new ArrayList<>();
        arrayListByIDCode = new ArrayList<>();
//        goodsData = new GoodsData();
//        goodsDataArrayList = new ArrayList<GoodsData>();

        edtWRDNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                floatingActionButton.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                wrDataGenerals.clear();
                setAdapter(wrDataGenerals);
                floatingActionButton.setVisibility(View.VISIBLE);
            }

            @Override
            public void afterTextChanged(Editable editable) {
//                if (TextUtils.isEmpty(editable)) {
//                    alertError(getApplicationContext(), "Nhập số phiếu");
//                    edtWRDNumber.requestFocus();
//                    return;
//                }

//                if (editable.length() >= 1) {
//                    Call<List<WRDataHeader>> callback = iWarehouseApi.getWRDataHeader(edtWRDNumber.getText().toString().trim().toUpperCase());
//                    callback.enqueue(new Callback<List<WRDataHeader>>() {
//                        @Override
//                        public void onResponse(Call<List<WRDataHeader>> call, Response<List<WRDataHeader>> response) {
//                            if (response.isSuccessful()) {
//                                wrDataHeaders = (ArrayList<WRDataHeader>) response.body();
//                                if (wrDataHeaders.size() > 0) {
//                                    alertSuccess(getApplicationContext(), "Lấy dữ liệu thành công");
//                                    wrDataHeader = wrDataHeaders.get(0);
//                                    fetchWRDataGeneralData(edtWRDNumber.getText().toString().toUpperCase());
//                                    hideSoftKeyBoard();
////                                    edtReferenceNumber.setText(wrDataHeader.getReferenceNumber());
//                                    edtTotalQuantityOrg.setText(String.format("%.0f", wrDataHeader.getTotalQuantityOrg()));
//                                    edtTotalQuantity.setText(String.format("%.0f", wrDataHeader.getTotalQuantity()));
//                                    edtGoodsID.requestFocus();
//                                }
//
//                            }
//                        }
//
//                        @Override
//                        public void onFailure(Call<List<WRDataHeader>> call, Throwable t) {
//                            Toast.makeText(getApplicationContext(), "Fetch wrdataheader fail", Toast.LENGTH_SHORT).show();
//                        }
//                    });
////                    fetchWRDataDetailData(edtWRDNumber.getText().toString().toUpperCase());
//                }
            }
        });

//        edtWRDNumber.setOnKeyListener(new View.OnKeyListener() {
//            public boolean onKey(View v, int keyCode, KeyEvent event) {
//                // If the event is a key-down event on the "enter" button
//                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
//                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
//                    fetdata();
//                    return true;
//                }
//                return false;
//            }
//        });

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fetdata();

            }
        });

        edtGoodsID.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (TextUtils.isEmpty(edtWRDNumber.getText().toString().toUpperCase())) {
                    alertError(getApplicationContext(), "Nhập Conveyance number");
                    edtWRDNumber.requestFocus();
                    return;
                }
            }
        });

        edtQuantity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (TextUtils.isEmpty(edtWRDNumber.getText().toString().toUpperCase())) {
                    alertError(getApplicationContext(), "Nhập Conveyance number");
                    edtWRDNumber.requestFocus();
                    return;
                }

            }
        });

        btnClearWRDNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtWRDNumber.setText("");
//                edtReferenceNumber.setText("");
            }
        });
        btnClearGoodsID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtGoodsID.setText("");
            }
        });
        btnClearDateTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtDateTime.setText("");
            }
        });
        btnClearQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtQuantity.setText("");
            }
        });
        btnClearList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (wrDataGenerals.size() > 0) {
                    clearlist();
                }
            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                process();
            }
        });
        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void initComponent() {
        recyclerView = findViewById(R.id.recycler_view);
        edtWRDNumber = findViewById(R.id.edtWRDNumber);
//        edtReferenceNumber = findViewById(R.id.edtReferenceNumber);
        edtGoodsID = findViewById(R.id.edtGoodsID);
        edtDateTime = findViewById(R.id.edtDate);
        edtQuantity = findViewById(R.id.edtQuantity);
        btnClearWRDNumber = findViewById(R.id.btn_clear_wrdnumber);
        btnClearGoodsID = findViewById(R.id.btn_clear_goodsid);
        btnClearDateTime = findViewById(R.id.btn_clear_Datetime);
        btnClearQuantity = findViewById(R.id.btn_clear_quantity);
        btnClearList = findViewById(R.id.btn_clear_data);
        btnSave = findViewById(R.id.btn_save_data);
        btnExit = findViewById(R.id.btn_exit);
        edtTotalQuantityOrg = findViewById(R.id.edtTotalQuantityOrg);
        edtTotalQuantity = findViewById(R.id.edtTotalQuantity);
        edtTotalGoodsOrg = findViewById(R.id.edtTotalGoodsOrg);
        edtTotalGoods = findViewById(R.id.edtTotalGoods);
        floatingActionButton = findViewById(R.id.floating_action_button);
//        receiptDataDAO = new ReceiptDataDAO(ReceiptDataActivity.this);
    }

    private void fetdata() {
        if (TextUtils.isEmpty(edtWRDNumber.getText().toString())) {
            alertError(getApplicationContext(), "Nhập số phiếu");
            edtWRDNumber.requestFocus();
            return;
        }
        Call<List<WRDataHeader>> callback = iWarehouseApi.getWRDataHeader(edtWRDNumber.getText().toString().trim().toUpperCase());
        callback.enqueue(new Callback<List<WRDataHeader>>() {
            @Override
            public void onResponse(Call<List<WRDataHeader>> call, Response<List<WRDataHeader>> response) {
                if (response.isSuccessful()) {
                    wrDataHeaders = (ArrayList<WRDataHeader>) response.body();
                    if (wrDataHeaders.size() > 0) {
                        alertSuccess(getApplicationContext(), "Lấy dữ liệu thành công");
                        wrDataHeader = wrDataHeaders.get(0);
                        fetchWRDataGeneralData(edtWRDNumber.getText().toString().toUpperCase());
                        hideSoftKeyBoard();
//                                    edtReferenceNumber.setText(wrDataHeader.getReferenceNumber());
                        edtTotalQuantityOrg.setText(String.format("%.0f", wrDataHeader.getTotalQuantityOrg()));
                        edtTotalQuantity.setText(String.format("%.0f", wrDataHeader.getTotalQuantity()));
                        floatingActionButton.setVisibility(View.GONE);
                        edtGoodsID.requestFocus();
                    }

                }
            }

            @Override
            public void onFailure(Call<List<WRDataHeader>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetch wrdataheader fail", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void clear() {
        edtGoodsID.setText("");
//        edtIDCode.setText("");
        edtQuantity.setText("");
    }

    private WRDataHeader fetchWRDataHeaderData(String wrdNumber) {
        Call<List<WRDataHeader>> callback = iWarehouseApi.getWRDataHeader(wrdNumber);
        callback.enqueue(new Callback<List<WRDataHeader>>() {
            @Override
            public void onResponse(Call<List<WRDataHeader>> call, Response<List<WRDataHeader>> response) {
                if (response.isSuccessful()) {
                    wrDataHeaders = (ArrayList<WRDataHeader>) response.body();
                    if (wrDataHeaders.size() > 0) {
                        wrDataHeader = wrDataHeaders.get(0);
                        hideSoftKeyBoard();
//                        edtReferenceNumber.setText(wrDataHeader.getReferenceNumber());
                        edtTotalQuantityOrg.setText(String.format("%.0f", wrDataHeader.getTotalQuantityOrg()));
                        edtTotalQuantity.setText(String.format("%.0f", wrDataHeader.getTotalQuantity()));
//                        edtGoodsID.requestFocus();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<WRDataHeader>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetch wrdataheader fail", Toast.LENGTH_SHORT).show();
            }
        });
        return wrDataHeader;
    }

    private ArrayList<WRDataGeneral> fetchWRDataGeneralData(String wrdNumber) {
        Call<List<WRDataGeneral>> callback = iWarehouseApi.getWRDataGeneralById(wrdNumber);
        callback.enqueue(new Callback<List<WRDataGeneral>>() {
            @Override
            public void onResponse(Call<List<WRDataGeneral>> call, Response<List<WRDataGeneral>> response) {
                if (response.isSuccessful()) {
                    wrDataGenerals = (ArrayList<WRDataGeneral>) response.body();
                    if (wrDataGenerals.size() > 0) {
                        edtTotalGoodsOrg.setText(wrDataGenerals.size() + "");
                        totalGoods = 0.0;
                        getTotalGoods(wrDataGenerals);
                        edtTotalGoods.setText(String.format("%.0f", totalGoods));
                    }
                    setAdapter(wrDataGenerals);
                }
            }

            @Override
            public void onFailure(Call<List<WRDataGeneral>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetch  wrdatadetail fail", Toast.LENGTH_SHORT).show();
            }
        });
        return wrDataGenerals;
    }

//    private ArrayList<WRDataDetail> fetchWRDataDetailData(String wrdNumber) {
//        Call<List<WRDataDetail>> callback = iWarehouseApi.getWRDataDetailById(wrdNumber);
//        callback.enqueue(new Callback<List<WRDataDetail>>() {
//            @Override
//            public void onResponse(Call<List<WRDataDetail>> call, Response<List<WRDataDetail>> response) {
//                if (response.isSuccessful()) {
//                    wrDataDetails = (ArrayList<WRDataDetail>) response.body();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<List<WRDataDetail>> call, Throwable t) {
//                Toast.makeText(getApplicationContext(), "Fetch  wrdatadetail fail", Toast.LENGTH_SHORT).show();
//            }
//        });
//        return wrDataDetails;
//    }

//    private ArrayList<WRDataGeneral> fetchWRDataGeneralByGoodsID(String wrdNumber, String goodsId) {
//        Call<List<WRDataGeneral>> callback = iWarehouseApi.getWRDataGeneralByGoods(wrdNumber, goodsId);
//        callback.enqueue(new Callback<List<WRDataGeneral>>() {
//            @Override
//            public void onResponse(Call<List<WRDataGeneral>> call, Response<List<WRDataGeneral>> response) {
//                if (response.isSuccessful()) {
//                    arrayListByGoodsID = (ArrayList<WRDataGeneral>) response.body();
//                    if (arrayListByGoodsID.size() > 0) {
//                        //isGoodsIDExist = true;
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<List<WRDataGeneral>> call, Throwable t) {
//                Toast.makeText(getApplicationContext(), "get wrdatadetail by idcode" + t.getMessage() + "", Toast.LENGTH_SHORT).show();
//            }
//        });
//        return arrayListByGoodsID;
//    }

//    private ArrayList<WRDataGeneral> fetchWRDataGeneralByIdCode(String wrdNumber, String goodsId, String idCode) {
//        Call<List<WRDataGeneral>> callback = iWarehouseApi.getWRDataGeneralByIDCode(wrdNumber, goodsId, idCode);
//        callback.enqueue(new Callback<List<WRDataGeneral>>() {
//            @Override
//            public void onResponse(Call<List<WRDataGeneral>> call, Response<List<WRDataGeneral>> response) {
//                if (response.isSuccessful()) {
//                    arrayListByIDCode = new ArrayList<>();
//                    arrayListByIDCode = (ArrayList<WRDataGeneral>) response.body();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<List<WRDataGeneral>> call, Throwable t) {
//                Toast.makeText(getApplicationContext(), "get wrdatadetail by idcode" + t.getMessage() + "", Toast.LENGTH_SHORT).show();
//            }
//        });
//        return arrayListByIDCode;
//    }

    private void putWRDataHeader(String wRDNumber, WRDataHeader wrDataHeader) {
        Call<WRDataHeader> callback = iWarehouseApi.updateWRDataHeader(wRDNumber, wrDataHeader);
        callback.enqueue(new Callback<WRDataHeader>() {
            @Override
            public void onResponse(Call<WRDataHeader> call, Response<WRDataHeader> response) {
                if (response.isSuccessful()) {
                    fetchWRDataHeaderData(wRDNumber);
                }
            }

            @Override
            public void onFailure(Call<WRDataHeader> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "put wrdataheader fail" + t.getMessage() + "", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void putWRDataGeneral(String wRDNumber, String goodsID, int ordinal, WRDataGeneral wrDataGeneral) {
        Call<WRDataGeneral> callback = iWarehouseApi.updateWRDataGeneral(wRDNumber, goodsID, ordinal, wrDataGeneral);
        callback.enqueue(new Callback<WRDataGeneral>() {
            @Override
            public void onResponse(Call<WRDataGeneral> call, Response<WRDataGeneral> response) {
                if (response.isSuccessful()) {
                    fetchWRDataGeneralData(wRDNumber);
                    totalQuantity = 0.0;
                    totalGoods = 0.0;
                    getTotalQuantity(wrDataGenerals);
                    getTotalGoods(wrDataGenerals);
                    edtTotalQuantity.setText(String.format("%.0f", totalQuantity));
                    edtTotalGoods.setText(String.format("%.0f", totalGoods));

                    WRDataHeader wrDataHeaderUpdate = wrDataHeader;
                    if(!TextUtils.isEmpty(edtQuantity.getText())) {
                        wrDataHeaderUpdate.setTotalQuantity(totalQuantity + Double.parseDouble(edtQuantity.getText().toString().toUpperCase()));
                    }else {
                        wrDataHeaderUpdate.setTotalQuantity(totalQuantity);
                    }
                    putWRDataHeader(wrDataHeaderUpdate.getWrdNumber(), wrDataHeaderUpdate);
                } else {
                    alertError(getApplicationContext(), "Cập nhật dữ liệu nhập thất bại " + response.message() + "-" + response.headers() + "-" + response.code() + "-" + response.errorBody() + "-" + response.errorBody().contentType());
                }

            }

            @Override
            public void onFailure(Call<WRDataGeneral> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "put wrdatadetail fail" + t.getMessage() + "", Toast.LENGTH_SHORT).show();
            }
        });
    }

//    private void putWRDataDetail(String wRDNumber, String goodsID, int ordinal, WRDataDetail wrDataDetail) {
//        Call<WRDataDetail> callback = iWarehouseApi.updateWRDataDetail(wRDNumber, goodsID, ordinal, wrDataDetail);
//        callback.enqueue(new Callback<WRDataDetail>() {
//            @Override
//            public void onResponse(Call<WRDataDetail> call, Response<WRDataDetail> response) {
//                if (response.isSuccessful()) {
//                    fetchWRDataDetailData(wRDNumber);
//                    Toast.makeText(getApplicationContext(), "Success!!!", Toast.LENGTH_SHORT).show();
//                } else {
//                    alertError(getApplicationContext(), "Cập nhật dữ liệu nhập thất bại");
//                }
//
//            }
//
//            @Override
//            public void onFailure(Call<WRDataDetail> call, Throwable t) {
//                Toast.makeText(getApplicationContext(), "put wrdatadetail fail" + t.getMessage() + "", Toast.LENGTH_SHORT).show();
//            }
//        });
//    }

    private void getTotalQuantity(ArrayList<WRDataGeneral> wrDataGenerals) {
        for (int i = 0; i < wrDataGenerals.size(); i++) {
            if (wrDataGenerals.size() > 0 && wrDataGenerals.get(i).getQuantity() > 0) {
                totalQuantity += wrDataGenerals.get(i).getQuantity();
            }
        }
    }

    private void getTotalGoods(ArrayList<WRDataGeneral> wrDataGenerals) {
        for (int i = 0; i < wrDataGenerals.size(); i++) {
            if (wrDataGenerals.get(i).getQuantity().doubleValue() > 0) {
                totalGoods += 1;
            }
        }
    }

    private Boolean checkGoodsIDDuplicate(ArrayList<WRDataGeneral> wrDataGenerals, String goodsID) {
        for (int i = 0; i < wrDataGenerals.size(); i++) {
            if (wrDataGenerals.get(i).getQuantity() > 0 && wrDataGenerals.get(i).getGoodsID().equalsIgnoreCase(goodsID)) {
                isGoodsIdDuplicate = true;
                break;
            }
        }
        return isGoodsIdDuplicate;
    }

    private Boolean checkExistGoodsID(String goodsID, ArrayList<WRDataGeneral> wrDataGenerals) {
        for (int i = 0; i < wrDataGenerals.size(); i++) {
            if (goodsID.equalsIgnoreCase(wrDataGenerals.get(i).getGoodsID())) {
                isGoodsIDExist = true;
                break;
            }
        }
        return isGoodsIDExist;
    }

    private Boolean checkGoodsIDAndIDCodeScaned(String goodsID, String idCode, ArrayList<WRDataGeneral> wrDataGenerals) {
        Boolean isBreak = false;
        for (int i = 0; i < wrDataGenerals.size(); i++) {
            String idCode2 = wrDataGenerals.get(i).getIdCode();
            ArrayList<String> listDateTime = new ArrayList<String>(Arrays.asList(idCode2.split(",")));
            for (int j = 0; j < listDateTime.size(); j++) {
                if (goodsID.equalsIgnoreCase(wrDataGenerals.get(i).getGoodsID()) && idCode.equalsIgnoreCase(listDateTime.get(j).toString().toUpperCase())
                        && wrDataGenerals.get(i).getQuantity().doubleValue() > 0) {
                    isGoodsIDAndIDCodeScanned = true;
                    isBreak = true;
                    break;
                }
                if (isBreak == true) {
                    break;
                }
            }
        }
        return isGoodsIDAndIDCodeScanned;
    }

    private Boolean checkGoodsIDAndDateScaned(String goodsID, String date, ArrayList<WRDataGeneral> wrDataGenerals1, ArrayList<GoodsData> goodsData1) {
        for (int i = 0; i < wrDataGenerals1.size(); i++) {
            for (int j = 0; j < goodsData1.size(); j++) {
                if (wrDataGenerals1.get(i).getQuantity().doubleValue() > 0 && goodsID.equalsIgnoreCase(goodsData1.get(j).getGoodsID()) && date.equalsIgnoreCase(goodsData1.get(j).getDate())) {
                    isGoodsIDAndDateScanned = true;
                    break;
                }
            }
        }
        return isGoodsIDAndDateScanned;
    }

    private ArrayList<WRDataGeneral> getNotYetList(ArrayList<WRDataGeneral> wrDataGenerals) {
        ArrayList<WRDataGeneral> notYetList = new ArrayList<>();
        for (int i = 0; i < wrDataGenerals.size(); i++) {
            if (wrDataGenerals.get(i).getQuantity() == 0) {
                notYetList.add(wrDataGenerals.get(i));
            }
        }
        return notYetList;
    }

    private ArrayList<WRDataGeneral> getEnoughtYetList(ArrayList<WRDataGeneral> wrDataGenerals) {
        ArrayList<WRDataGeneral> enoughYettList = new ArrayList<>();
        for (int i = 0; i < wrDataGenerals.size(); i++) {
            if (wrDataGenerals.get(i).getQuantity() > 0 && wrDataGenerals.get(i).getQuantity() < wrDataGenerals.get(i).getQuantityOrg()) {
                enoughYettList.add(wrDataGenerals.get(i));
            }
        }
        return enoughYettList;
    }

    private ArrayList<WRDataGeneral> getEnoughtList(ArrayList<WRDataGeneral> wrDataGenerals) {
        ArrayList<WRDataGeneral> enoughtList = new ArrayList<>();
        for (int i = 0; i < wrDataGenerals.size(); i++) {
            if (wrDataGenerals.get(i).getQuantity() > 0 && wrDataGenerals.get(i).getQuantity().doubleValue() == wrDataGenerals.get(i).getQuantityOrg().doubleValue()) {
                enoughtList.add(wrDataGenerals.get(i));
            }
        }
        return enoughtList;
    }

    private ArrayList<WRDataGeneral> getOvertList(ArrayList<WRDataGeneral> wrDataGenerals) {
        ArrayList<WRDataGeneral> overList = new ArrayList<>();
        for (int i = 0; i < wrDataGenerals.size(); i++) {
            if (wrDataGenerals.get(i).getQuantity() > wrDataGenerals.get(i).getQuantityOrg()) {
                overList.add(wrDataGenerals.get(i));
            }
        }
        return overList;
    }

    private void alertError(Context context, String message) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));
        dialog.setTitle("Error")
                .setIcon(R.drawable.ic_baseline_error_24)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                    }
                }).show();
    }

    private void alertSuccess(Context context, String message) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));
        dialog.setTitle("Success")
                .setIcon(R.drawable.ic_baseline_done_24)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                    }
                }).show();
    }

    private void setAdapter(ArrayList<WRDataGeneral> wrDataGenerals) {
        adapter = new WRDataDetailAdapter2(getApplicationContext(), wrDataGenerals,
                new WRDataDetailAdapter2.ItemClickListener() {
                    @Override
                    public void onClick(WRDataGeneral wrDataGeneral) {
                    }
                },
                new WRDataDetailAdapter2.ItemEditNoteClickListener() {
                    @Override
                    public void onClick(WRDataGeneral wrDataGeneral) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(ReceiptDataActivity.this);
                        ViewGroup viewGroup = findViewById(android.R.id.content);
                        View dialogView = LayoutInflater.from(ReceiptDataActivity.this).inflate(R.layout.dialog_edit_note_wrdatageneral, viewGroup, false);
                        builder.setView(dialogView);
                        final AlertDialog alertDialog = builder.create();
                        alertDialog.show();

                        TextInputEditText edtNote = alertDialog.findViewById(R.id.edtNote);
                        TextInputEditText edtPartNumber = alertDialog.findViewById(R.id.edtGoodsID);
                        AppCompatButton btnSave = alertDialog.findViewById(R.id.btnSave);

                        edtPartNumber.setText(wrDataGeneral.getGoodsID());
                        edtNote.setText(wrDataGeneral.getNote());

                        btnSave.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                WRDataGeneral wrDataGeneral1 = wrDataGeneral;
                                wrDataGeneral1.setNote(edtNote.getText().toString());
                                try {
                                    wrDataGeneral1.setEditedDateTime(simpleDateFormat.parse(simpleDateFormat.format(Calendar.getInstance().getTime())));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                putWRDataGeneral(wrDataGeneral1.getWrdNumber(), wrDataGeneral1.getGoodsID(), wrDataGeneral1.getOrdinal(), wrDataGeneral1);
                                Toast.makeText(ReceiptDataActivity.this, "Success", Toast.LENGTH_SHORT).show();
                                alertDialog.dismiss();
                            }
                        });
                    }
                });
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void hideSoftKeyBoard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);

        if (imm.isAcceptingText()) { // verify if the soft keyboard is open
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.action_bar_wrdatadetail_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        if (id == R.id.action_all) {
            ArrayList<WRDataGeneral> allList = wrDataGenerals;
            setAdapter(allList);
            return true;
        }
        if (id == R.id.action_not_yet) {
            ArrayList<WRDataGeneral> notYetList = getNotYetList(wrDataGenerals);
            setAdapter(notYetList);
            return true;
        }
        if (id == R.id.action_enought_yet) {
            ArrayList<WRDataGeneral> enoughtYetLish = getEnoughtYetList(wrDataGenerals);
            setAdapter(enoughtYetLish);
            return true;
        }
        if (id == R.id.action_enought) {
            ArrayList<WRDataGeneral> enoughtList = getEnoughtList(wrDataGenerals);
            setAdapter(enoughtList);
            return true;
        }
        if (id == R.id.action_over) {
            ArrayList<WRDataGeneral> overList = getOvertList(wrDataGenerals);
            setAdapter(overList);
            return true;
        }
//        if (id == R.id.action_save) {
//            process();
//            return true;
//        }
//        if (id == R.id.action_approve) {
//            if (wrDataHeader != null) {
//                WRDataHeader wrDataHeaderUpdate = wrDataHeader;
//                wrDataHeaderUpdate.setHandlingStatusID("2");
//                wrDataHeaderUpdate.setHandlingStatusName("Duyệt mức 2");
//                putWRDataHeader(edtWRDNumber.getText().toString().toUpperCase(), wrDataHeaderUpdate);
//                alertSuccess(getApplicationContext(), "Duyệt mức 2 nhập kho thành công");
//            }
//            return true;
//        }
//        if (id == R.id.action_clearlist) {
//            if(wrDataGenerals.size()>0) {
//                clearlist();
//            }
//            return true;
//        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onOpened(EMDKManager emdkManager) {
        this.emdkManager = emdkManager;
        // Acquire the barcode manager resources
        barcodeManager = (BarcodeManager) emdkManager.getInstance(FEATURE_TYPE.BARCODE);
        // Add connection listener
        if (barcodeManager != null) {
            barcodeManager.addConnectionListener(this);
        }
        // Enumerate scanner devices
        enumerateScannerDevices();

        //
        initScanner();

        stopScan();

    }

    @Override
    protected void onResume() {
        super.onResume();
        // The application is in foreground
        if (emdkManager != null) {
            // Acquire the barcode manager resources
            initBarcodeManager();
            // Enumerate scanner devices
            enumerateScannerDevices();
            // Initialize scanner
            initScanner();
            stopScan();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        // The application is in background
        // Release the barcode manager resources
        deInitScanner();
        deInitBarcodeManager();
    }

    @Override
    public void onClosed() {
        // Release all the resources
        if (emdkManager != null) {
            emdkManager.release();
            emdkManager = null;
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Release all the resources
        if (emdkManager != null) {
            emdkManager.release();
            emdkManager = null;
        }
    }

    @Override
    public void onConnectionChange(ScannerInfo scannerInfo, ConnectionState connectionState) {
        String status;
        String scannerName = "";
        String statusExtScanner = connectionState.toString().toUpperCase();
        String scannerNameExtScanner = scannerInfo.getFriendlyName();
        if (deviceList.size() != 0) {
            scannerName = deviceList.get(1).getFriendlyName();
        }
        if (scannerName.equalsIgnoreCase(scannerNameExtScanner)) {
            switch (connectionState) {
                case CONNECTED:
                    synchronized (lock) {
                        initScanner();
                        setDecoders();
                        bExtScannerDisconnected = false;
                    }
                    break;
                case DISCONNECTED:
                    bExtScannerDisconnected = true;
                    synchronized (lock) {
                        deInitScanner();
                    }
                    break;
            }
        } else {
            bExtScannerDisconnected = false;
        }
    }

    @Override
    public void onData(ScanDataCollection scanDataCollection) {
        if ((scanDataCollection != null) && (scanDataCollection.getResult() == ScannerResults.SUCCESS)) {
            ArrayList<ScanData> scanData = scanDataCollection.getScanData();
            for (ScanData data : scanData) {
                updateData(data.getData());
            }
        }
    }

    @Override
    public void onStatus(StatusData statusData) {
        ScannerStates state = statusData.getState();
        switch (state) {
            case IDLE:
//                statusString = statusData.getFriendlyName() + " is enabled and idle...";
//                updateStatus(statusString);
                if (bContinuousMode) {
                    try {
                        scanner.triggerType = TriggerType.SOFT_ALWAYS;
                        // An attempt to use the scanner continuously and rapidly (with a delay < 100 ms between scans)
                        // may cause the scanner to pause momentarily before resuming the scanning.
                        // Hence add some delay (>= 100ms) before submitting the next read.
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        scanner.read();
                    } catch (ScannerException e) {
                        updateStatus(e.getMessage());
                    }
                }
                // set trigger type
                if (bSoftTriggerSelected) {
                    scanner.triggerType = TriggerType.SOFT_ONCE;
                    bSoftTriggerSelected = false;
                } else {
                    scanner.triggerType = TriggerType.HARD;
                }
                // set decoders
                if (bDecoderSettingsChanged) {
                    setDecoders();
                    bDecoderSettingsChanged = false;
                } else {
                    setDecoders();
                }
                // submit read
                if (!scanner.isReadPending() && !bExtScannerDisconnected) {
                    try {
                        scanner.read();
                    } catch (ScannerException e) {
                    }
                }
                break;
            case WAITING:
                break;
            case SCANNING:
                break;
            case DISABLED:
                break;
            case ERROR:
                break;
            default:
                break;
        }
    }

    private void initScanner() {
        if (scanner == null) {
            if ((deviceList != null) && (deviceList.size() != 0)) {
                if (barcodeManager != null)
                    scanner = barcodeManager.getDevice(deviceList.get(1));
            } else {
//                updateStatus("Failed to get the specified scanner device! Please close and restart the application.");
                return;
            }
            if (scanner != null) {
                scanner.addDataListener(this);
                scanner.addStatusListener(this);
                try {
                    scanner.enable();
                } catch (ScannerException e) {
                    updateStatus(e.getMessage());
                    deInitScanner();
                }
            } else {
//                updateStatus("Failed to initialize the scanner device.");
            }
        }
    }

    private void deInitScanner() {
        if (scanner != null) {
            try {
                scanner.disable();
            } catch (Exception e) {
//                updateStatus(e.getMessage());
            }

            try {
                scanner.removeDataListener(this);
                scanner.removeStatusListener(this);
            } catch (Exception e) {
//                updateStatus(e.getMessage());
            }

            try {
                scanner.release();
            } catch (Exception e) {
//                updateStatus(e.getMessage());
            }
            scanner = null;
        }
    }

    private void initBarcodeManager() {
        barcodeManager = (BarcodeManager) emdkManager.getInstance(FEATURE_TYPE.BARCODE);
        // Add connection listener
        if (barcodeManager != null) {
            barcodeManager.addConnectionListener(this);
        }
    }

    private void deInitBarcodeManager() {
        if (emdkManager != null) {
            emdkManager.release(FEATURE_TYPE.BARCODE);
        }
    }


    private void enumerateScannerDevices() {
        if (barcodeManager != null) {
            deviceList = barcodeManager.getSupportedDevicesInfo();
            if ((deviceList != null) && (deviceList.size() != 0)) {

            } else {
            }
        }
    }

    private void setDecoders() {
        if (scanner != null) {
            try {
                ScannerConfig config = scanner.getConfig();
                // Set EAN8
                config.decoderParams.ean8.enabled = true;
                // Set EAN13
                config.decoderParams.ean13.enabled = true;
                // Set Code39
                config.decoderParams.code39.enabled = true;
                //Set Code128
                config.decoderParams.code128.enabled = true;
                //set inverser1Mode
                config.readerParams.readerSpecific.laserSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                config.readerParams.readerSpecific.cameraSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                config.readerParams.readerSpecific.imagerSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                scanner.setConfig(config);
            } catch (ScannerException e) {
                updateStatus(e.getMessage());
            }
        }
    }

    private void setReaderParams() {
        if (scanner != null) {
            try {
                ScannerConfig config = scanner.getConfig();
                config.readerParams.readerSpecific.laserSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                scanner.setConfig(config);
            } catch (ScannerException e) {
                updateStatus(e.getMessage());
            }
        }
    }

    private void stopScan() {

        if (scanner != null) {
            // Cancel the pending read.
            try {
                scanner.cancelRead();
            } catch (ScannerException e) {
                e.printStackTrace();
            }
        }
    }

    private void cancelRead() {
        if (scanner != null) {
            if (scanner.isReadPending()) {
                try {
                    scanner.cancelRead();
                } catch (ScannerException e) {
                }
            }
        }
    }

    private void updateStatus(final String status) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
            }
        });
    }

    private void updateData(final String result) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (result != null) {
                    if (edtWRDNumber.hasFocus()) {
                        edtWRDNumber.setText(result);
                    } else {
                        String goodsID = "";
                        String quantity = "";
                        String idCode = "";
                        String stt = "";
                        int index1 = result.indexOf("-");
                        int index2 = result.indexOf("-", index1 + 1);
                        int index3 = result.lastIndexOf("-");
                        if (index1 > 0) {
                            if (index2 > 0) {
                                goodsID = result.substring(0, index1);
                                quantity = result.substring(index1 + 1, index2);
                                idCode = result.substring(index2 + 1, result.length());
                                stt = result.substring(index3 + 1, result.length());
                            } else {
                                goodsID = result.substring(0, index1);
                                quantity = result.substring(index1 + 1, result.length());
                                idCode = "";
                                stt = "";
                            }
                        }
                        edtGoodsID.setText(goodsID);
                        edtQuantity.setText(quantity);
                        edtDateTime.setText(idCode);
                    }

                    process();
                }
            }
        });
    }

//    private void addGoodsData(GoodsData goodsData) {
//        receiptDataDAO.insert(goodsData);
//    }
//
//    private ArrayList<GoodsData> getGoodsDataByWRDNumber(String wrdNumber) {
//        return receiptDataDAO.getByWRDNumber(wrdNumber);
//    }
//
//    private void deleteGoodsDataByWRDNumber(String wrdNumber) {
//        receiptDataDAO.deleteByWRDNumber(wrdNumber);
//    }

    private void process() {
        if (TextUtils.isEmpty(edtGoodsID.getText().toString().toUpperCase())) {
            alertError(getApplicationContext(), "Nhập mã hàng");
            edtGoodsID.requestFocus();
            return;
        }
//        if (TextUtils.isEmpty(edtQuantity.getText().toString().toUpperCase())) {
//            alertError(getApplicationContext(), "Nhập số lượng");
//            edtQuantity.requestFocus();
//            return;
//        }
//        if (!edtQuantity.getText().toString().toUpperCase().matches(numberRegex)) {
//            alertError(getApplicationContext(), "Không phải là số");
//            return;
//        }

        isGoodsIDExist = false;
        checkExistGoodsID(edtGoodsID.getText().toString().toUpperCase(), wrDataGenerals);
        if (isGoodsIDExist == true) {
            isGoodsIDExist = false;
        } else {
            alertError(getApplicationContext(), "Mã hàng không tồn tại");
            return;
        }


//        goodsDataArrayList = getGoodsDataByWRDNumber(edtWRDNumber.getText().toString().toUpperCase());
        isGoodsIDAndIDCodeScanned = false;
        checkGoodsIDAndIDCodeScaned(edtGoodsID.getText().toString().toUpperCase(), edtDateTime.getText().toString().toUpperCase(), wrDataGenerals);
        if (isGoodsIDAndIDCodeScanned == true) {
            alertError(getApplicationContext(), "Đã scan.Scan mã hàng khác");

            try {
                AssetFileDescriptor afd = null;
                afd = getAssets().openFd("error.mp3");
                MediaPlayer player = new MediaPlayer();
                player.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                player.prepare();
                player.start();
                Thread.sleep(200);
                player.stop();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return;
        } else {
            try {
                AssetFileDescriptor afd = null;
                afd = getAssets().openFd("ok.mp3");
                MediaPlayer player = new MediaPlayer();
                player.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                player.prepare();
                player.start();
                Thread.sleep(200);
                player.stop();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
//        goodsData.setWrdNumber(edtWRDNumber.getText().toString().toUpperCase());
//        goodsData.setGoodsID(edtGoodsID.getText().toString().toUpperCase());
//        goodsData.setDate(edtDateTime.getText().toString().toUpperCase());
//        goodsData.setStatus("OK");
//        addGoodsData(goodsData);


        totalQuantity = 0.0;
        totalGoods = 0.0;
        getTotalGoods(wrDataGenerals);
        getTotalQuantity(wrDataGenerals);
        Call<List<WRDataGeneral>> callback = iWarehouseApi.getWRDataGeneralByGoods(edtWRDNumber.getText().toString().toUpperCase(), edtGoodsID.getText().toString().toUpperCase());
        callback.enqueue(new Callback<List<WRDataGeneral>>() {
            @Override
            public void onResponse(Call<List<WRDataGeneral>> call, Response<List<WRDataGeneral>> response) {
                if (response.isSuccessful()) {
                    arrayListByGoodsID = (ArrayList<WRDataGeneral>) response.body();
                    if (arrayListByGoodsID.size() > 0) {
                        WRDataGeneral wrDataGeneralUpdate = arrayListByGoodsID.get(0);
                        checkGoodsIDDuplicate(arrayListByGoodsID, edtGoodsID.getText().toString().toUpperCase());
                        try {
                            if (isGoodsIdDuplicate == false) {
                                wrDataGeneralUpdate.setQuantity(Double.parseDouble(edtQuantity.getText().toString().toUpperCase()));
                                wrDataGeneralUpdate.setTotalGoods(totalGoods + 1);
                            } else {
                                wrDataGeneralUpdate.setQuantity(wrDataGeneralUpdate.getQuantity() + Double.parseDouble(edtQuantity.getText().toString().toUpperCase()));
                                wrDataGeneralUpdate.setTotalGoods(totalGoods);
                                isGoodsIdDuplicate = false;
                            }

                            if (wrDataGeneralUpdate.getStartedDateTime() == null) {
                                wrDataGeneralUpdate.setStartedDateTime(simpleDateFormat.parse(simpleDateFormat.format(Calendar.getInstance().getTime())));
                            }
                            wrDataGeneralUpdate.setEditedDateTime(simpleDateFormat.parse(simpleDateFormat.format(Calendar.getInstance().getTime())));
                            wrDataGeneralUpdate.setWrdNumber(arrayListByGoodsID.get(0).getWrdNumber());
                            wrDataGeneralUpdate.setGoodsID(edtGoodsID.getText().toString().toUpperCase());
                            wrDataGeneralUpdate.setIdCode(wrDataGeneralUpdate.getIdCode() + edtDateTime.getText().toString().toUpperCase() + ",");
                            wrDataGeneralUpdate.setOrdinal(arrayListByGoodsID.get(0).getOrdinal());
                            wrDataGeneralUpdate.setTotalQuantity(totalQuantity + Double.parseDouble(edtQuantity.getText().toString().toUpperCase()));
                            putWRDataGeneral(wrDataGeneralUpdate.getWrdNumber(), wrDataGeneralUpdate.getGoodsID(), wrDataGeneralUpdate.getOrdinal(), wrDataGeneralUpdate);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<List<WRDataGeneral>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "get wrdatadetail by idcode" + t.getMessage() + "", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void clearlist() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(ReceiptDataActivity.this);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.dialog_clear_list, viewGroup, false);
        builder.setView(dialogView);
        final android.app.AlertDialog alertDialog = builder.create();
        alertDialog.show();

        Button btnYes, btnNo;
        btnYes = alertDialog.findViewById(R.id.btnYes);
        btnNo = alertDialog.findViewById(R.id.btnNo);
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int i = 0; i < wrDataGenerals.size(); i++) {
                    WRDataGeneral wrDataGeneralUpdate = wrDataGenerals.get(i);

                    wrDataGeneralUpdate.setEditedDateTime(null);
                    wrDataGeneralUpdate.setStartedDateTime(null);
                    wrDataGeneralUpdate.setQuantity(0.0);
                    wrDataGeneralUpdate.setTotalGoods(0.0);
                    wrDataGeneralUpdate.setTotalQuantity(0.0);
                    wrDataGeneralUpdate.setIdCode("");
                    putWRDataGeneral(wrDataGeneralUpdate.getWrdNumber(), wrDataGeneralUpdate.getGoodsID(), wrDataGeneralUpdate.getOrdinal(), wrDataGeneralUpdate);
                }
                WRDataHeader wrDataHeaderUpdate = wrDataHeader;
                wrDataHeaderUpdate.setTotalQuantity(0.0);
                putWRDataHeader(edtWRDNumber.getText().toString().toUpperCase(), wrDataHeaderUpdate);
//                deleteGoodsDataByWRDNumber(edtWRDNumber.getText().toString().toUpperCase());
                alertDialog.cancel();
                Toast.makeText(ReceiptDataActivity.this, "Đã xóa dữ liệu quét" + edtWRDNumber.getText().toString().toUpperCase(), Toast.LENGTH_SHORT).show();
            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.cancel();
            }
        });
    }
}