package com.example.warehousemobileclient.model;

public class Location {
    private String locationID;

    public Location() {
    }

    public Location(String locationID) {
        this.locationID = locationID;
    }

    public String getLocationID() {
        return locationID;
    }

    public void setLocationID(String locationID) {
        this.locationID = locationID;
    }
}
