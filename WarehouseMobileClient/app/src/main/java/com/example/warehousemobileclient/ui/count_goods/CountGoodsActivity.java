package com.example.warehousemobileclient.ui.count_goods;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.warehousemobileclient.R;
import com.example.warehousemobileclient.adapter.CheckGoodsAdapter;
import com.example.warehousemobileclient.adapter.CountGoodsAdapter;
import com.example.warehousemobileclient.dao.CountGoodsDAO;
import com.example.warehousemobileclient.dao.LocationDAO;
import com.example.warehousemobileclient.helper.CSVReadFile;
import com.example.warehousemobileclient.helper.ConnectFTPServer;
import com.example.warehousemobileclient.helper.NetworkHelper;
import com.example.warehousemobileclient.model.CountGoods;
import com.example.warehousemobileclient.model.GoodsData;
import com.example.warehousemobileclient.model.Location;
import com.example.warehousemobileclient.model.TSDetail;
import com.example.warehousemobileclient.model.WRDataGeneral;
import com.example.warehousemobileclient.model.WRDataHeader;
import com.example.warehousemobileclient.service.IWarehouseApi;
import com.example.warehousemobileclient.service.RetrofitService;
import com.example.warehousemobileclient.ui.check_goods.CheckGoodsActivity;
import com.example.warehousemobileclient.ui.check_goods.ListCheckGoodsActivity;
import com.symbol.emdk.EMDKManager;
import com.symbol.emdk.EMDKResults;
import com.symbol.emdk.barcode.BarcodeManager;
import com.symbol.emdk.barcode.ScanDataCollection;
import com.symbol.emdk.barcode.Scanner;
import com.symbol.emdk.barcode.ScannerConfig;
import com.symbol.emdk.barcode.ScannerException;
import com.symbol.emdk.barcode.ScannerInfo;
import com.symbol.emdk.barcode.ScannerResults;
import com.symbol.emdk.barcode.StatusData;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CountGoodsActivity extends AppCompatActivity implements EMDKManager.EMDKListener, Scanner.DataListener, Scanner.StatusListener, BarcodeManager.ScannerConnectionListener {
    private EMDKManager emdkManager = null;
    private BarcodeManager barcodeManager = null;
    private Scanner scanner = null;
    private Spinner spinnerScannerDevices = null;
    private List<ScannerInfo> deviceList = null;
    private final Object lock = new Object();
    private boolean bSoftTriggerSelected = false;
    private boolean bDecoderSettingsChanged = false;
    private boolean bExtScannerDisconnected = false;
    private boolean bContinuousMode = false;

    private AutoCompleteTextView edtLocationID = null;
    private EditText edtGoodsID = null;
    private EditText edtQuantity = null;
    private RecyclerView recyclerView = null;
    private EditText edtTotalQuantity = null;
    private EditText edtTotalGoodsID = null;
    private Button btnSave = null;
    private Button btnDelete = null;
    private Button btnDeleteAll = null;
    private Button btnExit = null;
    private Button btnClearLocationID = null;
    private Button btnClearGoodsID = null;
    private Button btnClearQuantity = null;

    ArrayList<CountGoods> countGoodsArrayList = null;
    ArrayList<CountGoods> countGoodsByMultiID = null;
    CountGoodsAdapter adapter = null;
    CountGoodsDAO countGoodsDAO = null;
    CountGoods countGoods = null;

    ArrayList<Location> locationArrayList = null;
    LocationDAO locationDAO = null;
    Location location = null;
    int totalLocation = 0;

    Boolean isGoodsIdDuplicate = false;

    IWarehouseApi iWarehouseApi = RetrofitService.getService();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_count_goods);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Đếm hàng");

        EMDKResults results = EMDKManager.getEMDKManager(getApplicationContext(), this);
        if (results.statusCode != EMDKResults.STATUS_CODE.SUCCESS) {
            updateStatus("EMDKManager object request failed!");
            return;
        }

        initComponent();

        //autocomplete location
        locationArrayList = fetchLocations();

        edtLocationID.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (editable.length() >= 1) {
//                    countGoodsArrayList = countGoodsDAO.getByLocationID(edtLocationID.getText().toString().toUpperCase());
                    countGoodsArrayList = fetchCountGoodsByLocationID(edtLocationID.getText().toString().toUpperCase());
                    if (checkExistLocation(edtLocationID.getText().toString().toUpperCase(), locationArrayList) == true) {
                        hideSoftKeyBoard();
                        edtGoodsID.requestFocus();
                    }
                }
            }
        });


        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isGoodsIdDuplicate = false;
                checkGoodsIDDuplicate(countGoodsArrayList, edtLocationID.getText().toString().toUpperCase(), edtGoodsID.getText().toString().toUpperCase());
                if (isGoodsIdDuplicate == true) {
                    alertSuccess(getApplicationContext(), "Mã hàng " + edtGoodsID.getText().toString().toUpperCase() + "thuộc location " + edtLocationID.getText().toString().toUpperCase() +
                    " đã có trong danh sách đếm hàng,dữ liệu sẽ được cập nhật");
                    edit();
                    isGoodsIdDuplicate = false;
                } else {
                    add();
                }

            }
        });
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DeleteByMultiID(edtLocationID.getText().toString().toUpperCase(),edtGoodsID.getText().toString().toUpperCase());
            }
        });
        btnDeleteAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DeleteByLocationID(edtLocationID.getText().toString().toUpperCase());

            }
        });
        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        btnClearLocationID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtLocationID.setText("");
            }
        });
        btnClearGoodsID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtGoodsID.setText("");
            }
        });
        btnClearQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtQuantity.setText("");
            }
        });
    }

    private void initComponent() {
        edtLocationID = findViewById(R.id.edtLocationID);
        edtGoodsID = findViewById(R.id.edtGoodsID);
        edtQuantity = findViewById(R.id.edtQuantity);
        recyclerView = findViewById(R.id.recycler_view);
        edtTotalQuantity = findViewById(R.id.edtTotalQuantity);
        edtTotalGoodsID = findViewById(R.id.edtTotalGoods);
        btnSave = findViewById(R.id.btn_save_data);
        btnDelete = findViewById(R.id.btn_clear_data);
        btnDeleteAll = findViewById(R.id.btn_clear_all_data);
        btnExit = findViewById(R.id.btn_exit);
        btnClearLocationID = findViewById(R.id.btn_clear_locationid);
        btnClearGoodsID = findViewById(R.id.btn_clear_goodsid);
        btnClearQuantity = findViewById(R.id.btn_clear_quantity);
        countGoodsArrayList = new ArrayList<CountGoods>();
        countGoodsDAO = new CountGoodsDAO(CountGoodsActivity.this);
        locationArrayList = new ArrayList<Location>();
        locationDAO = new LocationDAO(CountGoodsActivity.this);
        countGoods = new CountGoods();
        location = new Location();
    }

    private ArrayList<Location> fetchLocations() {
        Call<List<Location>> callback = iWarehouseApi.getLocations();
        callback.enqueue(new Callback<List<Location>>() {
            @Override
            public void onResponse(Call<List<Location>> call, Response<List<Location>> response) {
                if (response.isSuccessful()) {
                    locationArrayList = (ArrayList<Location>) response.body();
                    ArrayList<String> stringArrayList = new ArrayList<>();
                    for (Location location : locationArrayList) {
                        stringArrayList.add(location.getLocationID());
                    }
                    ArrayAdapter adapterCountries
                            = new ArrayAdapter(CountGoodsActivity.this, android.R.layout.simple_list_item_1, stringArrayList);
                    edtLocationID.setAdapter(adapterCountries);
                    edtLocationID.setThreshold(1);
                }
            }

            @Override
            public void onFailure(Call<List<Location>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetch  wrdatadetail fail", Toast.LENGTH_SHORT).show();
            }
        });
        return locationArrayList;
    }

    private int totalQuantityByLocationID(ArrayList<CountGoods> countGoodss) {
        int total = 0;
        for (CountGoods countGoods : countGoodss) {
            total += countGoods.getQuantity();
        }
        return total;
    }

    private int totalGoodsByLocationID(ArrayList<CountGoods> countGoodss) {
        int total = 0;
        total = countGoodss.size();
        return total;
    }

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
        }
    }

    private void setAdapter(ArrayList<CountGoods> countGoods) {
        adapter = new CountGoodsAdapter(CountGoodsActivity.this, countGoods, new CountGoodsAdapter.ItemClickListener() {
            @Override
            public void onClick(CountGoods countGoods1) {
                edtGoodsID.setText(countGoods1.getGoodsID());
                edtQuantity.setText(countGoods1.getQuantity() + "");
            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void addLocation(Location location) {
        totalLocation = 0;
        locationArrayList = locationDAO.getAll();
        for (Location location1 : locationArrayList) {
            if (!location.getLocationID().equalsIgnoreCase(location1.getLocationID())) {
                locationDAO.insert(location);
                totalLocation++;
            }
        }
    }

    private void add() {
        if (TextUtils.isEmpty(edtLocationID.getText().toString().toUpperCase())) {
            alertError(getApplicationContext(), "Chưa nhập location id");
            edtLocationID.requestFocus();
            return;
        }
        if (TextUtils.isEmpty(edtGoodsID.getText().toString().toUpperCase())) {
            alertError(getApplicationContext(), "Scan mã hàng");
            edtGoodsID.requestFocus();
            return;
        }
        if (TextUtils.isEmpty(edtQuantity.getText().toString().toUpperCase())) {
            alertError(getApplicationContext(), "Chưa nhập số lượng");
            edtQuantity.requestFocus();
            return;
        }

//        if (countGoodsDAO.checkExist(edtLocationID.getText().toString().toUpperCase(), edtGoodsID.getText().toString().toUpperCase()) == true) {
//            alertError(getApplicationContext(), "Mã hàng " + edtGoodsID.getText().toString().toUpperCase() + "thuộc location " + edtLocationID.getText().toString().toUpperCase() +
//                    " đã đếm");
//            return;
//        }
        if (checkExistLocation(edtLocationID.getText().toString().toUpperCase(), locationArrayList) == false) {
            alertError(getApplicationContext(), "Location " + edtLocationID.getText().toString().toUpperCase() + " không đúng");
            return;
        }
        countGoods.setLocationID(edtLocationID.getText().toString().toUpperCase());
        countGoods.setGoodsID(edtGoodsID.getText().toString().toUpperCase());
        countGoods.setQuantity(Double.parseDouble(edtQuantity.getText().toString().toUpperCase()));
        countGoods.setStatus("New");
        countGoods.setCountDate(simpleDateFormat.format(Calendar.getInstance().getTime()));
        //post to sqllite
//        countGoodsDAO.insert(countGoods);
        //post to sql server
        postCountGoodsToServer(countGoods);
//        countGoodsArrayList = countGoodsDAO.getByLocationID(edtLocationID.getText().toString().toUpperCase());

        edtTotalGoodsID.setText(totalGoodsByLocationID(countGoodsArrayList) + "");
        edtTotalQuantity.setText(totalQuantityByLocationID(countGoodsArrayList) + "");
    }

    private void edit() {
        if (TextUtils.isEmpty(edtLocationID.getText().toString().toUpperCase())) {
            alertError(getApplicationContext(), "Chưa nhập location id");
            edtLocationID.requestFocus();
            return;
        }
        if (TextUtils.isEmpty(edtGoodsID.getText().toString().toUpperCase())) {
            alertError(getApplicationContext(), "Scan mã hàng");
            edtGoodsID.requestFocus();
            return;
        }
        if (TextUtils.isEmpty(edtQuantity.getText().toString().toUpperCase())) {
            alertError(getApplicationContext(), "Chưa nhập số lượng");
            edtQuantity.requestFocus();
            return;
        }

        if (checkExistLocation(edtLocationID.getText().toString().toUpperCase(), locationArrayList) == false) {
            alertError(getApplicationContext(), "Location " + edtLocationID.getText().toString().toUpperCase() + " không đúng");
            return;
        }
        countGoods.setLocationID(edtLocationID.getText().toString().toUpperCase());
        countGoods.setGoodsID(edtGoodsID.getText().toString().toUpperCase());
        countGoods.setQuantity(Double.parseDouble(edtQuantity.getText().toString().toUpperCase()));
        countGoods.setStatus("Update");
        countGoods.setCountDate(simpleDateFormat.format(Calendar.getInstance().getTime()));
        //put to sqllite
//        countGoodsDAO.update(countGoods);
        //put to sql server
        putCountGoodsToServer(edtLocationID.getText().toString().toUpperCase(), edtGoodsID.getText().toString().toUpperCase(), countGoods);
        edtTotalGoodsID.setText(totalGoodsByLocationID(countGoodsArrayList) + "");
        edtTotalQuantity.setText(totalQuantityByLocationID(countGoodsArrayList) + "");
    }

    private void deleteByLocationAndGoods() {
        countGoodsDAO.deleteByID(edtLocationID.getText().toString().toUpperCase(), edtGoodsID.getText().toString().toUpperCase());
        fetchCountGoodsByLocationID(edtLocationID.getText().toString().toUpperCase());
        edtTotalGoodsID.setText(totalGoodsByLocationID(countGoodsArrayList) + "");
        edtTotalQuantity.setText(totalQuantityByLocationID(countGoodsArrayList) + "");
    }

    private void deleteByLocation() {
        countGoodsDAO.deleteByID(edtLocationID.getText().toString().toUpperCase());
        fetchCountGoodsByLocationID(edtLocationID.getText().toString().toUpperCase());
        edtTotalGoodsID.setText(totalGoodsByLocationID(countGoodsArrayList) + "");
        edtTotalQuantity.setText(totalQuantityByLocationID(countGoodsArrayList) + "");
    }

    private Boolean checkExistLocation(String locationID, ArrayList<Location> locations) {
        Boolean flag = false;
        for (int i = 0; i < locations.size(); i++) {
            if (locations.size() > 0 && locationID.equalsIgnoreCase(locations.get(i).getLocationID())) {
                flag = true;
                break;
            } else {
                flag = false;
            }
        }
        return flag;
    }

    private Boolean checkGoodsIDDuplicate(ArrayList<CountGoods> countGoodsArrayList, String locationID, String goodsID) {
        for (int i = 0; i < countGoodsArrayList.size(); i++) {
            if (countGoodsArrayList.get(i).getLocationID().equalsIgnoreCase(locationID) && countGoodsArrayList.get(i).getGoodsID().equalsIgnoreCase(goodsID)) {
                isGoodsIdDuplicate = true;
                break;
            }
        }
        return isGoodsIdDuplicate;
    }




    private void alertError(Context context, String message) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));
        dialog.setTitle("Error")
                .setIcon(R.drawable.ic_baseline_error_24)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                    }
                }).show();
    }

    private void alertSuccess(Context context, String message) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));
        dialog.setTitle("Success")
                .setIcon(R.drawable.ic_baseline_done_24)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                    }
                }).show();
    }

    private void hideSoftKeyBoard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);

        if (imm.isAcceptingText()) { // verify if the soft keyboard is open
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onOpened(EMDKManager emdkManager) {
        this.emdkManager = emdkManager;
        // Acquire the barcode manager resources
        barcodeManager = (BarcodeManager) emdkManager.getInstance(EMDKManager.FEATURE_TYPE.BARCODE);
        // Add connection listener
        if (barcodeManager != null) {
            barcodeManager.addConnectionListener(this);
        }
        // Enumerate scanner devices
        enumerateScannerDevices();

        //
        initScanner();

        stopScan();

    }

    @Override
    protected void onResume() {
        super.onResume();
        // The application is in foreground
        if (emdkManager != null) {
            // Acquire the barcode manager resources
            initBarcodeManager();
            // Enumerate scanner devices
            enumerateScannerDevices();
            // Initialize scanner
            initScanner();
            stopScan();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        // The application is in background
        // Release the barcode manager resources
        deInitScanner();
        deInitBarcodeManager();
    }

    @Override
    public void onClosed() {
        // Release all the resources
        if (emdkManager != null) {
            emdkManager.release();
            emdkManager = null;
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Release all the resources
        if (emdkManager != null) {
            emdkManager.release();
            emdkManager = null;
        }
    }

    @Override
    public void onConnectionChange(ScannerInfo scannerInfo, BarcodeManager.ConnectionState connectionState) {
        String status;
        String scannerName = "";
        String statusExtScanner = connectionState.toString().toUpperCase();
        String scannerNameExtScanner = scannerInfo.getFriendlyName();
        if (deviceList.size() != 0) {
            scannerName = deviceList.get(1).getFriendlyName();
        }
        if (scannerName.equalsIgnoreCase(scannerNameExtScanner)) {
            switch (connectionState) {
                case CONNECTED:
                    synchronized (lock) {
                        initScanner();
                        setDecoders();
                        bExtScannerDisconnected = false;
                    }
                    break;
                case DISCONNECTED:
                    bExtScannerDisconnected = true;
                    synchronized (lock) {
                        deInitScanner();
                    }
                    break;
            }
        } else {
            bExtScannerDisconnected = false;
        }
    }

    @Override
    public void onData(ScanDataCollection scanDataCollection) {
        if ((scanDataCollection != null) && (scanDataCollection.getResult() == ScannerResults.SUCCESS)) {
            ArrayList<ScanDataCollection.ScanData> scanData = scanDataCollection.getScanData();
            for (ScanDataCollection.ScanData data : scanData) {
                updateData(data.getData());
            }
        }
    }

    @Override
    public void onStatus(StatusData statusData) {
        StatusData.ScannerStates state = statusData.getState();
        switch (state) {
            case IDLE:
//                statusString = statusData.getFriendlyName() + " is enabled and idle...";
//                updateStatus(statusString);
                if (bContinuousMode) {
                    try {
                        scanner.triggerType = Scanner.TriggerType.SOFT_ALWAYS;
                        // An attempt to use the scanner continuously and rapidly (with a delay < 100 ms between scans)
                        // may cause the scanner to pause momentarily before resuming the scanning.
                        // Hence add some delay (>= 100ms) before submitting the next read.
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        scanner.read();
                    } catch (ScannerException e) {
                        updateStatus(e.getMessage());
                    }
                }
                // set trigger type
                if (bSoftTriggerSelected) {
                    scanner.triggerType = Scanner.TriggerType.SOFT_ONCE;
                    bSoftTriggerSelected = false;
                } else {
                    scanner.triggerType = Scanner.TriggerType.HARD;
                }
                // set decoders
                if (bDecoderSettingsChanged) {
                    setDecoders();
                    bDecoderSettingsChanged = false;
                } else {
                    setDecoders();
                }
                // submit read
                if (!scanner.isReadPending() && !bExtScannerDisconnected) {
                    try {
                        scanner.read();
                    } catch (ScannerException e) {
                    }
                }
                break;
            case WAITING:
                break;
            case SCANNING:
                break;
            case DISABLED:
                break;
            case ERROR:
                break;
            default:
                break;
        }
    }

    private void initScanner() {
        if (scanner == null) {
            if ((deviceList != null) && (deviceList.size() != 0)) {
                if (barcodeManager != null)
                    scanner = barcodeManager.getDevice(deviceList.get(1));
            } else {
//                updateStatus("Failed to get the specified scanner device! Please close and restart the application.");
                return;
            }
            if (scanner != null) {
                scanner.addDataListener(this);
                scanner.addStatusListener(this);
                try {
                    scanner.enable();
                } catch (ScannerException e) {
                    updateStatus(e.getMessage());
                    deInitScanner();
                }
            } else {
//                updateStatus("Failed to initialize the scanner device.");
            }
        }
    }

    private void deInitScanner() {
        if (scanner != null) {
            try {
                scanner.disable();
            } catch (Exception e) {
//                updateStatus(e.getMessage());
            }

            try {
                scanner.removeDataListener(this);
                scanner.removeStatusListener(this);
            } catch (Exception e) {
//                updateStatus(e.getMessage());
            }

            try {
                scanner.release();
            } catch (Exception e) {
//                updateStatus(e.getMessage());
            }
            scanner = null;
        }
    }

    private void initBarcodeManager() {
        barcodeManager = (BarcodeManager) emdkManager.getInstance(EMDKManager.FEATURE_TYPE.BARCODE);
        // Add connection listener
        if (barcodeManager != null) {
            barcodeManager.addConnectionListener(this);
        }
    }

    private void deInitBarcodeManager() {
        if (emdkManager != null) {
            emdkManager.release(EMDKManager.FEATURE_TYPE.BARCODE);
        }
    }


    private void enumerateScannerDevices() {
        if (barcodeManager != null) {
            deviceList = barcodeManager.getSupportedDevicesInfo();
            if ((deviceList != null) && (deviceList.size() != 0)) {

            } else {
            }
        }
    }

    private void setDecoders() {
        if (scanner != null) {
            try {
                ScannerConfig config = scanner.getConfig();
                // Set EAN8
                config.decoderParams.ean8.enabled = true;
                // Set EAN13
                config.decoderParams.ean13.enabled = true;
                // Set Code39
                config.decoderParams.code39.enabled = true;
                //Set Code128
                config.decoderParams.code128.enabled = true;
                //set inverser1Mode
                config.readerParams.readerSpecific.laserSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                config.readerParams.readerSpecific.cameraSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                config.readerParams.readerSpecific.imagerSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                scanner.setConfig(config);
            } catch (ScannerException e) {
                updateStatus(e.getMessage());
            }
        }
    }

    private void setReaderParams() {
        if (scanner != null) {
            try {
                ScannerConfig config = scanner.getConfig();
                config.readerParams.readerSpecific.laserSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                scanner.setConfig(config);
            } catch (ScannerException e) {
                updateStatus(e.getMessage());
            }
        }
    }

    private void stopScan() {

        if (scanner != null) {
            // Cancel the pending read.
            try {
                scanner.cancelRead();
            } catch (ScannerException e) {
                e.printStackTrace();
            }
        }
    }

    private void cancelRead() {
        if (scanner != null) {
            if (scanner.isReadPending()) {
                try {
                    scanner.cancelRead();
                } catch (ScannerException e) {
                }
            }
        }
    }

    private void updateStatus(final String status) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
            }
        });
    }

    private void updateData(final String result) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (result != null) {
                    String goodsID = "";
                    int index1 = result.indexOf("-");
                    if (index1 > 0) {
                        goodsID = result.substring(0, index1);
                    } else {
                        goodsID = result;
                    }
                    edtGoodsID.setText(goodsID);
                    edtQuantity.requestFocus();
                }
            }
        });
    }

    private ArrayList<CountGoods> fetchCountGoodsByLocationID(String locationID) {
        Call<List<CountGoods>> callback = iWarehouseApi.GetCountGoodsByID(locationID);
        callback.enqueue(new Callback<List<CountGoods>>() {
            @Override
            public void onResponse(Call<List<CountGoods>> call, Response<List<CountGoods>> response) {
                if (response.isSuccessful()) {
                    countGoodsArrayList = (ArrayList<CountGoods>) response.body();
                    setAdapter(countGoodsArrayList);
                    edtTotalGoodsID.setText(totalGoodsByLocationID(countGoodsArrayList) + "");
                    edtTotalQuantity.setText(totalQuantityByLocationID(countGoodsArrayList) + "");
                }
            }

            @Override
            public void onFailure(Call<List<CountGoods>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetch  wrdatadetail fail", Toast.LENGTH_SHORT).show();
            }
        });
        return countGoodsArrayList;
    }

    private ArrayList<CountGoods> fetchCountGoodsByMultiID(String locationID, String goodsID) {
        Call<List<CountGoods>> callback = iWarehouseApi.GetCountGoodsByMultiID(locationID, goodsID);
        callback.enqueue(new Callback<List<CountGoods>>() {
            @Override
            public void onResponse(Call<List<CountGoods>> call, Response<List<CountGoods>> response) {
                if (response.isSuccessful()) {
                    countGoodsArrayList = (ArrayList<CountGoods>) response.body();
                    setAdapter(countGoodsArrayList);
                }
            }

            @Override
            public void onFailure(Call<List<CountGoods>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetch  wrdatadetail fail", Toast.LENGTH_SHORT).show();
            }
        });
        return countGoodsArrayList;
    }

    private void putCountGoodsToServer(String locationID, String goodsID, CountGoods countGoods) {
        Call<CountGoods> callback = iWarehouseApi.updateCountGoods(locationID, goodsID, countGoods);
        callback.enqueue(new Callback<CountGoods>() {
            @Override
            public void onResponse(Call<CountGoods> call, Response<CountGoods> response) {
                if (response.isSuccessful()) {
                    hideSoftKeyBoard();
                    fetchCountGoodsByLocationID(edtLocationID.getText().toString().toUpperCase());
                } else {
                    Toast.makeText(getApplicationContext(), "Fail", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CountGoods> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Failture", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void postCountGoodsToServer(CountGoods countGoods) {
        Call<CountGoods> callback = iWarehouseApi.creatCountGoods(countGoods);
        callback.enqueue(new Callback<CountGoods>() {
            @Override
            public void onResponse(Call<CountGoods> call, Response<CountGoods> response) {
                if (response.isSuccessful()) {

                } else {
//                    Toast.makeText(getApplicationContext(), "Fail", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CountGoods> call, Throwable t) {
//                Toast.makeText(getApplicationContext(), "Failture ", Toast.LENGTH_SHORT).show();
            }
        });
        hideSoftKeyBoard();
        fetchCountGoodsByLocationID(edtLocationID.getText().toString().toUpperCase());
    }

    private void DeleteByLocationID(String locationID) {
        Call<Boolean> callback = iWarehouseApi.DeleteByLocationID(locationID);
        callback.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                if (response.isSuccessful()) {
                    alertSuccess(getApplicationContext(), "Xóa thành công data của "+locationID);
                    fetchCountGoodsByLocationID(locationID);
                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Failure", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void DeleteByMultiID(String locationID,String goodsID) {
        Call<Boolean> callback = iWarehouseApi.DeleteByMultiID(locationID,goodsID);
        callback.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                if (response.isSuccessful()) {
                    alertSuccess(getApplicationContext(), "Xóa thành công data của mã part number "+goodsID+" thuộc location "+locationID);
                    fetchCountGoodsByLocationID(locationID);
                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Failure", Toast.LENGTH_SHORT).show();
            }
        });
    }
}