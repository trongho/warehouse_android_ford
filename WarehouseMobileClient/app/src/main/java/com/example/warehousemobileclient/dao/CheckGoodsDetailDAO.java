package com.example.warehousemobileclient.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.warehousemobileclient.helper.DBHelper;
import com.example.warehousemobileclient.model.CheckGoodsDetail;
import com.example.warehousemobileclient.model.CheckGoodsHeader;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class CheckGoodsDetailDAO {
    DBHelper dbHelper;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public CheckGoodsDetailDAO(Context context) {
        dbHelper = new DBHelper(context);
    }

    public ArrayList<CheckGoodsDetail> getAll() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        ArrayList<CheckGoodsDetail> list = new ArrayList<>();
        String sql = "SELECT * FROM CHECKGOODSDETAIL";
        Cursor c = db.rawQuery(sql, null);
        c.moveToFirst();
        while (!c.isAfterLast()) {
            try {
            String id = c.getString(0);
            Date checkDate = simpleDateFormat.parse(c.getString(1));
            String goodsid = c.getString(2);
            list.add(new CheckGoodsDetail(id,checkDate, goodsid));
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }
            c.moveToNext();
        }
        return list;
    }

    public ArrayList<CheckGoodsDetail> getByID(String ID) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        ArrayList<CheckGoodsDetail> list = new ArrayList<>();
        String sql = "SELECT * FROM CHECKGOODSDETAIL WHERE ID=? ";
        Cursor c = db.rawQuery(sql, new String[]{ID});
        c.moveToFirst();
        while (!c.isAfterLast()) {
            try {
                String id = c.getString(0);
                Date checkDate = simpleDateFormat.parse(c.getString(1));
                String goodsid = c.getString(2);
                list.add(new CheckGoodsDetail(id,checkDate, goodsid));
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }
            c.moveToNext();
        }
        return list;
    }

    public ArrayList<CheckGoodsDetail> getByMonthDay(String dateString){
        int index1 = dateString.indexOf("-");
        int index2 = dateString.indexOf("-", index1 + 1);
        String year = dateString.substring(0, index1);
        String month = dateString.substring(index1 + 1, index2);
        String day = dateString.substring(index2 + 1,dateString.length());
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        ArrayList<CheckGoodsDetail> list = new ArrayList<>();
        String sql = "SELECT * FROM CHECKGOODSDETAIL WHERE STRFTIME('%Y-%m-%d',CHECKGOODSDETAIL.CHECKDATE)=?";
        Cursor c = db.rawQuery(sql, new String[]{year+"-"+month+"-"+day});
        c.moveToFirst();
        while (!c.isAfterLast()) {
            try {
                String id = c.getString(0);
                Date checkDate = simpleDateFormat.parse(c.getString(1));
                String goodsid = c.getString(2);
                list.add(new CheckGoodsDetail(id,checkDate, goodsid));
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }
            c.moveToNext();
        }
        return list;
    }

    public long insert(CheckGoodsDetail checkGoodsDetail) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("CHECKDATE", simpleDateFormat.format(Calendar.getInstance().getTime()));
        values.put("GOODSID",checkGoodsDetail.getGoodsID());
        return db.insert("CHECKGOODSDETAIL", null, values);
    }

    public int update(CheckGoodsDetail checkGoodsDetail) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("CHECKDATE", String.valueOf(checkGoodsDetail.getCheckDate()));
        values.put("GOODSID",checkGoodsDetail.getGoodsID());
        return db.update("CHECKGOODSDETAIL", values, "ID=?", new String[]{checkGoodsDetail.getId()});
    }

    public int deleteByID(String id) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        return db.delete("CHECKGOODSDETAIL","ID=?", new String[]{id});
    }

    public int deleteByDateAuto30() {
        Date curentDate=Calendar.getInstance().getTime();
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        return db.delete("CHECKGOODSDETAIL","CHECKGOODSDETAIL.CHECKDATE<= date('now','-30 day')", new String[]{});
    }

    public int deleteByDate(String deleteDate) {
        Date curentDate=Calendar.getInstance().getTime();
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        return db.delete("CHECKGOODSDETAIL","CHECKGOODSDETAIL.CHECKDATE=?", new String[]{deleteDate});
    }
}
