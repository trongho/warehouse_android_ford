package com.example.warehousemobileclient.ui.issue_order;

import android.content.Intent;
import android.opengl.Visibility;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.warehousemobileclient.R;
import com.example.warehousemobileclient.adapter.WIDataGeneralAdapter;
import com.example.warehousemobileclient.adapter.WRDataHeaderAdapter2;
import com.example.warehousemobileclient.model.WIDataGeneral;
import com.example.warehousemobileclient.model.WIDataHeader;
import com.example.warehousemobileclient.model.WRDataGeneral;
import com.example.warehousemobileclient.model.WRDataHeader;
import com.example.warehousemobileclient.service.IWarehouseApi;
import com.example.warehousemobileclient.service.RetrofitService;
import com.example.warehousemobileclient.ui.warehouse_receipt.ReceiptDataDetail;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class IssueOrderDetailFragment extends Fragment {
    private TextView tvWIDNumber = null;
    private TextView tvWIDReferenceNumber = null;
    private TextView tvWIDDate = null;
    private TextView tvWIDHandlingStatus = null;
    private TextView tvQuantityOrg = null;
    private TextView tvQuantity = null;
    private TextView tvWIDataGeneral = null;
    public static final String WIDNumber = "widnumber";
    IWarehouseApi iWarehouseApi = RetrofitService.getService();
    WIDataHeader wiDataHeader;
    ArrayList<WIDataGeneral> wiDataGenerals;
    private RecyclerView recyclerView=null;
    private WIDataGeneralAdapter adapter;
    String widNumber = "";

    public IssueOrderDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_issue_order_detail, container, false);
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);
        initComponent(view);

        wiDataGenerals=new ArrayList<>();
        wiDataHeader=new WIDataHeader();

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            widNumber = getArguments().getString(WIDNumber);
        }

        fetchWIDHeaderByWIDNumber(widNumber);
        fetchWIDataGeneral(widNumber);


        tvWIDataGeneral.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(recyclerView.getVisibility()==View.VISIBLE){
                    recyclerView.setVisibility(View.INVISIBLE);
                }else{
                    recyclerView.setVisibility(View.VISIBLE);
                }

            }
        });
    }

    private void initComponent(View view) {
        tvWIDNumber = view.findViewById(R.id.tvWIDNumer);
        tvWIDReferenceNumber = view.findViewById(R.id.tvReferenceNumber);
        tvWIDDate = view.findViewById(R.id.tvWIDDate);
        tvWIDHandlingStatus = view.findViewById(R.id.tvHandlingStatus);
        tvQuantityOrg = view.findViewById(R.id.tvTotalQuantityOrg);
        tvQuantity = view.findViewById(R.id.tvTotalQuantity);
        tvWIDataGeneral=view.findViewById(R.id.tvWIDataGeneral);
        recyclerView=view.findViewById(R.id.categories_recycler_view);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Navigation.findNavController(getActivity(), R.id.nav_host_fragment_activity_main).popBackStack();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private WIDataHeader fetchWIDHeaderByWIDNumber(String wIDNumer) {
        Call<List<WIDataHeader>> callback = iWarehouseApi.getWIDataHeader(wIDNumer);
        callback.enqueue(new Callback<List<WIDataHeader>>() {
            @Override
            public void onResponse(Call<List<WIDataHeader>> call, Response<List<WIDataHeader>> response) {
                if (response.isSuccessful()) {
                    wiDataHeader = response.body().get(0);
                    if(wiDataHeader!=null) {
                        tvWIDNumber.setText(wiDataHeader.getWidNumber());
                        tvWIDReferenceNumber.setText(wiDataHeader.getReferenceNumber());
                        tvWIDDate.setText(wiDataHeader.getWidDate());
                        tvWIDHandlingStatus.setText(wiDataHeader.getHandlingStatusName());
                        tvQuantityOrg.setText(wiDataHeader.getTotalQuantityOrg() + "");
                        tvQuantity.setText(wiDataHeader.getTotalQuantity() + "");
                    }
                    Toast.makeText(getContext(),"Fetch success!!!",Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(getContext(),"Not found!!!",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<WIDataHeader>> call, Throwable t) {
                Toast.makeText(getContext(),"fetch fail!!!",Toast.LENGTH_SHORT).show();
            }
        });
        return wiDataHeader;
    }

    private ArrayList<WIDataGeneral> fetchWIDataGeneral(String widNumber) {
        Call<List<WIDataGeneral>> callback = iWarehouseApi.getWIDataGeneralById(widNumber);
        callback.enqueue(new Callback<List<WIDataGeneral>>() {
            @Override
            public void onResponse(Call<List<WIDataGeneral>> call, Response<List<WIDataGeneral>> response) {
                if (response.isSuccessful()) {
                    wiDataGenerals = (ArrayList<WIDataGeneral>) response.body();
                    setAdapter(wiDataGenerals);
                }
            }

            @Override
            public void onFailure(Call<List<WIDataGeneral>> call, Throwable t) {
                Toast.makeText(getContext(), "Fetch fail", Toast.LENGTH_SHORT).show();
            }
        });
        return wiDataGenerals;
    }

    private void setAdapter(ArrayList<WIDataGeneral> wiDataGenerals){
        adapter=new WIDataGeneralAdapter(getContext(), wiDataGenerals, new WIDataGeneralAdapter.ItemClickListener() {
            @Override
            public void onClick(WIDataGeneral wiDataGeneral) {

            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }
}