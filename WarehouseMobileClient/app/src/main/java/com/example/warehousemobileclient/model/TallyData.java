package com.example.warehousemobileclient.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class TallyData {
    @SerializedName("tsNumber")
    @Expose
    private String tsNumber;
    @SerializedName("tsDate")
    @Expose
    private String tsDate;
    @SerializedName("goodsID")
    @Expose
    private String goodsID;
    @SerializedName("goodsName")
    @Expose
    private String goodsName;
    @SerializedName("quantity")
    @Expose
    private Double quantity;
    @SerializedName("totalQuantity")
    @Expose
    private Double totalQuantity;
    @SerializedName("totalRow")
    @Expose
    private Double totalRow;
    @SerializedName("creatorID")
    @Expose
    private String creatorID;
    @SerializedName("createdDateTime")
    @Expose
    private String createdDateTime;
    @SerializedName("editerID")
    @Expose
    private String editerID;
    @SerializedName("editedDateTime")
    @Expose
    private String editedDateTime;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("no")
    @Expose
    private Integer no;

    public TallyData() {
    }

    public TallyData(String tsNumber, String tsDate, String goodsID, String goodsName, Double quantity, Double totalQuantity, Double totalRow, String creatorID, String createdDateTime, String editerID, String editedDateTime, String status, Integer no) {
        this.tsNumber = tsNumber;
        this.tsDate = tsDate;
        this.goodsID = goodsID;
        this.goodsName = goodsName;
        this.quantity = quantity;
        this.totalQuantity = totalQuantity;
        this.totalRow = totalRow;
        this.creatorID = creatorID;
        this.createdDateTime = createdDateTime;
        this.editerID = editerID;
        this.editedDateTime = editedDateTime;
        this.status = status;
        this.no = no;
    }

    public String getTsNumber() {
        return tsNumber;
    }

    public void setTsNumber(String tsNumber) {
        this.tsNumber = tsNumber;
    }

    public String getTsDate() {
        return tsDate;
    }

    public void setTsDate(String tsDate) {
        this.tsDate = tsDate;
    }

    public String getGoodsID() {
        return goodsID;
    }

    public void setGoodsID(String goodsID) {
        this.goodsID = goodsID;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public Double getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(Double totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public Double getTotalRow() {
        return totalRow;
    }

    public void setTotalRow(Double totalRow) {
        this.totalRow = totalRow;
    }

    public String getCreatorID() {
        return creatorID;
    }

    public void setCreatorID(String creatorID) {
        this.creatorID = creatorID;
    }

    public String getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(String createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public String getEditerID() {
        return editerID;
    }

    public void setEditerID(String editerID) {
        this.editerID = editerID;
    }

    public String getEditedDateTime() {
        return editedDateTime;
    }

    public void setEditedDateTime(String editedDateTime) {
        this.editedDateTime = editedDateTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getNo() {
        return no;
    }

    public void setNo(Integer no) {
        this.no = no;
    }
}
