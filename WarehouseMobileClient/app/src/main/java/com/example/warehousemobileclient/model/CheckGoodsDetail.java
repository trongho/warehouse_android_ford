package com.example.warehousemobileclient.model;

import java.util.Date;

public class CheckGoodsDetail {
    private String id;
    private Date checkDate;
    private String goodsID;

    public CheckGoodsDetail() {
    }

    public CheckGoodsDetail(String id, Date checkDate, String goodsID) {
        this.id = id;
        this.checkDate = checkDate;
        this.goodsID = goodsID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getCheckDate() {
        return checkDate;
    }

    public void setCheckDate(Date checkDate) {
        this.checkDate = checkDate;
    }

    public String getGoodsID() {
        return goodsID;
    }

    public void setGoodsID(String goodsID) {
        this.goodsID = goodsID;
    }
}
