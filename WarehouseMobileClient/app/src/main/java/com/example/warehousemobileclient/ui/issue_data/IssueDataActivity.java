package com.example.warehousemobileclient.ui.issue_data;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.warehousemobileclient.R;
import com.example.warehousemobileclient.adapter.WIDataGeneralAdapter;
import com.example.warehousemobileclient.adapter.WRDataDetailAdapter2;
import com.example.warehousemobileclient.dao.IssueDataDAO;
import com.example.warehousemobileclient.dao.ReceiptDataDAO;
import com.example.warehousemobileclient.model.GoodsData;
import com.example.warehousemobileclient.model.GoodsDataIssue;
import com.example.warehousemobileclient.model.WIDataDetail;
import com.example.warehousemobileclient.model.WIDataGeneral;
import com.example.warehousemobileclient.model.WIDataHeader;
import com.example.warehousemobileclient.model.WRDataDetail;
import com.example.warehousemobileclient.model.WRDataGeneral;
import com.example.warehousemobileclient.model.WRDataHeader;
import com.example.warehousemobileclient.service.IWarehouseApi;
import com.example.warehousemobileclient.service.RetrofitService;
import com.example.warehousemobileclient.ui.receipt_data.ReceiptDataActivity;
import com.symbol.emdk.EMDKManager;
import com.symbol.emdk.EMDKResults;
import com.symbol.emdk.barcode.BarcodeManager;
import com.symbol.emdk.barcode.ScanDataCollection;
import com.symbol.emdk.barcode.Scanner;
import com.symbol.emdk.barcode.ScannerConfig;
import com.symbol.emdk.barcode.ScannerException;
import com.symbol.emdk.barcode.ScannerInfo;
import com.symbol.emdk.barcode.ScannerResults;
import com.symbol.emdk.barcode.StatusData;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Dictionary;
import java.util.HashSet;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class IssueDataActivity extends AppCompatActivity implements EMDKManager.EMDKListener, Scanner.DataListener, Scanner.StatusListener, BarcodeManager.ScannerConnectionListener {
    private EMDKManager emdkManager = null;
    private BarcodeManager barcodeManager = null;
    private Scanner scanner = null;
    private Spinner spinnerScannerDevices = null;
    private List<ScannerInfo> deviceList = null;
    private final Object lock = new Object();
    private boolean bSoftTriggerSelected = false;
    private boolean bDecoderSettingsChanged = false;
    private boolean bExtScannerDisconnected = false;
    private boolean bContinuousMode = false;

    private String numberRegex = "\\d+(?:\\.\\d+)?";

    private RecyclerView recyclerView = null;
    private EditText edtWIDNumber = null;
    private EditText edtGoodsID = null;
    private EditText edtDateTime = null;
    private EditText edtQuantity = null;
    private EditText edtTotalQuantityOrg = null;
    private EditText edtTotalQuantity = null;
    private EditText edtTotalGoodsOrg = null;
    private EditText edtTotalGoods = null;
    private Button btnClearWIDNumber = null;
    private Button btnClearGoodsID = null;
    private Button btnClearQuantity = null;
    private Button btnClearList = null;
    private Button btnExit = null;
    private Button btnSave = null;
    private WIDataGeneralAdapter adapter;
    ArrayList<WIDataGeneral> wiDataGenerals;
    ArrayList<WIDataGeneral> arrayListByGoodsID;
    ArrayList<WIDataHeader> wiDataHeaders;
    WIDataHeader wiDataHeader;
    IWarehouseApi iWarehouseApi = RetrofitService.getService();
    private static final String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%";

    Boolean isGoodsIDExist = false;
    Boolean isGoodsIDAndDateScanned = false;
    Boolean isGoodsIdDuplicate = false;
    Boolean isLastGoodsScaned = false;
    Double totalGoods = 0.0;
    Double totalQuantity = 0.0;
    Double totalGoodsOrg = 0.0;
    Double totalQuantityOrg = 0.0;

    ArrayList<GoodsDataIssue> goodsDataArrayList = null;
    GoodsDataIssue goodsDataIssue = null;
    String date = "";
    IssueDataDAO issueDataDAO;

    private Spinner spGoodsGroupID = null;
    private Spinner spPickerName = null;
    int GoodsGroupIDIndex = 0;
    int PickerNameIndex = 0;
    String selectedGoodsGroupID = "All";
    String selectedPickerName = "All";
    ArrayList<String> goodsGroupIDs = null;
    ArrayList<String> pickerNames = null;

    private EditText edtScanNumber = null;
    int scanNumber = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_issue_data);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Xuất hàng");
        getSupportActionBar().setDisplayShowTitleEnabled(true);


        EMDKResults results = EMDKManager.getEMDKManager(getApplicationContext(), this);
        if (results.statusCode != EMDKResults.STATUS_CODE.SUCCESS) {
            updateStatus("EMDKManager object request failed!");
            return;
        }
        initComponent();

        edtScanNumber.setText(scanNumber + "");

        edtWIDNumber.requestFocus();

        wiDataHeaders = new ArrayList<>();
        wiDataGenerals = new ArrayList<>();
        arrayListByGoodsID = new ArrayList<>();
        goodsDataIssue = new GoodsDataIssue();
        goodsDataArrayList = new ArrayList<GoodsDataIssue>();
        goodsGroupIDs = new ArrayList<String>();
        pickerNames = new ArrayList<String>();

        spGoodsGroupID.setEnabled(false);
        spPickerName.setEnabled(false);
        spGoodsGroupID.setSelection(GoodsGroupIDIndex);
        spPickerName.setSelection(PickerNameIndex);


        edtWIDNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (TextUtils.isEmpty(editable)) {
                    alertError(getApplicationContext(), "Nhập số phiếu");
                    edtWIDNumber.requestFocus();
                    return;
                }

                if (editable.length() >= 1) {
                    Call<List<WIDataHeader>> callback = iWarehouseApi.getWIDataHeader(edtWIDNumber.getText().toString().trim());
                    callback.enqueue(new Callback<List<WIDataHeader>>() {
                        @Override
                        public void onResponse(Call<List<WIDataHeader>> call, Response<List<WIDataHeader>> response) {
                            if (response.isSuccessful()) {

//                                setGoodsGroupIDSpinner();
//                                setPickerNameSpinner();
//                                addGoodsGroupIDListener();
//                                addPickerNameListener();

                                wiDataHeaders = (ArrayList<WIDataHeader>) response.body();
                                if (wiDataHeaders.size() > 0) {
                                    wiDataHeader = wiDataHeaders.get(0);
                                    alertSuccess(getApplicationContext(), "Lấy dữ liệu thành công!!!");
                                    fetchWIDataGeneralDataByScanOption(edtWIDNumber.getText().toString(), "1");

                                    setGoodsGroupIDSpinner();
                                    setPickerNameSpinner();
                                    addGoodsGroupIDListener();
                                    addPickerNameListener();
                                    hideSoftKeyBoard();
                                    edtGoodsID.requestFocus();
                                }

                            }
                        }

                        @Override
                        public void onFailure(Call<List<WIDataHeader>> call, Throwable t) {
                            Toast.makeText(getApplicationContext(), "Fetch widataheader fail", Toast.LENGTH_SHORT).show();
                        }
                    });
//                    fetchWRDataDetailData(edtWRDNumber.getText().toString());
                }
            }
        });

        edtGoodsID.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (TextUtils.isEmpty(edtWIDNumber.getText().toString())) {
                    alertError(getApplicationContext(), "Nhập Conveyance number");
                    edtWIDNumber.requestFocus();
                    return;
                }
            }
        });

        edtQuantity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (TextUtils.isEmpty(edtWIDNumber.getText().toString())) {
                    alertError(getApplicationContext(), "Nhập Conveyance number");
                    edtWIDNumber.requestFocus();
                    return;
                }

            }
        });

        btnClearWIDNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtWIDNumber.setText("");
//                edtReferenceNumber.setText("");
            }
        });
        btnClearGoodsID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtGoodsID.setText("");
            }
        });
        btnClearQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtQuantity.setText("");
            }
        });
        btnClearList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (wiDataGenerals.size() > 0) {
                    clearlist();
                }
            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                process();
            }
        });
        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void initComponent() {
        recyclerView = findViewById(R.id.recycler_view);
        edtWIDNumber = findViewById(R.id.edtWIDNumber);
        edtGoodsID = findViewById(R.id.edtGoodsID);
        edtDateTime = findViewById(R.id.edtDate);
        edtQuantity = findViewById(R.id.edtQuantity);
        btnClearWIDNumber = findViewById(R.id.btn_clear_widnumber);
        btnClearGoodsID = findViewById(R.id.btn_clear_goodsid);
        btnClearQuantity = findViewById(R.id.btn_clear_quantity);
        edtTotalQuantityOrg = findViewById(R.id.edtTotalQuantityOrg);
        edtTotalQuantity = findViewById(R.id.edtTotalQuantity);
        edtTotalGoodsOrg = findViewById(R.id.edtTotalGoodsOrg);
        edtTotalGoods = findViewById(R.id.edtTotalGoods);
        issueDataDAO = new IssueDataDAO(IssueDataActivity.this);
        btnClearList = findViewById(R.id.btn_clear_data);
        btnSave = findViewById(R.id.btn_save_data);
        btnExit = findViewById(R.id.btn_exit);
        spGoodsGroupID = findViewById(R.id.spinnerGoodsGroupID);
        spPickerName = findViewById(R.id.spinnerPickerName);
        edtScanNumber = findViewById(R.id.edtScanNumber);
    }

    private void setGoodsGroupIDSpinner() {
        Call<List<String>> callback = iWarehouseApi.getGoodsGroupIDs(edtWIDNumber.getText().toString().trim());
        callback.enqueue(new Callback<List<String>>() {
            @Override
            public void onResponse(Call<List<String>> call, Response<List<String>> response) {
                if (response.isSuccessful()) {
                    goodsGroupIDs = (ArrayList<String>) response.body();
                    if (goodsGroupIDs.size() > 1) {
                        spGoodsGroupID.setEnabled(true);
                    }
                    ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(IssueDataActivity.this
                            , android.R.layout.simple_spinner_item, goodsGroupIDs);
                    spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spGoodsGroupID.setAdapter(spinnerAdapter);
                }
            }

            @Override
            public void onFailure(Call<List<String>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetch goodsGroupID fail" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
//
//        Toast.makeText(getApplicationContext(),wiDataGenerals.size()+"",Toast.LENGTH_SHORT).show();
//        for (int i = 0; i < wiDataGenerals.size(); i++) {
//            String goodsGroupID = wiDataGenerals.get(i).getGoodsGroupID();
//            goodsGroupIDs.add(goodsGroupID);
//        }
//        HashSet<String> hashSet = new HashSet<String>();
//        hashSet.addAll(goodsGroupIDs);
//        goodsGroupIDs.clear();
//        goodsGroupIDs.add(0,"All");
//        goodsGroupIDs.addAll(hashSet);
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(IssueDataActivity.this
                , android.R.layout.simple_spinner_item, goodsGroupIDs);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spGoodsGroupID.setAdapter(spinnerAdapter);
        spGoodsGroupID.setEnabled(true);
    }

    private void addGoodsGroupIDListener() {
        spGoodsGroupID.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                selectedGoodsGroupID = spGoodsGroupID.getSelectedItem().toString();
                if ((GoodsGroupIDIndex != position)) {
                    GoodsGroupIDIndex = position;
                }
                if (GoodsGroupIDIndex == 0) {
                    if (selectedPickerName.equalsIgnoreCase("All")) {
                        wiDataGenerals = fetchWIDataGeneralDataByScanOption(edtWIDNumber.getText().toString(), "1");
                    } else {
                        wiDataGenerals = fetchWIDataGeneralDataByPickerNameAndScanOption(edtWIDNumber.getText().toString(), selectedPickerName, "1");
                    }
                } else {
                    if (selectedPickerName.equalsIgnoreCase("All")) {
                        wiDataGenerals = fetchWIDataGeneralDataByGoodsGroupIDAndScanOption(edtWIDNumber.getText().toString(), spGoodsGroupID.getSelectedItem().toString(), "1");
                    } else {
                        wiDataGenerals = fetchWIDataGeneralDataByGoodsGroupIDPickerNameScanOption(edtWIDNumber.getText().toString(),
                                spGoodsGroupID.getSelectedItem().toString(), selectedPickerName, "1");
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void setPickerNameSpinner() {
        Call<List<String>> callback = iWarehouseApi.getPickerNames(edtWIDNumber.getText().toString().trim());
        callback.enqueue(new Callback<List<String>>() {
            @Override
            public void onResponse(Call<List<String>> call, Response<List<String>> response) {
                if (response.isSuccessful()) {
                    pickerNames = (ArrayList<String>) response.body();
                    if (pickerNames.size() > 1) {
                        spPickerName.setEnabled(true);
                    }
                    ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(IssueDataActivity.this
                            , android.R.layout.simple_spinner_item, pickerNames);
                    spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spPickerName.setAdapter(spinnerAdapter);
                }
            }

            @Override
            public void onFailure(Call<List<String>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetch pickerNames fail" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
//        for (int i = 0; i < wiDataGenerals1.size(); i++) {
//            String pickerName = wiDataGenerals1.get(i).getPickerName();
//            pickerNames.add(pickerName);
//        }
//        HashSet<String> hashSet = new HashSet<String>();
//        hashSet.addAll(pickerNames);
//        pickerNames.clear();
//        pickerNames.add(0,"All");
//        pickerNames.addAll(hashSet);
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(IssueDataActivity.this
                , android.R.layout.simple_spinner_item, pickerNames);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spPickerName.setAdapter(spinnerAdapter);
    }

    private void addPickerNameListener() {
        spPickerName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                selectedPickerName = spPickerName.getSelectedItem().toString();
                if ((PickerNameIndex != position)) {
                    PickerNameIndex = position;
                }
                if (PickerNameIndex == 0) {
                    if (selectedGoodsGroupID.equalsIgnoreCase("All")) {
                        wiDataGenerals = fetchWIDataGeneralDataByScanOption(edtWIDNumber.getText().toString(), "1");
                    } else {
                        wiDataGenerals = fetchWIDataGeneralDataByGoodsGroupIDAndScanOption(edtWIDNumber.getText().toString(), selectedGoodsGroupID, "1");
                    }
                } else {
                    if (selectedGoodsGroupID.equalsIgnoreCase("All")) {
                        wiDataGenerals = fetchWIDataGeneralDataByPickerNameAndScanOption(edtWIDNumber.getText().toString(), spPickerName.getSelectedItem().toString(), "1");
                    } else {
                        wiDataGenerals = fetchWIDataGeneralDataByGoodsGroupIDPickerNameScanOption(edtWIDNumber.getText().toString(),
                                selectedGoodsGroupID, spPickerName.getSelectedItem().toString(), "1");
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }


    private WIDataHeader fetchWIDataHeaderData(String widNumber) {
        Call<List<WIDataHeader>> callback = iWarehouseApi.getWIDataHeader(widNumber);
        callback.enqueue(new Callback<List<WIDataHeader>>() {
            @Override
            public void onResponse(Call<List<WIDataHeader>> call, Response<List<WIDataHeader>> response) {
                if (response.isSuccessful()) {
                    wiDataHeaders = (ArrayList<WIDataHeader>) response.body();
                    if (wiDataHeaders.size() > 0) {
                        wiDataHeader = wiDataHeaders.get(0);
                        hideSoftKeyBoard();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<WIDataHeader>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetch wIdataheader fail", Toast.LENGTH_SHORT).show();
            }
        });
        return wiDataHeader;
    }

    private ArrayList<WIDataGeneral> fetchWIDataGeneralData(String widNumber) {
        Call<List<WIDataGeneral>> callback = iWarehouseApi.getWIDataGeneralById(widNumber);
        callback.enqueue(new Callback<List<WIDataGeneral>>() {
            @Override
            public void onResponse(Call<List<WIDataGeneral>> call, Response<List<WIDataGeneral>> response) {
                if (response.isSuccessful()) {
                    wiDataGenerals = (ArrayList<WIDataGeneral>) response.body();
                    if (wiDataGenerals.size() > 0) {
                        edtTotalGoodsOrg.setText(String.format("%.0f", wiDataGenerals.get(0).getTotalGoodsOrg()));
                        totalGoods = 0.0;
                        getTotalGoods(wiDataGenerals);
                        edtTotalGoods.setText(String.format("%.0f", totalGoods));
                    }
                    setAdapter(wiDataGenerals);
                }
            }

            @Override
            public void onFailure(Call<List<WIDataGeneral>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetch  wrdatadetail fail", Toast.LENGTH_SHORT).show();
            }
        });
        return wiDataGenerals;
    }

    private ArrayList<WIDataGeneral> fetchWIDataGeneralDataByGoodsGroupID(String widNumber, String goodsGroupID) {
        Call<List<WIDataGeneral>> callback = iWarehouseApi.GetByGoodsGroupID(widNumber, goodsGroupID);
        callback.enqueue(new Callback<List<WIDataGeneral>>() {
            @Override
            public void onResponse(Call<List<WIDataGeneral>> call, Response<List<WIDataGeneral>> response) {
                if (response.isSuccessful()) {
                    wiDataGenerals = (ArrayList<WIDataGeneral>) response.body();
                }
            }

            @Override
            public void onFailure(Call<List<WIDataGeneral>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetch fail", Toast.LENGTH_SHORT).show();
            }
        });
        return wiDataGenerals;
    }

    private ArrayList<WIDataGeneral> fetchWIDataGeneralDataByPickerName(String widNumber, String pickerName) {
        Call<List<WIDataGeneral>> callback = iWarehouseApi.GetByPickerName(widNumber, pickerName);
        callback.enqueue(new Callback<List<WIDataGeneral>>() {
            @Override
            public void onResponse(Call<List<WIDataGeneral>> call, Response<List<WIDataGeneral>> response) {
                if (response.isSuccessful()) {
                    wiDataGenerals = (ArrayList<WIDataGeneral>) response.body();
                }
            }

            @Override
            public void onFailure(Call<List<WIDataGeneral>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetch fail", Toast.LENGTH_SHORT).show();
            }
        });
        return wiDataGenerals;
    }

    private ArrayList<WIDataGeneral> fetchWIDataGeneralDataByScanOption(String widNumber, String scanOption) {
        Call<List<WIDataGeneral>> callback = iWarehouseApi.GetByScanOption(widNumber, scanOption);
        callback.enqueue(new Callback<List<WIDataGeneral>>() {
            @Override
            public void onResponse(Call<List<WIDataGeneral>> call, Response<List<WIDataGeneral>> response) {
                if (response.isSuccessful()) {
                    wiDataGenerals = (ArrayList<WIDataGeneral>) response.body();
                    if (wiDataGenerals.size() > 0) {

                        totalQuantityOrg = 0.0;
                        totalQuantity = totalQuantityOrg;
                        totalGoodsOrg = 0.0;
                        getTotalQuantityOrg(wiDataGenerals);
                        getTotalQuantity(wiDataGenerals);
                        getTotalGoodsOrg(wiDataGenerals);
                        totalGoods = totalGoodsOrg;
                        getTotalGoods(wiDataGenerals);
                        edtTotalGoodsOrg.setText(String.format("%.0f", totalGoodsOrg));
                        edtTotalGoods.setText(String.format("%.0f", totalGoods));
                        edtTotalQuantityOrg.setText(String.format("%.0f", totalQuantityOrg));
                        edtTotalQuantity.setText(String.format("%.0f", totalQuantity));
                        getScanNumber(edtTotalQuantity);
                    }
                    setAdapter(wiDataGenerals);
                }
            }

            @Override
            public void onFailure(Call<List<WIDataGeneral>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetch fail", Toast.LENGTH_SHORT).show();
            }
        });
        return wiDataGenerals;
    }

    private ArrayList<WIDataGeneral> fetchWIDataGeneralDataByGoodsGroupIDAndPickerName(String widNumber, String goodsGroupID, String pickerName) {
        Call<List<WIDataGeneral>> callback = iWarehouseApi.FilterByGoodsGroupIDAndpickerName(widNumber, goodsGroupID, pickerName);
        callback.enqueue(new Callback<List<WIDataGeneral>>() {
            @Override
            public void onResponse(Call<List<WIDataGeneral>> call, Response<List<WIDataGeneral>> response) {
                if (response.isSuccessful()) {
                    wiDataGenerals = (ArrayList<WIDataGeneral>) response.body();
                }
            }

            @Override
            public void onFailure(Call<List<WIDataGeneral>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetch fail", Toast.LENGTH_SHORT).show();
            }
        });
        return wiDataGenerals;
    }

    private ArrayList<WIDataGeneral> fetchWIDataGeneralDataByGoodsGroupIDAndScanOption(String widNumber, String goodsGroupID, String scanOption) {
        Call<List<WIDataGeneral>> callback = iWarehouseApi.FilterByGoodsGroupIDAndScanOption(widNumber, goodsGroupID, scanOption);
        callback.enqueue(new Callback<List<WIDataGeneral>>() {
            @Override
            public void onResponse(Call<List<WIDataGeneral>> call, Response<List<WIDataGeneral>> response) {
                if (response.isSuccessful()) {
                    wiDataGenerals = (ArrayList<WIDataGeneral>) response.body();
                    if (wiDataGenerals.size() > 0) {
                        totalQuantityOrg = 0.0;
                        totalQuantity = totalQuantityOrg;
                        totalGoodsOrg = 0.0;
                        getTotalQuantityOrg(wiDataGenerals);
                        getTotalQuantity(wiDataGenerals);
                        getTotalGoodsOrg(wiDataGenerals);
                        totalGoods = totalGoodsOrg;
                        getTotalGoods(wiDataGenerals);
                        edtTotalGoodsOrg.setText(String.format("%.0f", totalGoodsOrg));
                        edtTotalGoods.setText(String.format("%.0f", totalGoods));
                        edtTotalQuantityOrg.setText(String.format("%.0f", totalQuantityOrg));
                        edtTotalQuantity.setText(String.format("%.0f", totalQuantity));
                        getScanNumber(edtTotalQuantity);
                    }
                    setAdapter(wiDataGenerals);
                }
            }

            @Override
            public void onFailure(Call<List<WIDataGeneral>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetch fail", Toast.LENGTH_SHORT).show();
            }
        });
        return wiDataGenerals;
    }

    private ArrayList<WIDataGeneral> fetchWIDataGeneralDataByPickerNameAndScanOption(String widNumber, String pickerName, String scanOption) {
        Call<List<WIDataGeneral>> callback = iWarehouseApi.FilterByPickerNameAndScanOption(widNumber, pickerName, scanOption);
        callback.enqueue(new Callback<List<WIDataGeneral>>() {
            @Override
            public void onResponse(Call<List<WIDataGeneral>> call, Response<List<WIDataGeneral>> response) {
                if (response.isSuccessful()) {
                    wiDataGenerals = (ArrayList<WIDataGeneral>) response.body();
                    if (wiDataGenerals.size() > 0) {
                        totalQuantityOrg = 0.0;
                        totalQuantity = totalQuantityOrg;
                        totalGoodsOrg = 0.0;
                        getTotalQuantityOrg(wiDataGenerals);
                        getTotalQuantity(wiDataGenerals);
                        getTotalGoodsOrg(wiDataGenerals);
                        totalGoods = 0.0;
                        totalGoods = totalGoodsOrg;
                        getTotalGoods(wiDataGenerals);
                        edtTotalGoodsOrg.setText(String.format("%.0f", totalGoodsOrg));
                        edtTotalGoods.setText(String.format("%.0f", totalGoods));
                        edtTotalQuantityOrg.setText(String.format("%.0f", totalQuantityOrg));
                        edtTotalQuantity.setText(String.format("%.0f", totalQuantity));
                    }
                    setAdapter(wiDataGenerals);
                }
            }

            @Override
            public void onFailure(Call<List<WIDataGeneral>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetch fail", Toast.LENGTH_SHORT).show();
            }
        });
        return wiDataGenerals;
    }

    private ArrayList<WIDataGeneral> fetchWIDataGeneralDataByGoodsGroupIDPickerNameScanOption(String widNumber, String goodsGroupID, String pickerName, String scanOption) {
        Call<List<WIDataGeneral>> callback = iWarehouseApi.Filter(widNumber, goodsGroupID, pickerName, scanOption);
        callback.enqueue(new Callback<List<WIDataGeneral>>() {
            @Override
            public void onResponse(Call<List<WIDataGeneral>> call, Response<List<WIDataGeneral>> response) {
                if (response.isSuccessful()) {
                    wiDataGenerals = (ArrayList<WIDataGeneral>) response.body();
                    if (wiDataGenerals.size() > 0) {
                        totalQuantityOrg = 0.0;
                        totalQuantity = totalQuantityOrg;
                        totalGoodsOrg = 0.0;
                        getTotalQuantityOrg(wiDataGenerals);
                        getTotalQuantity(wiDataGenerals);
                        getTotalGoodsOrg(wiDataGenerals);
                        totalGoods = 0.0;
                        totalGoods = totalGoodsOrg;
                        getTotalGoods(wiDataGenerals);
                        edtTotalGoodsOrg.setText(String.format("%.0f", totalGoodsOrg));
                        edtTotalGoods.setText(String.format("%.0f", totalGoods));
                        edtTotalQuantityOrg.setText(String.format("%.0f", totalQuantityOrg));
                        edtTotalQuantity.setText(String.format("%.0f", totalQuantity));
                    }
                    setAdapter(wiDataGenerals);
                }
            }

            @Override
            public void onFailure(Call<List<WIDataGeneral>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetch fail", Toast.LENGTH_SHORT).show();
            }
        });
        return wiDataGenerals;
    }

    private void putWIDataHeader(String wIDNumber, WIDataHeader wiDataHeader) {
        Call<WIDataHeader> callback = iWarehouseApi.updateWIDataHeader(wIDNumber, wiDataHeader);
        callback.enqueue(new Callback<WIDataHeader>() {
            @Override
            public void onResponse(Call<WIDataHeader> call, Response<WIDataHeader> response) {
                if (response.isSuccessful()) {
                    fetchWIDataHeaderData(wIDNumber);
                }
            }

            @Override
            public void onFailure(Call<WIDataHeader> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "put wIdataheader fail" + t.getMessage() + "", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void putWIDataGeneral(String wIDNumber, String goodsID, int ordinal, String goodsGroupID, WIDataGeneral wiDataGeneral) {
        Call<WIDataGeneral> callback = iWarehouseApi.updateWIDataGeneral(wIDNumber, goodsID, ordinal, goodsGroupID, wiDataGeneral);
        callback.enqueue(new Callback<WIDataGeneral>() {
            @Override
            public void onResponse(Call<WIDataGeneral> call, Response<WIDataGeneral> response) {
                if (response.isSuccessful()) {


                    if (selectedGoodsGroupID.equalsIgnoreCase("All") && selectedPickerName.equalsIgnoreCase("ALl")) {
                        fetchWIDataGeneralDataByScanOption(edtWIDNumber.getText().toString(), "1");
                    } else if (!selectedGoodsGroupID.equalsIgnoreCase("All") && selectedPickerName.equalsIgnoreCase("ALl")) {
                        fetchWIDataGeneralDataByGoodsGroupIDAndScanOption(edtWIDNumber.getText().toString(), selectedGoodsGroupID, "1");
                    } else if (selectedGoodsGroupID.equalsIgnoreCase("All") && !selectedPickerName.equalsIgnoreCase("ALl")) {
                        fetchWIDataGeneralDataByPickerNameAndScanOption(edtWIDNumber.getText().toString(), selectedPickerName, "1");
                    } else {
                        fetchWIDataGeneralDataByGoodsGroupIDPickerNameScanOption(edtWIDNumber.getText().toString(), selectedGoodsGroupID, selectedPickerName, "1");
                    }


//                    getTotalQuantity(wiDataGenerals);
//                    getTotalGoods(wiDataGenerals);
//                    edtTotalQuantity.setText(String.format("%.0f", totalQuantity));
//                    edtTotalGoods.setText(String.format("%.0f", totalGoods));
                } else {
                    alertError(getApplicationContext(), "Cập nhật dữ liệu nhập thất bại");
                }

            }

            @Override
            public void onFailure(Call<WIDataGeneral> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "put wrdatadetail fail" + t.getMessage() + "", Toast.LENGTH_SHORT).show();
            }
        });
    }

//    private void getTotalQuantity(ArrayList<WIDataGeneral> wiDataGenerals) {
//        for (int i = 0; i < wiDataGenerals.size(); i++) {
//            if (wiDataGenerals.size() > 0 && wiDataGenerals.get(i).getQuantity() > 0) {
//                totalQuantity += wiDataGenerals.get(i).getQuantity();
//            }
//        }
//    }
//
//    private void getTotalGoods(ArrayList<WIDataGeneral> wiDataGenerals) {
//        for (int i = 0; i < wiDataGenerals.size(); i++) {
//            if (wiDataGenerals.get(i).getQuantity().doubleValue() > 0) {
//                totalGoods += 1;
//            }
//        }
//    }

    private void getTotalQuantityOrg(ArrayList<WIDataGeneral> wiDataGenerals) {
        for (int i = 0; i < wiDataGenerals.size(); i++) {
            totalQuantityOrg += wiDataGenerals.get(i).getQuantityOrg();
        }
    }

    private void getTotalGoodsOrg(ArrayList<WIDataGeneral> wiDataGenerals) {
        for (int i = 0; i < wiDataGenerals.size(); i++) {
            totalGoodsOrg = Double.valueOf(wiDataGenerals.size());
        }
    }

    private void getTotalQuantity(ArrayList<WIDataGeneral> wiDataGenerals) {
        for (int i = 0; i < wiDataGenerals.size(); i++) {
            if (wiDataGenerals.size() > 0) {
                totalQuantity += wiDataGenerals.get(i).getQuantityOrg() - wiDataGenerals.get(i).getQuantity();
            }
        }
    }

    private void getTotalGoods(ArrayList<WIDataGeneral> wiDataGenerals) {
        for (int i = 0; i < wiDataGenerals.size(); i++) {
            if (wiDataGenerals.get(i).getQuantity().doubleValue() > 0) {
                totalGoods -= 1;
            }
        }
    }

    private Boolean checkGoodsIDDuplicate(ArrayList<WIDataGeneral> wiDataGenerals, String goodsID) {
        for (int i = 0; i < wiDataGenerals.size(); i++) {
            if (wiDataGenerals.get(i).getQuantity() > 0 && wiDataGenerals.get(i).getGoodsID().equalsIgnoreCase(goodsID)) {
                isGoodsIdDuplicate = true;
                break;
            }
        }
        return isGoodsIdDuplicate;
    }

    private Boolean checkLastGoodsScaned(ArrayList<WIDataGeneral> wiDataGenerals1, String goodsID) {
        if (goodsID.equalsIgnoreCase(wiDataGenerals1.get(wiDataGenerals1.size() - 1).getGoodsID())) {
            isLastGoodsScaned = true;
        }
        return isLastGoodsScaned;
    }

    private Boolean checkExistGoodsID(String goodsID, ArrayList<WIDataGeneral> wiDataGenerals) {
        for (int i = 0; i < wiDataGenerals.size(); i++) {
            if (goodsID.equalsIgnoreCase(wiDataGenerals.get(i).getGoodsID())) {
                isGoodsIDExist = true;
                break;
            }
        }
        return isGoodsIDExist;
    }

    private Boolean checkGoodsIDAndDateScaned(String goodsID, String date, ArrayList<WIDataGeneral> wiDataGenerals1, ArrayList<GoodsDataIssue> goodsData1) {
        for (int i = 0; i < wiDataGenerals1.size(); i++) {
            for (int j = 0; j < goodsData1.size(); j++) {
                if (wiDataGenerals1.get(i).getQuantity().doubleValue() > 0 && goodsID.equalsIgnoreCase(goodsData1.get(j).getGoodsID()) && date.equalsIgnoreCase(goodsData1.get(j).getDate())) {
                    isGoodsIDAndDateScanned = true;
                    break;
                }
            }
        }
        return isGoodsIDAndDateScanned;
    }

    private ArrayList<WIDataGeneral> getNotYetList(ArrayList<WIDataGeneral> wiDataGenerals) {
        ArrayList<WIDataGeneral> notYetList = new ArrayList<>();
        for (int i = 0; i < wiDataGenerals.size(); i++) {
            if (wiDataGenerals.get(i).getQuantity() == 0) {
                notYetList.add(wiDataGenerals.get(i));
            }
        }
        return notYetList;
    }

    private ArrayList<WIDataGeneral> getEnoughtYetList(ArrayList<WIDataGeneral> wiDataGenerals) {
        ArrayList<WIDataGeneral> enoughYettList = new ArrayList<>();
        for (int i = 0; i < wiDataGenerals.size(); i++) {
            if (wiDataGenerals.get(i).getQuantity() > 0 && wiDataGenerals.get(i).getQuantity() < wiDataGenerals.get(i).getQuantityOrg()) {
                enoughYettList.add(wiDataGenerals.get(i));
            }
        }
        return enoughYettList;
    }

    private ArrayList<WIDataGeneral> getEnoughtList(ArrayList<WIDataGeneral> wiDataGenerals) {
        ArrayList<WIDataGeneral> enoughtList = new ArrayList<>();
        for (int i = 0; i < wiDataGenerals.size(); i++) {
            if (wiDataGenerals.get(i).getQuantity() > 0 && wiDataGenerals.get(i).getQuantity().doubleValue() == wiDataGenerals.get(i).getQuantityOrg().doubleValue()) {
                enoughtList.add(wiDataGenerals.get(i));
            }
        }
        return enoughtList;
    }

    private ArrayList<WIDataGeneral> getOvertList(ArrayList<WIDataGeneral> wiDataGenerals) {
        ArrayList<WIDataGeneral> overList = new ArrayList<>();
        for (int i = 0; i < wiDataGenerals.size(); i++) {
            if (wiDataGenerals.get(i).getQuantity() > wiDataGenerals.get(i).getQuantityOrg()) {
                overList.add(wiDataGenerals.get(i));
            }
        }
        return overList;
    }

    private void alertError(Context context, String message) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));
        dialog.setTitle("Error")
                .setIcon(R.drawable.ic_baseline_error_24)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                    }
                }).show();
    }

    private void alertSuccess(Context context, String message) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));
        dialog.setTitle("Success")
                .setIcon(R.drawable.ic_baseline_done_24)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                    }
                }).show();
    }

    private void setAdapter(ArrayList<WIDataGeneral> wiDataGenerals) {
        adapter = new WIDataGeneralAdapter(getApplicationContext(), wiDataGenerals, new WIDataGeneralAdapter.ItemClickListener() {
            @Override
            public void onClick(WIDataGeneral wiDataGeneral) {
            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void hideSoftKeyBoard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);

        if (imm.isAcceptingText()) { // verify if the soft keyboard is open
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.action_bar_issuedata_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        if (id == R.id.action_all) {
            ArrayList<WIDataGeneral> allList = wiDataGenerals;
            setAdapter(allList);
            return true;
        }
        if (id == R.id.action_not_yet) {
            ArrayList<WIDataGeneral> notYetList = getNotYetList(wiDataGenerals);
            setAdapter(notYetList);
            return true;
        }
        if (id == R.id.action_enought_yet) {
            ArrayList<WIDataGeneral> enoughtYetLish = getEnoughtYetList(wiDataGenerals);
            setAdapter(enoughtYetLish);
            return true;
        }
        if (id == R.id.action_enought) {
            ArrayList<WIDataGeneral> enoughtList = getEnoughtList(wiDataGenerals);
            setAdapter(enoughtList);
            return true;
        }
        if (id == R.id.action_over) {
            ArrayList<WIDataGeneral> overList = getOvertList(wiDataGenerals);
            setAdapter(overList);
            return true;
        }
//        if (id == R.id.action_save) {
//            process();
//            return true;
//        }
//        if (id == R.id.action_approve) {
//            if (wiDataHeader != null) {
//                WIDataHeader wiDataHeaderUpdate = wiDataHeader;
//                wiDataHeaderUpdate.setHandlingStatusID("2");
//                wiDataHeaderUpdate.setHandlingStatusName("Duyệt mức 2");
//                putWIDataHeader(edtWIDNumber.getText().toString(), wiDataHeaderUpdate);
//                alertSuccess(getApplicationContext(), "Duyệt mức 2 xuất kho thành công");
//            }
//            return true;
//        }
//        if (id == R.id.action_clearlist) {
//            if(wiDataGenerals.size()>0) {
//                clearlist();
//            }
//            return true;
//        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onOpened(EMDKManager emdkManager) {
        this.emdkManager = emdkManager;
        // Acquire the barcode manager resources
        barcodeManager = (BarcodeManager) emdkManager.getInstance(EMDKManager.FEATURE_TYPE.BARCODE);
        // Add connection listener
        if (barcodeManager != null) {
            barcodeManager.addConnectionListener(this);
        }
        // Enumerate scanner devices
        enumerateScannerDevices();

        //
        initScanner();

        stopScan();

    }

    @Override
    protected void onResume() {
        super.onResume();
        // The application is in foreground
        if (emdkManager != null) {
            // Acquire the barcode manager resources
            initBarcodeManager();
            // Enumerate scanner devices
            enumerateScannerDevices();
            // Initialize scanner
            initScanner();
            stopScan();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        // The application is in background
        // Release the barcode manager resources
        deInitScanner();
        deInitBarcodeManager();
    }

    @Override
    public void onClosed() {
        // Release all the resources
        if (emdkManager != null) {
            emdkManager.release();
            emdkManager = null;
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Release all the resources
        if (emdkManager != null) {
            emdkManager.release();
            emdkManager = null;
        }
    }

    @Override
    public void onConnectionChange(ScannerInfo scannerInfo, BarcodeManager.ConnectionState connectionState) {
        String status;
        String scannerName = "";
        String statusExtScanner = connectionState.toString();
        String scannerNameExtScanner = scannerInfo.getFriendlyName();
        if (deviceList.size() != 0) {
            scannerName = deviceList.get(1).getFriendlyName();
        }
        if (scannerName.equalsIgnoreCase(scannerNameExtScanner)) {
            switch (connectionState) {
                case CONNECTED:
                    synchronized (lock) {
                        initScanner();
                        setDecoders();
                        bExtScannerDisconnected = false;
                    }
                    break;
                case DISCONNECTED:
                    bExtScannerDisconnected = true;
                    synchronized (lock) {
                        deInitScanner();
                    }
                    break;
            }
        } else {
            bExtScannerDisconnected = false;
        }
    }

    @Override
    public void onData(ScanDataCollection scanDataCollection) {
        if ((scanDataCollection != null) && (scanDataCollection.getResult() == ScannerResults.SUCCESS)) {
            ArrayList<ScanDataCollection.ScanData> scanData = scanDataCollection.getScanData();
            for (ScanDataCollection.ScanData data : scanData) {
                updateData(data.getData());
            }
        }
    }

    @Override
    public void onStatus(StatusData statusData) {
        StatusData.ScannerStates state = statusData.getState();
        switch (state) {
            case IDLE:
//                statusString = statusData.getFriendlyName() + " is enabled and idle...";
//                updateStatus(statusString);
                if (bContinuousMode) {
                    try {
                        scanner.triggerType = Scanner.TriggerType.SOFT_ALWAYS;
                        // An attempt to use the scanner continuously and rapidly (with a delay < 100 ms between scans)
                        // may cause the scanner to pause momentarily before resuming the scanning.
                        // Hence add some delay (>= 100ms) before submitting the next read.
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        scanner.read();
                    } catch (ScannerException e) {
                        updateStatus(e.getMessage());
                    }
                }
                // set trigger type
                if (bSoftTriggerSelected) {
                    scanner.triggerType = Scanner.TriggerType.SOFT_ONCE;
                    bSoftTriggerSelected = false;
                } else {
                    scanner.triggerType = Scanner.TriggerType.HARD;
                }
                // set decoders
                if (bDecoderSettingsChanged) {
                    setDecoders();
                    bDecoderSettingsChanged = false;
                } else {
                    setDecoders();
                }
                // submit read
                if (!scanner.isReadPending() && !bExtScannerDisconnected) {
                    try {
                        scanner.read();
                    } catch (ScannerException e) {
                    }
                }
                break;
            case WAITING:
                break;
            case SCANNING:
                break;
            case DISABLED:
                break;
            case ERROR:
                break;
            default:
                break;
        }
    }

    private void initScanner() {
        if (scanner == null) {
            if ((deviceList != null) && (deviceList.size() != 0)) {
                if (barcodeManager != null)
                    scanner = barcodeManager.getDevice(deviceList.get(1));
            } else {
//                updateStatus("Failed to get the specified scanner device! Please close and restart the application.");
                return;
            }
            if (scanner != null) {
                scanner.addDataListener(this);
                scanner.addStatusListener(this);
                try {
                    scanner.enable();
                } catch (ScannerException e) {
                    updateStatus(e.getMessage());
                    deInitScanner();
                }
            } else {
//                updateStatus("Failed to initialize the scanner device.");
            }
        }
    }

    private void deInitScanner() {
        if (scanner != null) {
            try {
                scanner.disable();
            } catch (Exception e) {
//                updateStatus(e.getMessage());
            }

            try {
                scanner.removeDataListener(this);
                scanner.removeStatusListener(this);
            } catch (Exception e) {
//                updateStatus(e.getMessage());
            }

            try {
                scanner.release();
            } catch (Exception e) {
//                updateStatus(e.getMessage());
            }
            scanner = null;
        }
    }

    private void initBarcodeManager() {
        barcodeManager = (BarcodeManager) emdkManager.getInstance(EMDKManager.FEATURE_TYPE.BARCODE);
        // Add connection listener
        if (barcodeManager != null) {
            barcodeManager.addConnectionListener(this);
        }
    }

    private void deInitBarcodeManager() {
        if (emdkManager != null) {
            emdkManager.release(EMDKManager.FEATURE_TYPE.BARCODE);
        }
    }


    private void enumerateScannerDevices() {
        if (barcodeManager != null) {
            deviceList = barcodeManager.getSupportedDevicesInfo();
            if ((deviceList != null) && (deviceList.size() != 0)) {

            } else {
            }
        }
    }

    private void setDecoders() {
        if (scanner != null) {
            try {
                ScannerConfig config = scanner.getConfig();
                // Set EAN8
                config.decoderParams.ean8.enabled = true;
                // Set EAN13
                config.decoderParams.ean13.enabled = true;
                // Set Code39
                config.decoderParams.code39.enabled = true;
                //Set Code128
                config.decoderParams.code128.enabled = true;
                //set inverser1Mode
                config.readerParams.readerSpecific.laserSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                config.readerParams.readerSpecific.cameraSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                config.readerParams.readerSpecific.imagerSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                scanner.setConfig(config);
            } catch (ScannerException e) {
                updateStatus(e.getMessage());
            }
        }
    }

    private void setReaderParams() {
        if (scanner != null) {
            try {
                ScannerConfig config = scanner.getConfig();
                config.readerParams.readerSpecific.laserSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                scanner.setConfig(config);
            } catch (ScannerException e) {
                updateStatus(e.getMessage());
            }
        }
    }

    private void stopScan() {

        if (scanner != null) {
            // Cancel the pending read.
            try {
                scanner.cancelRead();
            } catch (ScannerException e) {
                e.printStackTrace();
            }
        }
    }

    private void cancelRead() {
        if (scanner != null) {
            if (scanner.isReadPending()) {
                try {
                    scanner.cancelRead();
                } catch (ScannerException e) {
                }
            }
        }
    }

    private void updateStatus(final String status) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
            }
        });
    }

    private void updateData(final String result) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (result != null) {
                    if (edtWIDNumber.hasFocus()) {
                        edtWIDNumber.setText(result);
                    } else {
                        String goodsID = "";
                        String quantity = "";
                        String datetime = "";
                        int index1 = result.indexOf("-");
                        int index2 = 0;
//                        int index2 = result.indexOf("-", index1 + 1);
                        if (index1 > 0) {
                            if (index2 > 0) {
                                goodsID = result.substring(0, index1);
                                quantity = result.substring(index1 + 1, index2);
                                datetime = result.substring(index2 + 1, result.length());
                            } else {
                                goodsID = result.substring(0, index1);
                                quantity = result.substring(index1 + 1, result.length());
                                datetime = "";
                            }
                        }
                        edtGoodsID.setText(goodsID);
                        edtQuantity.setText(quantity);
                        edtDateTime.setText(datetime);
                    }

                    checkLastGoodsScaned(wiDataGenerals, edtGoodsID.getText().toString());
                    if (isLastGoodsScaned == true) {
                        processAll();
                        alertSuccess(getApplicationContext(), "Hoàn thành xuất kho cho mã xe: " + selectedGoodsGroupID);
                    } else {
                        process();
                    }
                }
            }
        });
    }

    private void addGoodsData(GoodsDataIssue goodsData) {
        issueDataDAO.insert(goodsData);
    }

    private ArrayList<GoodsDataIssue> getGoodsDataByWIDNumber(String widNumber) {
        return issueDataDAO.getByWIDNumber(widNumber);
    }

    private void deleteGoodsDataByWIDNumber(String widNumber) {
        issueDataDAO.deleteByWIDNumber(widNumber);
    }

    private void process() {
        if (TextUtils.isEmpty(edtGoodsID.getText().toString())) {
            alertError(getApplicationContext(), "Nhập mã hàng");
            edtGoodsID.requestFocus();
            return;
        }
//        if (TextUtils.isEmpty(edtQuantity.getText().toString())) {
//            alertError(getApplicationContext(), "Nhập số lượng");
//            edtQuantity.requestFocus();
//            return;
//        }
//        if (!edtQuantity.getText().toString().matches(numberRegex)) {
//            alertError(getApplicationContext(), "Không phải là số");
//            return;
//        }

        if (selectedGoodsGroupID.equalsIgnoreCase("All")) {
            alertError(getApplicationContext(), " Chưa chọn mã dòng xe");
            return;
        }

        isGoodsIDExist = false;
        checkExistGoodsID(edtGoodsID.getText().toString(), wiDataGenerals);
        if (isGoodsIDExist == true) {
            isGoodsIDExist = false;
        } else {
            alertError(getApplicationContext(), "Mã hàng không tồn tại");
            return;
        }

        goodsDataArrayList = getGoodsDataByWIDNumber(edtWIDNumber.getText().toString());
        isGoodsIDAndDateScanned = false;
        checkGoodsIDAndDateScaned(edtGoodsID.getText().toString(), edtDateTime.getText().toString(), wiDataGenerals, goodsDataArrayList);
        if (isGoodsIDAndDateScanned == true) {
            alertError(getApplicationContext(), "Đã scan.Scan mã hàng khác");
            isGoodsIDAndDateScanned = false;
            return;
        }
        goodsDataIssue.setWidNumber(edtWIDNumber.getText().toString());
        goodsDataIssue.setGoodsID(edtGoodsID.getText().toString());
        goodsDataIssue.setGoodsGroupID("");
        goodsDataIssue.setDate(edtDateTime.getText().toString());
        goodsDataIssue.setStatus("OK");
        addGoodsData(goodsDataIssue);


//        totalQuantity = totalQuantityOrg;
//        totalGoods = totalGoodsOrg;
//        getTotalGoods(wiDataGenerals);
//        getTotalQuantity(wiDataGenerals);
        Call<List<WIDataGeneral>> callback = iWarehouseApi.GetByWIDNumberAndGoodsIDAndGoodsGroupID(edtWIDNumber.getText().toString(), edtGoodsID.getText().toString(), selectedGoodsGroupID);
        callback.enqueue(new Callback<List<WIDataGeneral>>() {
            @Override
            public void onResponse(Call<List<WIDataGeneral>> call, Response<List<WIDataGeneral>> response) {
                if (response.isSuccessful()) {
                    arrayListByGoodsID = (ArrayList<WIDataGeneral>) response.body();
                    if (arrayListByGoodsID.size() > 0) {
                        WIDataGeneral wiDataGeneralUpdate = arrayListByGoodsID.get(0);
                        checkGoodsIDDuplicate(arrayListByGoodsID, edtGoodsID.getText().toString());
                        if (isGoodsIdDuplicate == false) {
                            wiDataGeneralUpdate.setQuantity(Double.parseDouble(edtQuantity.getText().toString()));
                        } else {
                            wiDataGeneralUpdate.setQuantity(wiDataGeneralUpdate.getQuantity() + Double.parseDouble(edtQuantity.getText().toString()));
                            isGoodsIdDuplicate = false;
                        }
                        wiDataGeneralUpdate.setWidNumber(edtWIDNumber.getText().toString());
                        wiDataGeneralUpdate.setGoodsID(edtGoodsID.getText().toString());
                        wiDataGeneralUpdate.setOrdinal(arrayListByGoodsID.get(0).getOrdinal());
                        wiDataGeneralUpdate.setTotalGoods(wiDataGeneralUpdate.getTotalGoods() - totalGoodsOrg + totalGoods);
                        wiDataGeneralUpdate.setTotalQuantity(wiDataGeneralUpdate.getTotalQuantity() - wiDataGeneralUpdate.getQuantityOrg() + Double.parseDouble(edtQuantity.getText().toString()));
                        putWIDataGeneral(wiDataGeneralUpdate.getWidNumber(), wiDataGeneralUpdate.getGoodsID(), wiDataGeneralUpdate.getOrdinal(), wiDataGeneralUpdate.getGoodsGroupID(), wiDataGeneralUpdate);


                        WIDataHeader wiDataHeaderUpdate = wiDataHeader;
                        wiDataHeaderUpdate.setTotalQuantity(totalQuantity + Double.parseDouble(edtQuantity.getText().toString()));
                        putWIDataHeader(edtWIDNumber.getText().toString(), wiDataHeaderUpdate);

                    }
                }
            }

            @Override
            public void onFailure(Call<List<WIDataGeneral>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "get widatadetail by idcode" + t.getMessage() + "", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void processAll() {
        for (int i = 0; i < wiDataGenerals.size(); i++) {
            if (wiDataGenerals.get(i).getQuantity() == 0) {
                WIDataGeneral wiDataGeneralUpdate = wiDataGenerals.get(i);
                wiDataGeneralUpdate.setQuantity(wiDataGeneralUpdate.getQuantityOrg());
                wiDataGeneralUpdate.setWidNumber(edtWIDNumber.getText().toString());
                wiDataGeneralUpdate.setGoodsID(wiDataGeneralUpdate.getGoodsID());
                wiDataGeneralUpdate.setOrdinal(wiDataGeneralUpdate.getOrdinal());
                wiDataGeneralUpdate.setTotalGoods(wiDataGeneralUpdate.getTotalGoods() - totalGoodsOrg + totalGoods);
                wiDataGeneralUpdate.setTotalQuantity(wiDataGeneralUpdate.getTotalQuantity() - totalQuantityOrg + totalQuantity);
                putWIDataGeneral(wiDataGeneralUpdate.getWidNumber(), wiDataGeneralUpdate.getGoodsID(), wiDataGeneralUpdate.getOrdinal(), wiDataGeneralUpdate.getGoodsGroupID(), wiDataGeneralUpdate);

                WIDataHeader wiDataHeaderUpdate = wiDataHeader;
                wiDataHeaderUpdate.setTotalQuantity(0.0);
                putWIDataHeader(edtWIDNumber.getText().toString(), wiDataHeaderUpdate);
            }
        }
    }

    private void clearlist() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(IssueDataActivity.this);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.dialog_clear_list, viewGroup, false);
        builder.setView(dialogView);
        final android.app.AlertDialog alertDialog = builder.create();
        alertDialog.show();

        Button btnYes, btnNo;
        btnYes = alertDialog.findViewById(R.id.btnYes);
        btnNo = alertDialog.findViewById(R.id.btnNo);
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int i = 0; i < wiDataGenerals.size(); i++) {
                    WIDataGeneral wiDataGeneralUpdate = wiDataGenerals.get(i);
                    wiDataGeneralUpdate.setQuantity(0.0);
                    wiDataGeneralUpdate.setTotalGoods(wiDataGeneralUpdate.getTotalGoodsOrg());
                    wiDataGeneralUpdate.setTotalQuantity(wiDataGeneralUpdate.getTotalQuantityOrg());
                    putWIDataGeneral(wiDataGeneralUpdate.getWidNumber(), wiDataGeneralUpdate.getGoodsID(), wiDataGeneralUpdate.getOrdinal(), wiDataGeneralUpdate.getGoodsGroupID(), wiDataGeneralUpdate);
                }
                WIDataHeader wiDataHeaderUpdate = wiDataHeader;
                wiDataHeaderUpdate.setTotalQuantity(wiDataHeaderUpdate.getTotalQuantityOrg());
                putWIDataHeader(edtWIDNumber.getText().toString(), wiDataHeaderUpdate);
                deleteGoodsDataByWIDNumber(edtWIDNumber.getText().toString());
                alertDialog.cancel();
                alertSuccess(IssueDataActivity.this, "Đã xóa dữ liệu quét: " + edtWIDNumber.getText().toString());
            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.cancel();
            }
        });
    }

    private void getScanNumber(EditText editText) {
        scanNumber = 0;
        if (editText.getText().toString().equalsIgnoreCase("0")) {
            scanNumber = 1;
        }
        edtScanNumber.setText(scanNumber+"");
    }
}