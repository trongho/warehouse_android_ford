package com.example.warehousemobileclient.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class CountGoods {
    @SerializedName("locationID")
    @Expose
    private String locationID;
    @SerializedName("goodsID")
    @Expose
    private String goodsID;
    @SerializedName("goodsName")
    @Expose
    private String goodsName;
    @SerializedName("quantity")
    @Expose
    private Double quantity;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("countDate")
    @Expose
    private String countDate;

    public CountGoods() {
    }

    public CountGoods(String locationID, String goodsID, String goodsName, Double quantity, String status, String countDate) {
        this.locationID = locationID;
        this.goodsID = goodsID;
        this.goodsName = goodsName;
        this.quantity = quantity;
        this.status = status;
        this.countDate = countDate;
    }

    public String getLocationID() {
        return locationID;
    }

    public void setLocationID(String locationID) {
        this.locationID = locationID;
    }

    public String getGoodsID() {
        return goodsID;
    }

    public void setGoodsID(String goodsID) {
        this.goodsID = goodsID;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCountDate() {
        return countDate;
    }

    public void setCountDate(String countDate) {
        this.countDate = countDate;
    }
}
