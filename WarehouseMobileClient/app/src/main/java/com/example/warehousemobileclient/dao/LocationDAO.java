package com.example.warehousemobileclient.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ParseException;

import com.example.warehousemobileclient.helper.DBHelper;
import com.example.warehousemobileclient.model.GoodsData;
import com.example.warehousemobileclient.model.Location;

import java.util.ArrayList;

public class LocationDAO {
    DBHelper dbHelper;

    public LocationDAO(Context context) {
        dbHelper = new DBHelper(context);
    }

    public ArrayList<Location> getAll() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        ArrayList<Location> list = new ArrayList<>();
        String sql = "SELECT * FROM LOCATION";
        Cursor c = db.rawQuery(sql, null);
        c.moveToFirst();
        while (!c.isAfterLast()) {
            String locationid = c.getString(1);
            list.add(new Location(locationid));
            c.moveToNext();
        }
        return list;
    }


    public long insert(Location location) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("LOCATIONID",location.getLocationID());
        return db.insert("LOCATION", null, values);
    }

    public int delete() {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        return db.delete("LOCATION", null, null);
    }

}
