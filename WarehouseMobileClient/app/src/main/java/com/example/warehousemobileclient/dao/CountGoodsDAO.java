package com.example.warehousemobileclient.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.warehousemobileclient.helper.DBHelper;
import com.example.warehousemobileclient.model.CheckGoodsDetail;
import com.example.warehousemobileclient.model.CountGoods;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class CountGoodsDAO {
    DBHelper dbHelper;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public CountGoodsDAO(Context context) {
        dbHelper = new DBHelper(context);
    }

    public ArrayList<CountGoods> getAll() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        ArrayList<CountGoods> list = new ArrayList<>();
        String sql = "SELECT * FROM COUNTGOODS";
        Cursor c = db.rawQuery(sql, null);
        c.moveToFirst();
        while (!c.isAfterLast()) {
            String locationid = c.getString(1);
            String goodsid = c.getString(2);
            String goodsname = c.getString(3);
            Double quantity = c.getDouble(4);
            String status = c.getString(5);
            String countDate = c.getString(6);
            list.add(new CountGoods(locationid, goodsid, goodsname, quantity, status, countDate));
            c.moveToNext();
        }
        return list;
    }

    public ArrayList<CountGoods> getByLocationID(String locationID) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        ArrayList<CountGoods> list = new ArrayList<>();
        String sql = "SELECT * FROM COUNTGOODS WHERE LOCATIONID=? ";
        Cursor c = db.rawQuery(sql, new String[]{locationID});
        c.moveToFirst();
        while (!c.isAfterLast()) {
            String locationid = c.getString(1);
            String goodsid = c.getString(2);
            String goodsname = c.getString(3);
            Double quantity = c.getDouble(4);
            String status = c.getString(5);
            String countDate = c.getString(6);
            list.add(new CountGoods(locationid, goodsid, goodsname, quantity, status, countDate));
            c.moveToNext();
        }
        return list;
    }

    public ArrayList<CountGoods> getByCountDate(String dateString) {
        int index1 = dateString.indexOf("-");
        int index2 = dateString.indexOf("-", index1 + 1);
        String year = dateString.substring(0, index1);
        String month = dateString.substring(index1 + 1, index2);
        String day = dateString.substring(index2 + 1, dateString.length());
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        ArrayList<CountGoods> list = new ArrayList<>();
        String sql = "SELECT * FROM COUNTGOODS WHERE STRFTIME('%Y-%m-%d',COUNTGOODS.COUNTDATE)=?";
        Cursor c = db.rawQuery(sql, new String[]{year + "-" + month + "-" + day});
        c.moveToFirst();
        while (!c.isAfterLast()) {
            String locationid = c.getString(1);
            String goodsid = c.getString(2);
            String goodsname = c.getString(3);
            Double quantity = c.getDouble(4);
            String status = c.getString(5);
            String countDate = c.getString(6);
            list.add(new CountGoods(locationid, goodsid, goodsname, quantity, status, countDate));
            c.moveToNext();
        }
        return list;
    }

    public Double getSumQuantityByLocationID(String locationID) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Double sumQuantity = 0.0;
        String sql = "SELECT * FROM COUNTGOODS WHERE COUNTGOODS.LOCATIONID=?";
        Cursor c = db.rawQuery(sql, new String[]{locationID});
        if (c != null && c.getCount() > 0) {
            c.moveToFirst();
            while (!c.isAfterLast()) {
                sumQuantity = c.getDouble(4);
                c.moveToNext();
            }
        }
        return sumQuantity;
    }

    public Boolean checkExist(String locationID, String goodsID) {
        Boolean flag = false;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String sql = "SELECT * FROM COUNTGOODS WHERE LOCATIONID=? AND GOODSID=?";
        Cursor c = db.rawQuery(sql, new String[]{locationID, goodsID});
        c.moveToFirst();
        while (!c.isAfterLast()) {
            if (c != null && c.getCount() > 0) {
                flag = true;
            }
            c.moveToNext();
        }
        return flag;
    }

    public int getSumGoodsIDByLocationID(String locationID) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        int sumGoodsID = 0;
        String sql = "SELECT * FROM COUNTGOODS WHERE COUNTGOODS.LOCATIONID=?";
        Cursor c = db.rawQuery(sql, new String[]{locationID});
        if (c != null && c.getCount() > 0) {
            c.moveToFirst();
            while (!c.isAfterLast()) {
                sumGoodsID++;
                c.moveToNext();
            }
        }
        return sumGoodsID;
    }

    public long insert(CountGoods countGoods) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("LOCATIONID", countGoods.getLocationID());
        values.put("GOODSID", countGoods.getGoodsID());
        values.put("GOODSNAME", countGoods.getGoodsName());
        values.put("QUANTITY", countGoods.getQuantity());
        values.put("STATUS", countGoods.getStatus());
        values.put("COUNTDATE", simpleDateFormat.format(Calendar.getInstance().getTime()));
        return db.insert("COUNTGOODS", null, values);
    }

    public int update(CountGoods countGoods) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("LOCATIONID", countGoods.getLocationID());
        values.put("GOODSID", countGoods.getGoodsID());
        values.put("GOODSNAME", countGoods.getGoodsName());
        values.put("QUANTITY", countGoods.getQuantity());
        values.put("STATUS", countGoods.getStatus());
        values.put("COUNTDATE", simpleDateFormat.format(Calendar.getInstance().getTime()));
        return db.update("COUNTGOODS", values, "LOCATIONID=? AND GOODSID=?", new String[]{countGoods.getLocationID(), countGoods.getGoodsID()});
    }

    public int deleteByID(String locationID) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        return db.delete("COUNTGOODS", "LOCATIONID=?", new String[]{locationID});
    }

    public int deleteByID(String locationID, String goodsID) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        return db.delete("COUNTGOODS", "LOCATIONID=? AND GOODSID=?", new String[]{locationID, goodsID});
    }

    public int deleteByDate(String deleteDate) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        return db.delete("COUNTGOODS", "COUNTGOODS.COUNTDATE=?", new String[]{deleteDate});
    }
}
